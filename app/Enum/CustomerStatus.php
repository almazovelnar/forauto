<?php

namespace App\Enum;

class CustomerStatus
{
    const DEACTIVATED = 0;
    const ACTIVATED = 1;
    const DELETED = 2;

    public static function getList() : array {
        return [
            self::DEACTIVATED => trans('admin.status.deactivated'),
            self::ACTIVATED => trans('admin.status.activated'),
            self::DELETED => trans('admin.status.deleted')
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
