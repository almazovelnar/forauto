<?php
declare(strict_types=1);

namespace App\Enum;

class Sort
{
    const DATE_DESC = 'created_at desc';
    const POPULAR_DESC = 'views desc';
    const PRICE = 'COALESCE(sale_price, price) ASC';
    const PRICE_DESC = 'COALESCE(sale_price, price) desc';

    public static function getList(): array
    {
        return [
            self::DATE_DESC => trans('site.filter.date_desc'),
            self::POPULAR_DESC => trans('site.filter.popular'),
            self::PRICE => trans('site.filter.price'),
            self::PRICE_DESC => trans('site.filter.price_desc'),
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
