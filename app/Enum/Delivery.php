<?php
declare(strict_types=1);

namespace App\Enum;

class Delivery
{
    const MYSELF = 'myself';
    const COURIER = 'courier';

    public static function getList(): array
    {
        return [
            self::MYSELF => trans('site.delivery.myself'),
            self::COURIER => trans('site.delivery.courier')
        ];
    }

    public static function get(string $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
