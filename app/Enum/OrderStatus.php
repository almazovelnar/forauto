<?php
declare(strict_types=1);

namespace App\Enum;

class OrderStatus
{
    const PENDING = 0;
    const COMPLETED = 1;
    const CANCELED = 2;
    const REVERSED = 3;


    public static function getList(): array
    {
        return [
            self::PENDING => trans('admin.status.pending'),
            self::COMPLETED => trans('admin.status.completed'),
            self::CANCELED => trans('admin.status.canceled'),
            self::REVERSED => trans('admin.status.reversed')
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
