<?php
declare(strict_types=1);

namespace App\Enum;

class CategoryTemplate
{
    const DEFAULT = 'default';

    public static function getList(): array
    {
        return [
            self::DEFAULT => trans('admin.template.default')
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
