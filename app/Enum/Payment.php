<?php
declare(strict_types=1);

namespace App\Enum;

class Payment
{
    const POS = 'pos';
    const CASH = 'cash';

    public static function getList(): array
    {
        return [
            self::POS => trans('site.payment.pos'),
            self::CASH => trans('site.payment.cash')
        ];
    }

    public static function get(string $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
