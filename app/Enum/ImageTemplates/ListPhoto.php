<?php

namespace App\Enum\ImageTemplates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ListPhoto implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        $image->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $canvas = \Image::canvas(300,300);
        $canvas->insert($image, 'center');

        return $canvas;
    }
}
