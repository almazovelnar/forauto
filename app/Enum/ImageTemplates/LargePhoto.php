<?php

namespace App\Enum\ImageTemplates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class LargePhoto implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        $image->resize(800, 800, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $canvas = \Image::canvas(800, 800);
        $canvas->insert($image, 'center');

        return $canvas;
    }
}
