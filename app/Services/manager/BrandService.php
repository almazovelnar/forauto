<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Brand;
use Illuminate\Support\Facades\Storage;
use App\Repositories\BrandRepository;
use App\Http\Requests\Brand\{BrandCreateRequest, BrandUpdateRequest};

class BrandService
{
    public function create(BrandCreateRequest $request): Brand
    {
        $model = Brand::create(
            $request->category_id,
            $request->title,
            $request->link,
            $request->description,
            (bool) $request->status,
            (bool) $request->chosen,
            \Str::slug($request->slug),
            $request->meta,
            $request->parent_id,
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('brands');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(BrandUpdateRequest $request, Brand $model): Brand
    {
        $model->edit(
            +$request->category_id,
            $request->title,
            $request->link,
            $request->description,
            (bool) $request->status,
            (bool) $request->chosen,
            \Str::slug($request->slug),
            $request->meta,
            $request->parent_id,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('brands/' . $model->image)) {
                Storage::delete('brands/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('brands');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Brand $model)
    {
        if (!$model->delete())
            throw new RuntimeException('Deleting error');
    }
    function arraySlug($requestSlug)
    {
        $slug = [];
        foreach ($requestSlug as $key => $slg) {
            $slug[$key] = \Str::slug($slg);
        }
        return $slug;
    }
}
