<?php

namespace App\Services\manager;

use App\Http\Requests\Faq\FaqRequest;
use App\Models\Faq;

class FaqService
{
    public function create(FaqRequest $request)
    {
        $model = Faq::create(
            $request->language,
            $request->question,
            $request->answer,
            !!$request->status
        );

        $model->save();
        return $model;
    }

    public function update(Faq $model, FaqRequest $request)
    {
        $model->edit(
            $request->language,
            $request->question,
            $request->answer,
            !!$request->status
        );

        $model->save();
        return $model;
    }

    public function remove($model)
    {
        $model->delete();
    }
}
