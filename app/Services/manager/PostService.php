<?php

namespace App\Services\manager;

use App\Models\Tag;
use App\Models\Post;
use App\Repositories\TagRepository;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Post\{PostCreateRequest, PostUpdateRequest};

class PostService
{
    /**
     * @var TagRepository
     */
    private $tagRepository;
    /**
     * @var PhotoService
     */
    private $photoService;

    public function __construct(
        TagRepository $tagRepository,
        PhotoService  $photoService
    )
    {
        $this->tagRepository = $tagRepository;
        $this->photoService  = $photoService;
    }

    public function create(PostCreateRequest $request)
    {
        $tagIds = [];
        $model  = Post::create($request->category, $request->title, $request->description, $request->meta,
            (bool)$request->status, (bool)$request->chosen, $request->date, $this->arraySlug($request->slug));


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            if (!Storage::exists('posts')) Storage::makeDirectory('posts');
            $path = \Storage::path('posts') . '/' . $file->hashName();
            \Image::make($file)->fit(415, 275)->save($path, 100);
            $model->attachImage($file->hashName());
        }

        if (!empty($request->tags)) {
            foreach ($request->tags as $lang => $tagItems) {
                foreach ($tagItems as $tagItem) {
                    $tagSlug = \Str::slug($tagItem);

                    if (($tag = $this->tagRepository->get($tagSlug, $lang)) != null) {
                        $tag->count++;
                    } else {
                        $tag = Tag::create($tagItem, $tagSlug, $lang);
                    }

                    if ($tag->save()) {
                        $tagIds[] = $tag->id;
                    }
                }
            }
        }

        $model->save();

        if (!empty($tagIds)) {
            $model->tags()->sync($tagIds);
        }

        return $model;
    }


    public function update(PostUpdateRequest $request, Post $model)
    {
        $tagIds = [];
        $model->edit($request->category, $request->title, $request->description, $request->meta,
            (bool)$request->status, (bool)$request->chosen, $request->date, $this->arraySlug($request->slug));

        if ($request->hasFile('image')) {
            if (Storage::exists('posts/' . $model->image)) {
                Storage::delete('posts/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $path = \Storage::path('posts') . '/' . $file->hashName();
            \Image::make($file)->fit(415, 275)->save($path, 100);
            $model->attachImage($file->hashName());
        }

        if ($request->tags) {
            foreach ($request->tags as $lang => $tagItems) {
                foreach ($tagItems as $tagItem) {
                    $tagSlug = \Str::slug($tagItem);

                    if (($tag = $this->tagRepository->get($tagSlug, $lang)) != null) {
                        if (!$model->tags->contains($tag->id)) {
                            $tag->count++;
                        }
                    } else {
                        $tag = Tag::create($tagItem, $tagSlug, $lang);
                    }

                    if ($tag->save()) {
                        $tagIds[] = $tag->id;
                    }
                }
            }
        }

        $model->save();

        if ($tagIds) {
            $model->tags()->sync($tagIds);
        }

        return $model;
    }

    public function deleteImage(int $id)
    {
//        /** @var Post $model */
//        $model = Post::findOrFail($id);
//        Storage::delete('posts/' . $model->image);
//        $model->detachImage();
//        $model->save();
    }

    public function removeImage(int $id)
    {
        /** @var Post $model */
        $model = Post::findOrFail($id);
        Storage::delete('posts/' . $model->image);
        $model->detachImage();
        $model->save();
    }

    public function remove($model)
    {
        $model->delete();
    }

    public function arraySlug($requestSlug)
    {
        $slug = [];
        foreach ($requestSlug as $key => $slg) {
            $slug[$key] = \Str::slug($slg);
        }
        return $slug;
    }
}
