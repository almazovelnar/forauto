<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Models\ProductPhoto;
use App\Models\PropertyProduct;
use RuntimeException;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Imports\ProductImport;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Product\{ProductCreateRequest, ProductUpdateRequest};

class ProductService
{
    public function create(ProductCreateRequest $request): Product
    {
        $model = Product::create(
            $request->title,
            $request->description,
            $request->specifications,
            $request->category_id,
            $request->brand_id,
            $request->code,
            $request->package,
            $request->count,
            $request->price * 100,
            $this->generateSlug($request->title),
            (bool)$request->status,
            (bool)$request->chosen,
            (bool)$request->hot,
            (bool)$request->is_season,
            (bool)$request->is_new,
            $request->meta,
            $request->sale_price * 100 ?: null,
            $request->stars ?? 0
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('products');
            $model->attachImage($file->hashName());
        }

        if ($request->hasFile('season_banner')) {
            $file = $request->file('season_banner');
            $file->store('products');
            $model->attachSeasonBanner($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $productPhoto = ProductPhoto::create($model->id, (int)$photoId, $sortPhoto);
                    $productPhoto->save();
                }
            }

            if (!empty($request->get('property', []))) {
                $properties = $request->get('property', []);
                foreach ($properties as $propertyId => $propertyValue) {
                    if ($propertyValue) {
                        $propertyProduct = PropertyProduct::create($propertyId, $model->id, $propertyValue);
                        $propertyProduct->save();
                    }
                }
            }
        }

        return $model;
    }

    public function update(ProductUpdateRequest $request, Product $model): Product
    {
        foreach (Product::query()->where('id', '>', 0)->get() as $product) {
            $product->slug = $this->generateSlug($product->title, $product->id);
            $product->save();
        }

        $model->edit(
            $request->title,
            $request->description,
            $request->specifications,
            $request->category_id,
            $request->brand_id,
            $request->code,
            $request->package,
            $request->count,
            $request->price * 100,
            $this->generateSlug($request->title, $model->id),
            (bool)$request->status,
            (bool)$request->chosen,
            (bool)$request->hot,
            (bool)$request->is_season,
            (bool)$request->is_new,
            $request->meta,
            $request->sale_price * 100 ?: null,
            $request->stars ?? 0
        );

        if ($request->hasFile('season_banner')) {
            if (Storage::exists('products/' . $model->season_banner)) {
                Storage::delete('products/' . $model->season_banner);
                $model->detachSeasonBanner();
            }

            $file = $request->file('season_banner');
            $file->store('products');
            $model->attachSeasonBanner($file->hashName());
        }

        $model->productProperties()->delete();
        if (!empty($request->get('property', []))) {
            $properties = $request->get('property', []);
            foreach ($properties as $propertyId => $propertyValue) {
                if ($propertyValue) {
                    $propertyProduct = PropertyProduct::create($propertyId, $model->id, $propertyValue);
                    $propertyProduct->save();
                }
            }
        }

        if ($model->save()) {
            $model->photos()->detach();
            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $productPhoto = ProductPhoto::create($model->id, $photoId, $sortPhoto);
                    $productPhoto->save();
                }
            }
        }

        return $model;
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->storeAs('products/import', date('Y-m-d_h-i') . '.' . $file->extension());
        }

        Excel::import(new ProductImport, Storage::path($path));
    }

    public function remove(Product $model)
    {
        if (!$model->delete())
            throw new RuntimeException('Deleting error');
    }

    public function generateSlug($title, $id = null)
    {
        $title = is_array($title) ? $title['az'] : $title;
        $slug = \Str::slug($title);
        $query = Product::query()->where('slug', $slug);

        if ($id) {
            $query->where('id', '<>', $id);
        }

        if ($query->exists()) {
            if ($id) {
                $slug .='-' . $id;
            } else {
                $slug .='-' . (Product::query()->max('id') + 1);
            }
        }

        return $slug;
    }
}
