<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use App\Repositories\CategoryRepository;
use App\Http\Requests\Category\{CategoryCreateRequest, CategoryUpdateRequest};

class CategoryService
{
    public function create(CategoryCreateRequest $request): Category
    {
        $model = Category::create(
            $request->title,
            $request->description,
            (bool) $request->status,
            (bool) $request->on_filter,
            \Str::slug($request->slug),
            $request->meta,
            $request->parent_id,
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('categories');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(CategoryUpdateRequest $request, Category $model): Category
    {
        $model->edit(
            $request->title,
            $request->description,
            (bool) $request->status,
            (bool) $request->on_filter,
            \Str::slug($request->slug),
            $request->meta,
            $request->parent_id,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('categories/' . $model->image)) {
                Storage::delete('categories/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('categories');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Category $model)
    {
        if (!$model->delete())
            throw new RuntimeException('Deleting error');
    }

    function arraySlug($requestSlug)
    {
        $slug = [];
        foreach ($requestSlug as $key => $slg) {
            $slug[$key] = \Str::slug($slg);
        }

        return $slug;
    }
}
