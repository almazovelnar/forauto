<?php

namespace App\Services\manager;

use App\Models\Config;
use App\Repositories\ConfigRepository;
use App\Http\Requests\Config\ConfigRequest;

class ConfigService
{
    public function create(ConfigRequest $request)
    {

        $model = Config::create(
            $request->param,
            $request->value,
            $request->default,
            $request->label,
            $request->type
        );

        $model->save();
        return $model;
    }

    public function update(Config $model, ConfigRequest $request)
    {
        $model->edit(
            $request->param,
            $request->value,
            $request->default,
            $request->label,
            $request->type
        );

        $model->save();
        return $model;
    }

    public function remove($model)
    {
        $model->delete();
    }
}
