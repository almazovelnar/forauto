<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Models\Page;
use RuntimeException;
use App\Repositories\PageRepository;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Page\{PageCreateRequest, PageUpdateRequest};

class PageService
{
    public function create(PageCreateRequest $request): Page
    {
        $model = Page::create(
            $request->title,
            $request->description,
            (bool) $request->status,
            (bool) $request->on_menu,
            $request->slug,
            +$request->template
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('pages');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(PageUpdateRequest $request, Page $model): Page
    {
        $model->edit(
            $request->title,
            $request->description,
            (bool) $request->status,
            (bool) $request->on_menu,
            $request->slug,
            +$request->template
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('pages/' . $model->image)) {
                Storage::delete('pages/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('pages');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }



    public function deleteImage(int $id)
    {
        $model = Page::findOrFail($id);
        Storage::delete('pages/' . $model->image);
        $model->detachImage();
        $model->save();
    }

    public function remove(Page $model)
    {
        if (!$model->delete())
            throw new RuntimeException('Deleting error');
    }
}
