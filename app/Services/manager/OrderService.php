<?php

namespace App\Services\manager;

use App\Models\Order;
use RuntimeException;
use App\Http\Requests\Order\OrderUpdateRequest;

class OrderService
{
    public function update(OrderUpdateRequest $request, $model)
    {
        $model->edit(
            $request->status
        );

        $model->save();
        return $model;
    }

    public function remove(Order $order)
    {
        if (!$order->delete())
            throw new RuntimeException('Deleting error');
    }
}
