<?php

namespace App\Services\manager;

use Cache;
use App\Models\MainInfo;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\MainInfo\MainInfoRequest;

class MainInfoService
{
    public function update(MainInfoRequest $request, MainInfo $model)
    {
        $model->edit(
            $request->title,
            $request->description,
            $request->keywords,
            $request->address,
            $request->name,
            array_filter($request->email),
            array_filter($request->phone),
            $request->fax,
            $request->location
        );

        if($request->hasFile('logo')) {
            if(Storage::exists('main-info/' . $model->logo)) {
                Storage::delete('main-info/' . $model->logo);
                $model->detachLogo();
            }
            $file = $request->file('logo');
            $file->store('main-info');
            $model->attachLogo($file->hashName());
        }

        if($request->hasFile('favicon')) {
            if(Storage::exists('main-info/' . $model->favicon)) {
                Storage::delete('main-info/' . $model->favicon);
                $model->detachFavicon();
            }
            $file = $request->file('favicon');
            $file->store('main-info');
            $model->attachFavicon($file->hashName());
        }

        $model->save();
        return $model;
    }

    public function deleteLogo(int $id)
    {
        $model = MainInfo::findOrFail($id);
        Storage::delete('main-info/' . $model->logo);
        $model->detachLogo();
        $model->save();
    }

    public function deleteFavicon(int $id)
    {
        $model = MainInfo::findOrFail($id);
        Storage::delete('main-info/' . $model->favicon);
        $model->detachFavicon();
        $model->save();
    }

    public function flushCache(): void
    {
        Cache::delete('site-info');
    }
}
