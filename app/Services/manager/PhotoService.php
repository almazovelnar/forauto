<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Enum\Dimension;
use App\Helpers\CodeGenerator;
use App\Models\Photo;
use App\Models\Folder;
use App\Models\Size;
use Illuminate\Http\Request;
use Intervention\Image\Image;
use Storage;

class PhotoService
{
    public function create(Request $request)
    {
        $infoImage = null;
        $storage   = \Storage::disk('public');

        if ($request->hasFile('file')) {
            $file   = $request->file('file');
            $author = \Auth::user();

            $model = Photo::create(
                $file->hashName(),
                $file->getClientOriginalName(),
                $author->id
            );

            $folder = Folder::find((int)$request->folderId);
            $model->path = $folder->slug ?? null;
            $saveFolder = 'photos/' . $model->path;

            if (!$storage->has($saveFolder)) {
                $storage->createDir($saveFolder);
            }

            if ($storage->putFile($saveFolder, $file)) {
                $infoImage = @exif_read_data($storage->path($saveFolder . '/' . $file->hashName()));

                $orjinalImage = $model->path . '/' . $file->hashName();
                $folder       = $model->path;
                $this->createPhotoBySize($folder, $orjinalImage, $file->hashName());


                if ($infoImage) {
                    $model->description = $infoImage['ImageDescription'] ?? null;
                }
            }

            $model->save();

            return $model;
        }

        return false;
    }

    public function update(Request $request, Photo $photo)
    {
        $photo->edit($request->title, $request->description, (bool)$request->is_exclusive, $request->published_at, $request->author);
        $photo->save();
    }

    public function createPhotoBySize($oldFolder, $oldPath, $filename)
    {

        $storagePhotos = Storage::disk('public');

        if ($storagePhotos->exists($oldPath)) {
            foreach ($this->getPhotoSizes() as $photoFolderName => $photoSizes) {
                $storage = \Storage::disk('public');
                $image   = \Image::make($storagePhotos->path($oldPath));
                $folder  = "photos/{$photoFolderName}/{$oldFolder}";
                if (!$storage->has($folder)) {
                    $storage->createDir($folder);
                }
                $path   = \Storage::path($folder . '/' . $filename);
                $width  = null;
                $height = null;

                if ($photoSizes['commonSize']) {
                    if ($image->getHeight() > $image->getWidth()) {
                        $height    = $photoSizes['commonSize'];
                        $watermark = 'watermarkH.png';
                    } else {
                        $width     = $photoSizes['commonSize'];
                        $watermark = 'watermark.png';
                    }
                } else {
                    if ($image->getHeight() > $image->getWidth()) {
                        $height    = $photoSizes['height'];
                        $watermark = 'watermarkH.png';
                    } else {
                        $width     = $photoSizes['width'];
                        $height    = $photoSizes['height'];
                        $watermark = 'watermark.png';
                    }
                }
                $newImage = $image->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
                if ($photoSizes['watermark']) {
                    $newImage = $newImage->insert(public_path('images/' . $watermark), 'center');
                }
                $newImage->save($path, 100);
            }


        }
    }

    function getPhotoSizes()
    {
        return
            [
                'list'  => ['height' => 275, 'width' => 420, 'watermark' => false, 'commonSize' => ''],
                'large' => ['height' => '', 'width' => '', 'watermark' => true, 'commonSize' => 750],
            ];
    }


    public function changeSizes(Request $request, Photo $photo)
    {
        foreach ($request->price as $dimension => $price) {
            $size = Size::query()
                ->firstOrCreate([
                    'photo_id' => $photo->id, 'type' => $dimension
                ]);

            $size->price = ($price * 100);
            $size->save();
        }
    }

    public function crop(Request $request, Photo $photo)
    {
        $x      = (int)$request->x;
        $y      = (int)$request->y;
        $x      = $x > 0 ? $x : 0;
        $y      = $y > 0 ? $y : 0;
        $width  = (int)$request->width;
        $height = (int)$request->height;

        \Image::make(public_path('storage/photos/' . $photo->filename))->crop($width, $height, $x, $y)->save();
    }

    protected function getPhotoResolution(string $filepath): array
    {
        $sizes = getimagesize($filepath);
        return [$sizes[0], $sizes[1]];
    }

    public function remove(Photo $photo)
    {
//        try {
        $photo->image->delete();
        $photo->forceDelete();
        \Storage::disk('photos')->delete(($photo->path ? $photo->path . '/' : '') . $photo->filename);
//        } catch (\Exception $e) {}
    }

    public function softRemove(Photo $photo)
    {
        $photo->delete();
    }

    public function restore(Photo $photo)
    {
        $photo->restore();
    }

    public function createFolder(Request $request)
    {
        if (!Folder::query()->where('slug', $request->slug)->first()) {
            $model = Folder::create($request->title, $request->slug);

            if ($model->save()) {
                \Storage::disk('photos')->createDir($model->slug);
                return $model;
            }
        }

        return false;
    }

    public function updateFolder(Request $request)
    {
        $model = Folder::find($request->folderId);
        $model->edit($request->title);

        if ($model->save()) {
            \Storage::disk('photos')->createDir($model->slug);
            return $model;
        }
        return false;
    }

    public function deleteFolder(Request $request)
    {
        $folder      = Folder::find($request->input('id'));
        $folderPhoto = Photo::query()->where('path', $folder->slug)->first();
        if (!$folderPhoto) {
            Folder::find($request->input('id'))->delete();
            \Storage::disk('photos')->deleteDir($folder->slug);
        } else {
            throw new \Exception('This folder have photos');
        }
    }
}
