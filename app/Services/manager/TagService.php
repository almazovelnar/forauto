<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Models\Tag;
use App\Helpers\StringHelper;
use App\Http\Requests\Tag\{TagCreateRequest, TagUpdateRequest};

class TagService
{
    /**
     * @param TagCreateRequest $request
     * @return Tag
     */
    public function create(TagCreateRequest $request): Tag
    {
        $model = Tag::create(
            $request->name,
            StringHelper::generateSlug($request->slug ?? $request->name, $request->language),
            $request->language,
            $request->link,
            0,
            (bool)$request->chosen
        );

        $model->save();

        return $model;
    }

    /**
     * @param TagUpdateRequest $request
     * @param Tag $model
     * @return Tag
     */
    public function update(TagUpdateRequest $request, Tag $model): Tag
    {
        $model->edit(
            $request->name,
            StringHelper::generateSlug($request->slug ?? $request->name, $request->language),
            $request->language,
            $request->link,
            (bool)$request->chosen
        );

        $model->save();

        return $model;
    }

    /**
     * @param int $id
     * @return void
     */
    public function remove(int $id): void
    {
        $model = Tag::findOrFail($id);
        $model->delete();
    }
}
