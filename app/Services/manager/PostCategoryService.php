<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\PostCategory as Category;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\PostCategory\{CategoryCreateRequest, CategoryUpdateRequest};

class PostCategoryService
{
    public function create(CategoryCreateRequest $request): Category
    {
        $model = Category::create(
            $request->title,
            $request->description,
            (bool) $request->status,
            \Str::slug($request->slug),
            $request->parent_id,
            $request->meta
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('post-categories');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(CategoryUpdateRequest $request, Category $model): Category
    {
        $model->edit(
            $request->title,
            $request->description,
            (bool) $request->status,
            \Str::slug($request->slug),
            $request->parent_id,
            $request->meta,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('post-categories/' . $model->image)) {
                Storage::delete('post-categories/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('post-categories');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Category $model)
    {
        if (!$model->delete())
            throw new RuntimeException('Deleting error');
    }
}
