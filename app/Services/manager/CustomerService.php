<?php

namespace App\Services\manager;

use App\Enum\CustomerStatus;
use App\Models\Customer;
use App\Repositories\CustomerRepository;
use App\Http\Requests\Customer\{CustomerChangePasswordRequest,
    CustomerCreateRequest,
    CustomerRegisterRequest,
    CustomerProfileUpdateRequest,
    CustomerUpdateRequest};
use RuntimeException;

class CustomerService
{
    public function create(CustomerCreateRequest $request)
    {
        $model = Customer::create(
            $request->name,
            $request->surname,
            $request->phone,
            $request->password,
            (bool) $request->status
        );
        $model->save();
        return $model;
    }

    public function update(CustomerUpdateRequest $request, Customer $model)
    {
        $model->edit(
            $request->name,
            $request->surname,
            $request->city,
            $request->address,
            $request->phone,
            $request->password,
            (bool) $request->status,
        );
        $model->save();
        return $model;
    }

    public function updateProfile(CustomerProfileUpdateRequest $request, $model)
    {
        /** @var Customer $model */
        $model->edit(
            $request->name,
            $request->surname,
            $request->phone,
            $request->email
        );

        $model->save();
        return $model;
    }

    public function changePassword(CustomerChangePasswordRequest $request, $model): Customer
    {
        /** @var Customer $model */
        $model->password = \Hash::make($request->password);
        $model->save();

        return $model;
    }

    public function deleted($customer)
    {
        $model = Customer::find($customer->id);
        $model->status = CustomerStatus::DELETED;
        $model->save();
        return $model;
    }

    public function remove(Customer $model)
    {
        if (!$model->delete())
            throw new \RuntimeException('Deleting error');
    }

    public function register(CustomerRegisterRequest $request): Customer
    {
        $user = Customer::create(
            $request->name,
            $request->surname,
            $request->phone,
            $request->password,
            true
        );

        $user->save();

        return $user;
    }
}
