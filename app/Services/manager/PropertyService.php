<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Property;
use App\Http\Requests\Property\{PropertyCreateRequest, PropertyUpdateRequest};

class PropertyService
{
    public function create(PropertyCreateRequest $request): Property
    {

        $model = Property::create(
            +$request->category_id,
            $request->title,
            !!$request->status,
            !!$request->on_filter,
            $request->slug
        );

        $model->save();

        return $model;
    }

    public function update(PropertyUpdateRequest $request, Property $model): Property
    {
        $model->edit(
            +$request->category_id,
            $request->title,
            !!$request->status,
            !!$request->on_filter,
            $request->slug,
        );

        $model->save();

        return $model;
    }

    public function remove(Property $model)
    {
        if (!$model->delete())
            throw new RuntimeException('Deleting error');
    }
}
