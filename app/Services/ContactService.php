<?php

namespace App\Services;

use App\Http\Requests\ContactMessageRequest;
use Illuminate\Support\Facades\Mail;

class ContactService
{
    public function sendMessage(ContactMessageRequest $request)
    {
        $text = "<strong>YENI SORĞU (Əlaqə bölməsi):</strong> <u>".$request->get('phone')."</u> \n\n";
        $text .= "<strong>Adı</strong> - " . $request->get('surname') .' '. $request->get('name') . "\n";
        $text .= "<strong>E-mail</strong> - " . $request->get('email') . "\n\n";
        $text .= "<strong>Qeyd:</strong>\n";
        $text .= $request->get('message') . "\n";
        $this->sendMessageToBot('-932454593', $text, '6174651092:AAHbx-h-JMRb8U5b1DF3Fh4P07LV2k89z-M');

    }

    function sendMessageToBot($chatID, $messaggio, $token) {
        $url = "https://api.telegram.org/bot" . $token . "/sendMessage?chat_id=" . $chatID;
        $url = $url . "&text=" . urlencode($messaggio) . '&parse_mode=html';
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $optArray);
        if(curl_exec($ch) === false)
        {
            dd(curl_error($ch));
        }
        curl_close($ch);
    }
}
