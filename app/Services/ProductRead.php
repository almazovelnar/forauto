<?php
declare(strict_types=1);

namespace App\Services;

use DB;
use App\Models\Product;

final class ProductRead
{
    public function incrementViewsCount(Product $product): void
    {
        $pageIsRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';

        if (!$pageIsRefreshed) {
            $product->views += 1;
            $product->save();
        }
    }
}
