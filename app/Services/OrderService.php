<?php

namespace App\Services;

use App\Enum\Delivery;
use App\Enum\OrderStatus;
use App\Enum\Payment;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrderService
{

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function create($request)
    {
        $order = Order::create(
            Auth::id(),
            $request->email,
            $request->name,
            $request->surname,
            $request->phone,
            $request->address,
            $request->notes,
            $request->delivery,
            $request->payment,
            cart()->count(),
            get_price(cart()->priceTotal(), false) * 100,
            OrderStatus::PENDING
        );
        $res = [];
        $items = [];
        DB::beginTransaction();

        try {
            if ($order->save()) {
                foreach (getCart() as $item) {
                    $product = Product::findOrFail($item->id);
                    $orderItem = OrderItem::create($order->id, $item->id, $item->qty, $item->price, $item->name);
                    $orderItem->code = $product->code;
                    $orderItem->save();
                    $items[$orderItem->id] = $orderItem;
                }
            }

            $this->order($order, $items);
            $res['success'] = true;
            $res['order_id'] = $order->order_id;
            DB::commit();
        } catch (\Exception $e) {
            \Log::debug($e);
            $res['success'] = false;
            DB::rollBack();
        }

        return $res;
    }

    public function order($order, $items)
    {
        $text = "<strong>YENI SIFARIŞ:</strong> <u>". $order->order_id ."</u> \n\n";
        $text .= "<strong>Sifarişçi</strong> - {$order->surname} {$order->name}". "\n";
        $text .= "<strong>Əlaqə nömrəsi</strong> - {$order->phone}". "\n";
        $text .= "<strong>E-mail</strong> - {$order->email}". "\n";
        $text .= "<strong>Çatdırılma növü</strong> - " . Delivery::get($order->delivery) . "\n";
        $text .= "<strong>Ödəniş növü</strong> - " . Payment::get($order->payment) . "\n";
        $text .= "<strong>Status</strong> - " . OrderStatus::get($order->status) . "\n";
        $text .= "<strong>ÜMUMI MƏBLƏĞ</strong> - " . get_price((int)$order->total_price * (int)$order->total_count) . "\n\n";

        $text .= "<strong>----- MƏHSULLAR -----</strong>\n";

        $key = 0;
        foreach ($items as $item) {
            if ($key != 0) {
                $text .= "-----\n";
            }
            $text .= "<strong>Məhsulun kodu</strong> - " . $item['code'] . "\n";
            $text .= "<strong>Məhsulun adı</strong> - " . $item['title'] . "\n";
            $text .= "<strong>Sayı</strong> - " . $item['count'] . "\n";
            $text .= "<strong>Qiyməti</strong> - " . get_price($item['price']) . "\n";
            $key++;
        }

        $this->sendMessageToBot($text);
    }

    public function fastBuy(Request $request, Product $product)
    {
        $text = "<strong>YENI SIFARIŞ:</strong> <u>".$request->get('phone')."</u> \n\n";
        $text .= "<strong>Məhsulun adı</strong> - $product->title". "\n";
        $text .= "<strong>Məhsulun kodu</strong> - $product->code". "\n";
        $text .= "<strong>Qiyməti</strong> - " . get_price($product->sale_price ?? $product->price). "\n";
        $text .= "<strong>Sayı</strong> - " . $request->get('count') . "\n\n";
        $text .= "<strong>ÜMUMI MƏBLƏĞ</strong> - " . get_price(($product->sale_price ?? $product->price) * (int)$request->get('count')) . "\n";
        $this->sendMessageToBot($text);
    }

    function sendMessageToBot($messaggio, $chatID = '-932454593',  $token = '6174651092:AAHbx-h-JMRb8U5b1DF3Fh4P07LV2k89z-M') {
        $url = "https://api.telegram.org/bot" . $token . "/sendMessage?chat_id=" . $chatID;
        $url = $url . "&text=" . urlencode($messaggio) . '&parse_mode=html';
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $optArray);
        curl_exec($ch);
        curl_close($ch);
    }
}
