<?php

namespace App\Helpers;

use Session;

class SessionHelper
{
    public static function hasModel($name, $model): bool
    {
        $has = false;

        if (!Session::has($name)) {
            Session::put($name, collect($model->id));
        } else {
            if (Session::get($name)->contains($model->id)) {
                $has = true;
            } else {
                $models = Session::get($name)->push($model->id);
                Session::put($name, $models);
            }
        }

        return $has;
    }
}
