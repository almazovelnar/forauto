<?php
declare(strict_types=1);

namespace App\Helpers;

use App\Models\Config as ConfigModel;


final class Config
{
    /** @var self */
    private static $instance;

    public static function instance(): self
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function get(string $key)
    {
        $config = ConfigModel::where('param', $key)->first();;

        if (!$config) return NULL;
        return $config->value ?? $config->default;
    }

}
