<?php

namespace App\Helpers;

use Illuminate\Support\Str;

final class StringHelper
{
    public static function clearSearchQuery(?string $string): string
    {
        $string = preg_replace('/[^\p{L}\:\d]/u', ' ', $string);
        return preg_replace('/(\s{1})\1*/ui', ' ', trim($string));
    }

    public static function parseYoutubeCode(?string $string = null)
    {
        if ($string) {
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $string, $matches);
        }

        return $matches[0] ?? $string;
    }

    public static function clearContent(string $content, ?int $limit = 40): string
    {
        $content = strip_tags($content);
        $content = str_replace('&nbsp;', ' ', $content);
        $content = preg_replace("/\r|\n/", "", $content);
        return html_entity_decode(\Str::limit($content, $limit));
    }

    public static function generateSlug(string $string, ?string $language = null, ?string $separator = '-')
    {
        $slug = Str::slug($string, $separator, $language ?? config('app.locale'));
        return $slug;
    }
}
