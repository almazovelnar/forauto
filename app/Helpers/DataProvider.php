<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Itstructure\GridView\Helpers\SortHelper;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class DataProvider extends EloquentDataProvider
{
    private $enabledFilters = [];

    public function __construct(Builder $query, array $enabledFilters = [])
    {
        $this->enabledFilters = $enabledFilters;
        parent::__construct($query);
    }

    public function selectionConditions(Request $request, bool $strictFilters = false): void
    {
        if ($request->get('sort', null)) {
            $this->query->orderBy(SortHelper::getSortColumn($request), SortHelper::getDirection($request));
        }

        if (!is_null($request->filters)) {
            foreach ($request->filters as $column => $value) {
                if (!in_array($column, $this->enabledFilters)) {
                    continue;
                }
                if (is_null($value)) {
                    continue;
                }

                if ($strictFilters) {
                    $this->query->where($column, '=', $value);
                } else {
                    $this->query->where($column, 'like', '%' . $value . '%');
                }
            }
        }
    }

}
