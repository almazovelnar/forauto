<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Faq extends Model
{
    use Cachable;

    public $timestamps = false;

    public static function create($language, $question, $answer, $status)
    {

        $model = new static();

        $model->language    = $language;
        $model->question       = $question;
        $model->answer     = $answer;
        $model->status       = $status;

        return $model;
    }

    public function edit($language, $question, $answer, $status)
    {
        $this->language = $language;
        $this->question    = $question;
        $this->answer  = $answer;
        $this->status    = $status;
    }
}
