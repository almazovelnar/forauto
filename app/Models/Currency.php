<?php
declare(strict_types=1);

namespace App\Models;


use Kalnoy\Nestedset\QueryBuilder;
use App\Queries\CachedQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $code
 */
class Currency extends Model
{
    use HasFactory, Cachable;

    protected $guarded = [];

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;
        }
        return new QueryBuilder($query);

        return new CachedQueryBuilder($query);
    }


    public function edit($title, $code, $value)
    {
        $this->title = $title;
        $this->code  = $code;
        $this->value = $value;
    }


}
