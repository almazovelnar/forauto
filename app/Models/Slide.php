<?php
declare(strict_types=1);

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use App\Queries\CachedQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $info
 * @property string $link
 * @property string $image
 * @property bool $status
 * @property int|null $parent_id
 */
class Slide extends Model
{
    use HasFactory, HasTranslations, Cachable, NodeTrait;

    public $translatable = ['title', 'description', 'info', 'link'];

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;

            return new QueryBuilder($query);
        }

        return new CachedQueryBuilder($query);
    }

    public static function create(array $title, array $description, array $info, array $link, bool $status, string $published_at)
    {
        $model = new static();

        $model->title        = $title;
        $model->description  = $description;
        $model->info         = $info;
        $model->link         = $link;
        $model->status       = $status;
        $model->published_at = $published_at;

        return $model;
    }

    public function edit(array $title, array $description, array $info, array $link, bool $status, string $published_at)
    {
        $this->title        = $title;
        $this->description  = $description;
        $this->info         = $info;
        $this->link         = $link;
        $this->status       = $status;
        $this->published_at = $published_at;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
