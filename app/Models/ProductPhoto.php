<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    public $timestamps = true;

    protected $table   = 'product_photo';
    protected $guarded = [];

    public static function create($productId, $photoId, $sortPhoto)
    {
        $model = new static();

        $model->product_id  = $productId;
        $model->photo_id = $photoId;
        $model->sort     = $sortPhoto;

        return $model;
    }
}
