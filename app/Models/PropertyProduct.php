<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyProduct extends Model
{
    protected $table      = 'property_product';
    public    $timestamps = false;

    public static function create(int $property_id, int $product_id, ?string $value = null)
    {
        $model              = new static();

        $model->property_id = $property_id;
        $model->product_id  = $product_id;
        $model->value       = $value;

        return $model;
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
