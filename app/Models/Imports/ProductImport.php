<?php

namespace App\Models\Imports;

use App\Enum\Status;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $i => $row)
        {
            if ($i == 0) continue;
            $item = array_filter($row->all());
            if (count($item) != 7) continue;

            $name = $item[0];
            $code = $item[1];
            $brandName = $item[2];
            $categoryName = $item[3];
            $count = intval(str_replace(',', '', $item[4]));
            $package = floatval(str_replace(',', '', $item[3]));
            $price = floatval(str_replace(',', '', $item[6])) * 100;
            $meta = [
                'title' => $name,
                'description' => null,
                'keywords' => null,
            ];


            if (!($product = Product::query()->where('code', $code)->first())) {
                if (!($brand = Brand::query()->whereRaw('LOWER(slug) = ?', [strtolower($brandName)])->first())) {
                    throw new \Exception('Not found brand: '. $brandName);
                }
                if (!($category = Category::query()->whereRaw('LOWER(slug) = ?', [strtolower($categoryName)])->first()))  {
                    throw new \Exception('Not found category: '. $categoryName);
                }

                $product = Product::import(
                    $name, $code, $brand->id, $category->id, $count, $package, $price, strtoupper(uniqid()), Status::ACTIVE, $meta
                );
            } else {
                $product->importEdit($count, $price);
            }

            $product->save();
        }
    }
}
