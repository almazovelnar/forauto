<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * @property string $filename
 * @property string $slug
 * @property string $original_filename
 * @property string $author
 * @property string $information
 * @property string $source
 * @property string $path
 */
class Photo extends Model
{
    use HasTranslations, Cachable, SoftDeletes;

    protected $translatable = ['title', 'description'];

    protected $fillable = ['code'];

    public static function create($filename, $originalFilename, $author_id, $title = null, $description = null)
    {
        $model = new static();

        $model->slug = strtoupper(uniqid());
        $model->filename = $filename;
        $model->original_filename = $originalFilename;
        $model->author_id = $author_id;
        $model->title = $title;
        $model->description = $description;

        return $model;
    }

    public function edit($title, $description, $is_exclusive, $published_at, $author_id = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->is_exclusive = $is_exclusive;
        $this->published_at = $published_at;
        if ($author_id) {
            $this->author_id = $author_id;
        }
    }

    public function post()
    {
        return $this->hasOneThrough(Post::class, Image::class,
            'photo_id', 'id', 'id', 'post_id')->orderBy('sorting');
    }

    public function author()
    {
        return $this->belongsTo(User::class,'author_id');
    }

    public function sizes()
    {
        return $this->hasMany(Size::class);
    }

    public function size(string $type)
    {
        return $this->hasMany(Size::class)->where('type', $type)->first();
    }

    public function image()
    {
        return $this->hasOne(Image::class);
    }
}
