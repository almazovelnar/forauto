<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_item';

    public static function create($order_id, $item_id, $count, $price, $title)
    {
        $model = new static();

        $model->order_id = $order_id;
        $model->item_id = $item_id;
        $model->price = $price;
        $model->title = $title;
        $model->count = $count;

        return $model;
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'item_id');
    }
}
