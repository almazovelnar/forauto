<?php
declare(strict_types=1);

namespace App\Models;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $specifications
 * @property integer $category_id
 * @property integer $brand_id
 * @property string $code
 * @property string $package
 * @property integer $count
 * @property integer $price
 * @property integer $sale_price
 * @property string $slug
 * @property bool $status
 * @property bool $chosen
 */
class Product extends Model
{
    use HasFactory, HasTranslations, SoftDeletes;

    public $translatable = ['title', 'description', 'specifications', 'meta'];

    protected $casts = [
        'meta' => 'object'
    ];

    public static function create(
        $title, $description, $specifications, $categoryId, $brandId, $code, $package, $count, $price, $slug, $status, $chosen, $hot, $is_season, $is_new, $meta, $salePrice, $stars
    ) {
        $model = new static();

        $model->title          = $title;
        $model->description    = $description;
        $model->specifications = $specifications;
        $model->category_id    = $categoryId;
        $model->brand_id       = $brandId;
        $model->code           = $code;
        $model->package        = $package;
        $model->count          = $count;
        $model->price          = $price;
        $model->sale_price     = $salePrice;
        $model->status         = $status;
        $model->chosen         = $chosen;
        $model->slug           = $slug;
        $model->hot            = $hot;
        $model->is_season      = $is_season;
        $model->is_new         = $is_new;
        $model->meta           = $meta;
        $model->stars          = $stars;

        return $model;
    }

    public static function import(
        $title, $code, $brandId, $categoryId, $count, $package, $price, $slug, $status, $meta
    ) {
        $model = new static();

        $model->title       = $title;
        $model->category_id = $categoryId;
        $model->brand_id    = $brandId;
        $model->code        = $code;
        $model->package     = $package;
        $model->count       = $count;
        $model->price       = $price;
        $model->status      = $status;
        $model->slug        = $slug;
        $model->meta        = $meta;

        return $model;
    }

    public function importEdit($count, $price)
    {
        $this->count = $count;
        $this->price = $price;
    }

    public function edit(
        $title, $description, $specifications, $categoryId, $brandId, $code, $package, $count, $price, $slug, $status, $chosen, $hot, $is_season, $is_new, $meta, $salePrice, $stars
    ) {
        $this->title          = $title;
        $this->description    = $description;
        $this->specifications = $specifications;
        $this->category_id    = $categoryId;
        $this->brand_id       = $brandId;
        $this->code           = $code;
        $this->package        = $package;
        $this->count          = $count;
        $this->price          = $price;
        $this->sale_price     = $salePrice;
        $this->status         = $status;
        $this->chosen         = $chosen;
        $this->slug          = $slug;
        $this->hot            = $hot;
        $this->is_season      = $is_season;
        $this->is_new         = $is_new;
        $this->meta           = $meta;
        $this->stars          = $stars;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function attachSeasonBanner($image)
    {
        $this->season_banner = $image;
    }

    public function detachSeasonBanner()
    {
        $this->season_banner = null;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function properties()
    {
        return $this->hasManyThrough(
            Property::class,
            PropertyProduct::class,
            'product_id',
            'id',
            'id',
            'property_id',
        );
    }

    public function productProperties()
    {
        return $this->hasMany(PropertyProduct::class);
    }

    public function photos(): BelongsToMany
    {
        return $this->belongsToMany(Photo::class, 'product_photo')->withTimestamps();
    }
}
