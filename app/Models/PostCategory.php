<?php
declare(strict_types=1);

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property bool $status
 * @property bool $chosen
 * @property int|null $parent_id
 */
class PostCategory extends Model
{
    use HasFactory, HasTranslations, Sluggable, Cachable, NodeTrait;

    public $translatable = ['title', 'description', 'meta'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['id', 'title', 'slug']
            ]
        ];
    }

    public function newEloquentBuilder($query)
    {
        return new QueryBuilder($query);
    }

    public static function create(array $title, array $description, bool $status, string $slug, $parentId = null, $meta)
    {
        $model = new static();

        $model->title       = $title;
        $model->description = $description;
        $model->slug        = $slug;
        $model->status      = $status;
        $model->meta        = $meta;

        if ($parentId) {
            $model->parent_id = $parentId;
        }

        return $model;
    }

    public function edit(array $title, array $description, bool $status, string $slug, $parentId = null, $meta)
    {
        $this->title       = $title;
        $this->description = $description;
        $this->status      = $status;
        $this->meta        = $meta;
        $this->slug        = $slug;
        $this->parent_id   = $parentId;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }

    public function posts()
    {
        return $this->hasMany(
            Post::class,
        )->where('language', app()->getLocale())
            ->where('published_at', '<=', now());
    }


    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
