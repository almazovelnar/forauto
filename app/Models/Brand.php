<?php
declare(strict_types=1);

namespace App\Models;

use App\Queries\CachedQueryBuilder;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $link
 * @property string $description
 * @property string $slug
 * @property bool $status
 * @property bool $chosen
 * @property int|null $parent_id
 */
class Brand extends Model
{
    use HasFactory, HasTranslations, Sluggable, Cachable, NodeTrait;

    public $translatable = ['title', 'link', 'description', 'meta'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['title', 'slug']
            ]
        ];
    }

    public function newEloquentBuilder($query)
    {
//        return new QueryBuilder($query);

        return new CachedQueryBuilder($query);
    }

    public static function create(int $categoryId, array $title, array $link, array $description, bool $status, bool $chosen, string $slug, $meta, $parentId = null)
    {
        $model = new static();

        $model->category_id = $categoryId;
        $model->title       = $title;
        $model->link        = $link;
        $model->description = $description;
        $model->slug        = $slug;
        $model->status      = $status;
        $model->chosen      = $chosen;
        $model->meta        = $meta;

        if ($parentId) {
            $model->parent_id = $parentId;
        }

        return $model;
    }

    public function edit(int $categoryId, array $title, array $link, array $description, bool $status, bool $chosen, ?string $slug, $meta, $parentId = null)
    {
        $this->category_id = $categoryId;
        $this->title       = $title;
        $this->link        = $link;
        $this->description = $description;
        $this->status      = $status;
        $this->chosen      = $chosen;
        $this->meta        = $meta;
        $this->slug        = $slug;
        $this->parent_id   = $parentId;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }

    public function posts()
    {
        return $this->hasMany(
            Post::class,
        )->where('language', app()->getLocale())
            ->where('published_at', '<=', now());
    }


    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
