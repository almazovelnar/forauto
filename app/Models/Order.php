<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded=[];

    public static function create($customerId, $login, $name, $surname, $phone, $address, $notes, $delivery, $payment, $totalCount, $totalPrice, $status)
    {
        $model = new static();

        $model->order_id = uniqid();
        $model->customer_id = $customerId;
        $model->email = $login;
        $model->name = $name;
        $model->surname = $surname;
        $model->phone = $phone;
        $model->address = $address;
        $model->notes = $notes;
        $model->total_count = $totalCount;
        $model->total_price = $totalPrice;
        $model->status = $status;

        $model->delivery = $delivery;
        $model->payment = $payment;

        return $model;
    }
    public function edit($status)
    {
        $this->status = $status;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->surname;
    }
}
