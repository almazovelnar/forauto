<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MainInfo extends Model
{
    use HasFactory, HasTranslations, Cachable;

    public $translatable = ['title', 'description', 'keywords', 'address'];

    public $casts = [
        'email' => 'array',
        'phone' => 'array',
    ];

    protected $table = 'main_info';

    public $timestamps = false;

    public function edit($title, $description, $keywords, $address, $name, $email, $phone, $fax, $location)
    {
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;
        $this->address = $address;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->fax = $fax;
        $this->location = $location;
    }

    public function attachLogo($logo)
    {
        $this->logo = $logo;
    }

    public function attachFavicon($favicon)
    {
        $this->favicon = $favicon;
    }

    public function detachLogo()
    {
        $this->logo = null;
    }

    public function detachFavicon()
    {
        $this->favicon = null;
    }
}
