<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $language
 * @property string $link
 * @property int $count
 * @property bool $chosen
 * @property bool $is_main
 *
 * @property-read string $withPrefix
 */
class Tag extends Model
{
    protected $guarded = [];

    public static function create(
        string $name,
        string $slug,
        string $lang,
        ?string $link = null,
        ?int $count = 0,
        ?bool $chosen = false
    )
    {
        $model = new static();

        $model->name        = $name;
        $model->slug        = $slug;
        $model->language    = $lang;
        $model->count       = $count;
        $model->link        = $link;
        $model->chosen      = +$chosen;

        return $model;
    }

    public function edit(
        string $name,
        string $slug,
        string $language,
        ?string $link,
        ?bool $chosen = false
    ): void
    {
        $this->name        = $name;
        $this->slug        = $slug;
        $this->language    = $language;
        $this->link = $link;
        $this->chosen      = +$chosen;
    }

    public function getWithPrefixAttribute(): string
    {
        return '#' . $this->name;
    }

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'post_tag')
            ->where('status', Status::ACTIVE);
    }
}
