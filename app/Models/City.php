<?php
declare(strict_types=1);

namespace App\Models;

use Str;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use App\Queries\CachedQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property bool $status
 * @property bool $chosen
 * @property int|null $parent_id
 */
class City extends Model
{
    use HasFactory, HasTranslations, Sluggable, Cachable, NodeTrait;

    public $translatable = ['title','slug'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['id', 'title', 'slug']
            ]
        ];
    }

    public function newEloquentBuilder($query)
    {
//        if (! $this->isCachable()) {
//            $this->isCachable = false;
//
//        }
        return new QueryBuilder($query);

//        return new CachedQueryBuilder($query);
    }

    public static function create(array $title, bool $status, bool $show, array $slug, $parentId = null)
    {
        $model         = new static();
        $model->title  = $title;
        $model->status = $status;
        $model->show   = $show;
        $model->slug   = $slug;

        if ($parentId) {
            $model->parent_id = $parentId;
        }
        return $model;
    }

    public function edit(array $title, bool $status, bool $show, array $slug, $parentId)
    {
        $this->title     = $title;
        $this->status    = $status;
        $this->show      = $show;
        $this->slug      = $slug;
        $this->parent_id = $parentId;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }



}
