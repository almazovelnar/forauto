<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $phone
 * @property string $email
 * @property string $city
 * @property string $address
 * @property string $password
 * @property bool $status
 *
 * @property string $fullName
 */
class Customer extends Authenticatable
{
    use Cachable, SoftDeletes;

    public static function create(string $name, string $surname, ?string $phone, string $password, bool $status)
    {
        $model = new static();

        $model->name     = $name;
        $model->surname  = $surname;
        $model->phone    = $phone;
        $model->password = Hash::make($password);
        $model->status   = $status;

        return $model;
    }

    public function edit(string $name, string $surname, string $phone, ?string $email = null, $status = null)
    {
        $this->name    = $name;
        $this->surname = $surname;
        $this->phone   = $phone;
        $this->email   = $email;

        if ($status) {
            $this->status = $status;
        }
    }

    public function getFullNameAttribute(): string
    {
        return $this->name . ' ' . $this->surname;
    }
}
