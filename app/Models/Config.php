<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Config extends Model
{
    use HasFactory, Cachable;

    public $timestamps = false;

    public static function create($param, $value, $default, $label, $type)
    {

        $model = new static();

        $model->param   = $param;
        $model->value   = $value;
        $model->default = $default;
        $model->label   = $label;
        $model->type    = $type;

        return $model;
    }

    public function edit($param, $value, $default, $label, $type)
    {
        $this->param   = $param;
        $this->value   = $value;
        $this->default = $default;
        $this->label   = $label;
        $this->type    = $type;
    }
}
