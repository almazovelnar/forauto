<?php
declare(strict_types=1);

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property bool $status
 * @property bool $chosen
 * @property bool $on_filter
 * @property int|null $parent_id
 */
class Category extends Model
{
    use HasFactory, HasTranslations, Sluggable, Cachable, NodeTrait;

    public $translatable = ['title', 'description', 'meta'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['id', 'title', 'slug']
            ]
        ];
    }

    public function newEloquentBuilder($query)
    {
//        if (! $this->isCachable()) {
//            $this->isCachable = false;
//
//        }
        return new QueryBuilder($query);

//        return new CachedQueryBuilder($query);
    }

    public static function create(array $title, array $description, bool $status, bool $chosen, string $slug, $meta, $parentId = null)
    {
        $model = new static();

        $model->title       = $title;
        $model->description = $description;
        $model->slug        = $slug;
        $model->status      = $status;
        $model->on_filter   = $chosen;
        $model->meta        = $meta;

        if ($parentId) {
            $model->parent_id = $parentId;
        }

        return $model;
    }

    public function edit(array $title, array $description, bool $status, bool $chosen, string $slug, $meta, $parentId = null)
    {
        $this->title       = $title;
        $this->description = $description;
        $this->status      = $status;
        $this->on_filter   = $chosen;
        $this->meta        = $meta;
        $this->slug        = $slug;
        $this->parent_id   = $parentId;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }

    public function posts()
    {
        return $this->hasMany(
            Post::class,
        )->where('language', app()->getLocale())
            ->where('published_at', '<=', now());
    }


    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'category_id');
    }

    public function brands()
    {
        return $this->hasMany(Brand::class, 'category_id');
    }

    public function mainProperties()
    {
        return $this->hasMany(Property::class, 'category_id')
            ->where('on_filter', true)
            ->take(2);
    }
}
