<?php

namespace App\Models;

use Str;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use App\Queries\CachedQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property int $template
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property bool $status
 * @property bool $on_menu
 */
class Page extends Model
{
    use HasFactory, HasTranslations, Sluggable, NodeTrait, Cachable;

    const DEFAULT = 1;
    const SERVICE = 2;

    public $translatable = ['title', 'description'];


    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;

            return new QueryBuilder($query);
        }

        return new CachedQueryBuilder($query);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['id', 'title', 'slug']
            ]
        ];
    }

    public static function create(array $title, array $description, bool $status, bool $onMenu, ?string $slug, int $template)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->slug = Str::slug($slug);
        $model->status = $status;
        $model->on_menu = $onMenu;
        $model->template = $template;

        return $model;
    }

    public function edit(array $title, array $description, bool $status, bool $onMenu, ?string $slug, int $template)
    {
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        $this->slug = Str::slug($slug);
        $this->on_menu = $onMenu;
        $this->template = $template;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
