<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $slug
 * @property bool $status
 * @property bool $on_filter
 */
class Property extends Model
{
    use HasFactory, HasTranslations, Cachable;

    public $translatable = ['title'];

    public static function create(int $category_id, array $title, bool $status, bool $onFilter, string $slug)
    {
        $model = new static();

        $model->category_id = $category_id;
        $model->title       = $title;
        $model->slug        = $slug;
        $model->status      = $status;
        $model->on_filter   = $onFilter;

        return $model;
    }

    public function edit(int $category_id, array $title, bool $status, bool $onFilter, string $slug)
    {
        $this->category_id = $category_id;
        $this->title       = $title;
        $this->status      = $status;
        $this->on_filter   = $onFilter;
        $this->slug        = $slug;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function relates()
    {
        return $this->hasMany(PropertyProduct::class)->groupBy('value');
    }
}
