<?php

namespace App\Models;

use Str;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasTranslations;

    public $translatable = ['title', 'description', 'slug', 'meta'];
    protected $casts = [
        'meta' => 'object'
    ];
    protected $dates = ['published_at'];

    public static function create($category_id, $title, $description, $meta, bool $status, bool $chosen, $date, ?array $slug)
    {
        $model = new static();

        $model->category_id = $category_id;
        $model->title = $title;
        $model->description = $description;
        $model->meta = $meta;
        $model->status = $status;
        $model->chosen = $chosen;
        $model->slug = $slug;
        $model->published_at = $date ?? now();

        return $model;
    }

    public function edit($category_id, $title, $description, $meta, bool $status, bool $chosen, $date, ?array $slug)
    {
        $this->category_id = $category_id;
        $this->title = $title;
        $this->description = $description;
        $this->meta = $meta;
        $this->status = $status;
        $this->chosen = $chosen;
        $this->slug = $slug;
        $this->published_at = $date ?? now();
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'post_tag'
        );
    }

    public function langTags()
    {
        return $this->belongsToMany(
            Tag::class,
            'post_tag'
        )->where('language', app()->getLocale());
    }
}
