<?php

namespace App\Repositories;

use App\Enum\OrderStatus;
use App\Models\Order;
use App\Models\OrderItem;

class OrderRepository
{
    public function get($id)
    {
        return Order::find($id);
    }
    public function getByTransaction($id)
    {
        return Order::where('transaction_id',$id)->first();
    }

    public function getByOrderId($orderId)
    {
        return Order::where('order_id', $orderId)->first();
    }

    public function getCustomerOrder($orderId, $customerId)
    {
        return Order::where('order_id', $orderId)->where('customer_id', $customerId)->first();
    }

    public function getOrderItems($orderId)
    {
        return OrderItem::where('order_id', $orderId)->get();
    }

    public function getByCustomer($userId)
    {
        return Order::query()->where('customer_id', $userId)->orderByDesc('id')->get();
    }

    public function newOrdersCount()
    {
        return Order::where('status', OrderStatus::PENDING)->count();
    }

    public function completedOrdersCount()
    {
        return Order::where('status', OrderStatus::COMPLETED)->count();
    }

    public function canceledOrdersCount()
    {
        return Order::where('status', OrderStatus::CANCELED)->count();
    }

    public function totalPostsCount()
    {
        return Order::all()->count();
    }

    public function completedOrders(?int $limit = 20, ?int $offset = 0)
    {
        return Order::query()
            ->where('status', OrderStatus::COMPLETED)
            ->skip($offset)
            ->take($limit)
            ->orderByDesc('created_at')
            ->get();
    }
}
