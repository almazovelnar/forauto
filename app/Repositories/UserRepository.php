<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function get($id)
    {
        return User::find($id);
    }

    public function getAuthors($limit)
    {
        return User::query()->whereHas(
            'roles', function ($q) {
            $q->where('name', 'author');
        })->limit($limit)->get();

    }
}
