<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Category;

class CategoryRepository
{
    public function get($id)
    {
        return Category::find($id);
    }

    public function getMenuItems(?int $limit = 12)
    {
        return $this->categoryQuery()->whereNull('parent_id')->where('chosen',1)->defaultOrder()->take($limit)->get();
    }
    public function getOthers(?int $limit = 12)
    {
        return $this->categoryQuery()->whereNull('parent_id')->where('chosen',0)->defaultOrder()->take($limit)->get();
    }
    public function getAll(?int $limit = 12)
    {
        return $this->categoryQuery()->whereNull('parent_id')->defaultOrder()->take($limit)->get();
    }

    public function all()
    {
        return $this->categoryQuery()
            ->defaultOrder()
            ->where('status', Status::ACTIVE)
            ->get();
    }

    public function getFilterCategories()
    {
        return $this->categoryQuery()
            ->defaultOrder()
            ->where('on_filter', true)
            ->get();
    }

    public function allParents()
    {
        return $this->categoryQuery()
            ->defaultOrder()
            ->whereNull('parent_id')
            ->where('status', Status::ACTIVE)
            ->get();
    }

    public function getBySlug(string $slug): Category
    {
        return $this->categoryQuery()->where('slug', $slug)->firstOrFail();
    }

    public function categoryQuery()
    {
        return Category::where('status', Status::ACTIVE);
    }

    public function getChosen(?int $limit = 12)
    {
        return $this->categoryQuery()
            ->defaultOrder()
            ->where('chosen', true)
            ->take($limit)
            ->get();
    }


    public function totalCount()
    {
        return Category::count();
    }
}
