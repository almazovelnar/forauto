<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Page;
use Illuminate\Database\Eloquent\Collection;

class PageRepository
{
    public function get($id)
    {
        return Page::find($id);
    }

    public function getMenuItems(?int $limit = 12)
    {
        return $this->pageQuery()
            ->whereNull('parent_id')
            ->where('on_menu', true)
            ->defaultOrder()
            ->take($limit)
            ->get();
    }

    public function getFooterItems(?int $limit = 12, ?int $template = null)
    {
        $query = $this->pageQuery()
            ->whereNull('parent_id')
            ->defaultOrder()
            ->take($limit);

        if ($template) {
            $query->where('template', $template);
        }

        return $query->get();
    }

    public function getBySlug(string $slug): Page
    {
        return $this->pageQuery()->where('slug', $slug)->firstOrFail();
    }

    public function all(?int $template = null): ?Collection
    {
        $query = $this->pageQuery();

        if ($template) {
            $query->where('template', $template);
        }

        return $query->get();
    }

    public function pageQuery()
    {
        return Page::where('status', Status::ACTIVE);
    }

    public function totalCount()
    {
        return Page::count();
    }
}
