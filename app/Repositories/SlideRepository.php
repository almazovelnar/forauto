<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Slide;

class SlideRepository
{
    public function get($id)
    {
        return Slide::find($id);
    }

    public function all()
    {
        return $this->thisQuery()->defaultOrder()->get();
    }

    public function thisQuery()
    {
        return Slide::query()
            ->where('status', Status::ACTIVE)
            ->where('published_at','<=',now());
    }
}
