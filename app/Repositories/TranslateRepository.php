<?php

namespace App\Repositories;

use App\Models\Translate;

class TranslateRepository
{
    public function get($id)
    {
        return Translate::find($id);
    }
}
