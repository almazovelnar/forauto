<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Brand;

class BrandRepository
{
    public function get($id)
    {
        return Brand::find($id);
    }

    public function getChosenBrands(?int $limit = 10)
    {
        return $this->thisQuery()->whereNull('parent_id')->where('chosen',1)->defaultOrder()->take($limit)->get();
    }

    public function all()
    {
        return $this->thisQuery()
            ->defaultOrder()
            ->get();
    }

    public function getByCategory(int $catId)
    {
        return $this->thisQuery()
            ->where('category_id', $catId)
            ->defaultOrder()
            ->get();
    }
    public function allParents()
    {
        return $this->thisQuery()
            ->defaultOrder()
            ->whereNull('parent_id')
            ->get();
    }

    public function thisQuery()
    {
        return Brand::query()->where('status', Status::ACTIVE);
    }

    public function getChosen(?int $limit = 12)
    {
        return $this->thisQuery()
            ->defaultOrder()
            ->where('chosen', true)
            ->take($limit)
            ->get();
    }


    public function totalCount()
    {
        return Brand::count();
    }
}
