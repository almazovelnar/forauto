<?php

namespace App\Repositories;

use App\Models\Customer;

class CustomerRepository
{
    public function get($id)
    {
        return Customer::find($id);
    }

    public function changePassword(string $email, string $password): bool
    {
        /** @var Customer $model */
        $model = Customer::query()->where('email', $email)->firstOrFail();
        $model->password = \Hash::make($password);
        return $model->save();
    }

    public function totalCustomersCount()
    {
        return Customer::all()->count();
    }
    public function getAllCompanys()
    {
        return Customer::where('is_company',1)->get();
    }
}
