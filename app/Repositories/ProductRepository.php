<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    public function get($id)
    {
        return Product::find($id);
    }

    public function getByCategory(int $catId, ?int $limit = 24)
    {
    }

    public function getWithFiltersPaginate(?array $filters, ?string $searchQuery, int $limit = 24)
    {
        $query = $this->thisQuery();

        if (!empty($filters)) {
            $lang = app()->getLocale();
            if (!empty($searchQuery)) {
                $query->where(DB::raw('json_extract(title, "$.' . $lang . '")'), 'LIKE', "%" . strtolower($searchQuery) . "%");
            }
            if (!empty($filters['sort'])) {
                $query = $this->detectSort($query, $filters['sort']);
            }
            if (!empty($filters['startPrice'])) {
                $query->where('price', '>=', $filters['startPrice'] * 100);
            }
            if (!empty($filters['endPrice'])) {
                $query->where('price', '<=', $filters['endPrice'] * 100);
            }
            if (!empty($filters['brand'])) {
                $brandIds = Brand::query()->select('id')->whereIn('slug', $filters['brand'])->get()->pluck('id')->all();
                if (!empty($brandIds)) {
                    $query->whereIn('brand_id', $brandIds);
                }
            }
        }

        return $query->paginate($limit);
    }

    private function detectSort($query, $sort)
    {
        switch ($sort) {
            case 'popularity':
                $query->orderByDesc('views');
                $query->orderByDesc('bought');
                break;
            case 'price':
                $query->orderBy('price');
                break;
            case 'price_desc':
                $query->orderByDesc('price');
                break;
            case 'name':
                $query->orderBy('title');
                break;
            case 'name_desc':
                $query->orderByDesc('title');
                break;
            default:
                $query->orderByDesc('id');
                break;
        }

        return $query;
    }

    public function getOurProducts(?int $limit = 12)
    {
        return $this->thisQuery()
            ->orderByRaw('rand()')
            ->limit($limit)
            ->get();
    }

    public function getChosenProducts(?int $limit = 12)
    {
        return $this->thisQuery()
            ->where('chosen', true)
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }

    public function getHotProducts(?int $limit = 12)
    {
        return $this->thisQuery()
            ->where('hot', true)
            ->whereNotNull('sale_price')
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }

    public function getSeasonProducts(?int $limit = 2)
    {
        return $this->thisQuery()
            ->where('is_season', true)
            ->whereNotNull('season_banner')
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }

    public function getNewProducts(?int $limit = 12)
    {
        return $this->thisQuery()
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }

    public function getTopProducts(?int $limit = 12)
    {
        return $this->thisQuery()
            ->where('chosen', true)
            ->inRandomOrder()
            ->limit($limit)
            ->get();
    }

    public function getRelatedProducts($catId, $brandId, $excludeId, $limit = 12)
    {
        return $this->thisQuery()
            ->where(function ($subQuery) use($catId, $brandId) {
                $subQuery->where('category_id', $catId)->orWhere('brand_id', $brandId);
            })
            ->where('id', '<>', $excludeId)
            ->where('count', '>', 0)
            ->orderByRaw('rand()')
            ->limit($limit)
            ->get();
    }

    public function getRelatedProductsByCategory($catId, $excludeId, $limit = 12)
    {
        return $this->thisQuery()
            ->where('category_id', $catId)
            ->where('id', '<>', $excludeId)
            ->orderByRaw('rand()')
            ->limit($limit)
            ->get();
    }

    public function getBySlug(string $slug)
    {
        return $this->thisQuery()->where('slug', $slug)->firstOrFail();
    }

    public function thisQuery()
    {
        return Product::query()
            ->where('status', Status::ACTIVE)
            ->where('count', '>', 0);
    }


    public function getByQuery(?string $searchQuery, ?string $sort = 'created_at desc', ?bool $isHot = false, ?int $limit = 24)
    {
        $query = $this->thisQuery();

        if (!empty($searchQuery)) {
            $query->where(DB::raw('LOWER(json_extract(title, "$.' . app()->getLocale() . '"))'), 'LIKE', "%" . strtolower($searchQuery) . "%");
        }

        $query->orderByRaw($sort);
        if ($isHot) {
            $query->where('hot', true);
        }


        return $query->paginate($limit);
    }
}
