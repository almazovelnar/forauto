<?php

namespace App\Repositories;

use App\Models\Tag;

class TagRepository
{
    public function get(string $slug, string $lang)
    {
        return Tag::where('slug', $slug)->where('language', $lang)->first();
    }

    public function getByQuery(string $q, string $lang, ?int $count = 3) {
        return Tag::where('name', 'like', "%$q%")
            ->where('language', $lang)
            ->where('count', '>=', $count)
            ->get();
    }

    public function getPopularsByLang(int $limit)
    {
        return Tag::query()
            ->where('language', app()->getLocale())
            ->orderByDesc('count')
            ->limit($limit)
            ->get();
    }
}
