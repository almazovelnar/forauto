<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Post;
use Carbon\Carbon;

class PostRepository
{
    public function get($id)
    {
        return Post::find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->postQuery()
                ->where('slug->'.app()->getLocale(), $slug)
                ->firstOrFail();
    }

    public function getPosts(int $limit, ?int $offset = 0)
    {
        return $this->postQuery()
            ->orderBy('published_at', 'DESC')
            ->skip($offset)
            ->take($limit)
            ->get();
    }

    public function getRelatedPosts($catId, $excludeId, $limit = 3)
    {
        return $this->postQuery()
            ->where('id', '<>', $excludeId)
            ->where('category_id', $catId)
            ->orderBy('published_at', 'DESC')
            ->take($limit)
            ->get();
    }

    public function getChosenPosts(?int $limit = 3)
    {
        return $this->postQuery()
            ->where('chosen', true)
            ->orderByDesc('published_at')
            ->limit($limit)
            ->get();
    }

    public function getPopularPosts(int $limit)
    {
        return $this->postQuery()
            ->where('published_at', '>', Carbon::now()->subMonth()->toDateTimeString())
            ->orderBy('views', 'DESC')
            ->orderBy('published_at', 'DESC')
            ->take($limit)
            ->get();
    }

    public function getPostsByCatWithPaginate(int $catId, int $paginate)
    {
        return $this->postQuery()
            ->where('category_id', $catId)
            ->orderBy('published_at', 'DESC')
            ->paginate($paginate);
    }

    public function getPostsByTagWithPaginate($tagId, int $limit = 12)
    {
        return $this->postQuery()
            ->join('post_tag as pt', 'pt.post_id', '=', 'posts.id')
            ->join('tags as t', 'pt.tag_id', '=', 't.id')
            ->where('pt.tag_id', $tagId)
            ->paginate($limit);
    }

    public function getPostsWithPaginate(int $paginate)
    {
        return $this->postQuery()
            ->orderBy('published_at', 'DESC')
            ->paginate($paginate);
    }

    private function postQuery()
    {
        return Post::query()
            ->where('status', Status::ACTIVE)
            ->where('published_at', '<=', now());
    }
}
