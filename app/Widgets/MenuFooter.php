<?php

namespace App\Widgets;

use App\Models\Page;
use App\Repositories\PageRepository;
use Arrilot\Widgets\AbstractWidget;

class MenuFooter extends AbstractWidget
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
        parent::__construct();
    }


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.menu-footer', [
            'config'        => $this->config,
            'defaultPages'  => $this->pageRepository->all(Page::DEFAULT),
            'servicePages'  => $this->pageRepository->all(Page::SERVICE),
        ]);
    }
}
