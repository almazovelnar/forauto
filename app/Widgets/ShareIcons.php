<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Cache;

class ShareIcons extends AbstractWidget
{
    protected $config = [];
    private const SHARE_URLS = [
        'facebook'      => 'https://www.facebook.com/sharer/sharer.php?u={url}',
//        'vkontakte'     => 'https://vk.com/share.php?url={url}',
//        'odnoklassniki' => 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl={url}',
        'twitter'       => 'https://twitter.com/share?url={url}&text={text}',
//        'instagram'      => 'https://t.me/share/url?url={url}&text={text}',
        'telegram'      => 'https://t.me/share/url?url={url}&text={text}',
        'whatsapp'      => 'https://wa.me/?text={url}',
//        'viber'         => 'viber://forward?text={text} {url}'
    ];
    public $post;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.share-icons', [
            'shareUrls' => self::SHARE_URLS,
            'post'      => $this->config['post']
        ]);
    }
}
