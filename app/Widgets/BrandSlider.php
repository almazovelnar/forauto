<?php

namespace App\Widgets;

use App\Repositories\BrandRepository;
use Arrilot\Widgets\AbstractWidget;

class BrandSlider extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    /**
     * @var BrandRepository
     */
    private $brandRepository;


    public function __construct(BrandRepository $brandRepository, array $config = [])
    {
        parent::__construct($config);
        $this->brandRepository = $brandRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $brands = $this->brandRepository->getChosenBrands();

        return view('widgets.brand_slider', [
            'config' => $this->config,
            'brands' => $brands,
        ]);
    }
}
