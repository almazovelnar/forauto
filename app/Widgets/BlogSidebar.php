<?php

namespace App\Widgets;

use App\Repositories\PostCategoryRepository;
use App\Repositories\PostRepository;
use App\Repositories\TagRepository;
use Arrilot\Widgets\AbstractWidget;

class BlogSidebar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var PostCategoryRepository
     */
    private $postCategoryRepository;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    public function __construct(
        PostCategoryRepository $postCategoryRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        array $config = []
    )
    {
        parent::__construct($config);
        $this->postRepository = $postRepository;
        $this->postCategoryRepository = $postCategoryRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.blog_sidebar', [
            'config' => $this->config,
            'categories' => $this->postCategoryRepository->all(),
            'topPosts' => $this->postRepository->getPopularPosts(3),
            'tags' => $this->tagRepository->getPopularsByLang(7),
        ]);
    }
}
