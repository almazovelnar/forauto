<?php

namespace App\Widgets;

use App\Repositories\CategoryRepository;
use Arrilot\Widgets\AbstractWidget;

class MainFilter extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        array              $config = []
    ) {
        parent::__construct($config);
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.main_filter', [
            'config'     => $this->config,
            'categories' => $this->categoryRepository->getFilterCategories()
        ]);
    }
}
