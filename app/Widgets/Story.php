<?php

namespace App\Widgets;

use App\Repositories\PostRepository;
use App\Repositories\StoryRepository;
use Arrilot\Widgets\AbstractWidget;

class Story extends AbstractWidget
{
    protected $config = [];
    private $storyRepository;

    public function __construct(StoryRepository $storyRepository)
    {
        $this->storyRepository=$storyRepository;
    }


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.story', [
            'config' => $this->config,
            'story' =>$this->storyRepository->getMain(),
        ]);
    }
}
