<?php

namespace App\Widgets;

use App\Models\Tag;
use Arrilot\Widgets\AbstractWidget;

class SeoWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.seo_widget', [
            'config' => $this->config,
            'tags' => Tag::query()->where('chosen', true)->orderByDesc('id')->limit(12)->get()
        ]);
    }
}
