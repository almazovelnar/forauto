<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Cache;
use App\Services\manager\CurrencyService;

class Currency extends AbstractWidget
{
    protected $config = [];
    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(CurrencyService $currencyService, array $config = [])
    {
        parent::__construct($config);
        $this->currencyService = $currencyService;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $currencyList = $this->currencyService->getList();
        $currencyCount = count($currencyList);
        $limit = $currencyCount * 2;

        $data = Cache::remember('currency', 60 * 24, function () use($limit) {
            return \App\Models\Currency::query()->limit($limit)->orderbyDesc('id')->get();
        });

        $currencies = [];
        foreach ($data as $value) {
            $code = strtoupper($value->code);
            if (isset($currencies[$code]) && count($currencies[$code]) >= 2)
                continue;
            $currencies[$code][] = $value->value;
        }

        return view('widgets.currency', [
            'currencies' => $currencies
        ]);
    }
}
