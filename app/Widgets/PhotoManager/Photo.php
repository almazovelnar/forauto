<?php

namespace App\Widgets\PhotoManager;

use Arrilot\Widgets\AbstractWidget;

class Photo extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $modal = \View::make('widgets.photo_manager._modals', ['mode' => 'single']);

        return view('widgets.photo_manager.single', [
            'config' => $this->config,
            'modal' => $modal,
            'filename' => $this->config['filename']
        ]);
    }
}
