<?php

namespace App\Widgets;

use App\Repositories\PageRepository;
use Arrilot\Widgets\AbstractWidget;

class MenuMobileHeader extends AbstractWidget
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
        parent::__construct();
    }


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.menu-mobile-header', [
            'config'     => $this->config,
            'pages'      => $this->pageRepository->getMenuItems(5)
        ]);
    }
}
