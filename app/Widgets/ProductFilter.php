<?php

namespace App\Widgets;

use App\Models\Property;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Http\Request;

class ProductFilter extends AbstractWidget
{
    const TYPE_SIDEBAR = 'type-sidebar';
    const TYPE_LIST = 'type-list';

    protected $config = [];
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var BrandRepository
     */
    private $brandRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        BrandRepository $brandRepository,
        array $config = []
    )
    {
        parent::__construct($config);
        $this->categoryRepository = $categoryRepository;
        $this->brandRepository = $brandRepository;
    }

    public function run()
    {
        $category = $this->categoryRepository->getBySlug(request('slug'));
        $properties = Property::query()->where('category_id', $category->id)->where('status', true)->get();

        return view('widgets.product-filter.sidebar-filter', [
            'config' => $this->config,
            'properties' => $properties,
            'brands' => $this->brandRepository->getByCategory($category->id)
        ]);
    }
}
