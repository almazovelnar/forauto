<?php
declare(strict_types=1);

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Repositories\PostRepository;

class BlogWidget extends AbstractWidget
{
    protected $config = [];

    /**
     * @var PostRepository
     */
    private $blogRepository;

    public function __construct(
        PostRepository $blogRepository,
        $config
    )
    {
        parent::__construct($config);

        $this->blogRepository = $blogRepository;
    }

    public function run()
    {
        //

        return view('widgets.blog_widget', [
            'posts' => $this->blogRepository->getChosenPosts(),
            'config' => $this->config,
        ]);
    }
}
