<?php

namespace App\Widgets;

use App\Models\Product;
use App\Repositories\BrandRepository;
use Arrilot\Widgets\AbstractWidget;

class ProductSidebar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    /**
     * @var BrandRepository
     */
    private $brandRepository;


    public function __construct(
        BrandRepository $brandRepository,
        array $config = []
    )
    {
        parent::__construct($config);
        $this->brandRepository = $brandRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $brands = $this->brandRepository->all();

        return view('widgets.product_sidebar', [
            'config' => $this->config,
            'brands' => $brands,
            'maxPrice' => Product::query()->select('price')->max('price')
        ]);
    }
}
