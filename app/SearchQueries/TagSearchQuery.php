<?php
declare(strict_types=1);

namespace App\SearchQueries;

use App\Models\Tag;

class TagSearchQuery implements Query
{
    public function search(array $filters = [])
    {
        $query = Tag::query()->orderBy('name');

        if (!empty($filters['name'])) {
            $query->where('name', 'LIKE', '%' . $filters['name'] . '%');
        }

        if (!empty($filters['language'])) {
            $query->where('language', $filters['language']);
        }

        return $query;
    }

    public function query($filters = [])
    {
        // TODO: Implement query() method.
    }
}
