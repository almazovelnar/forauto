<?php

namespace App\SearchQueries;

use App\Models\Customer;

class CustomerQuery implements Query
{
    public function query($filters = [])
    {
        $query = Customer::query();

        if (!empty($filters['title'])) {
            $this->applyCustomerNameFilter($query, $filters['title']);
        }

        if (!empty($filters['email'])) {
            $query->where('email', 'like', '%' . $filters['email'] . '%');
        }

        if (isset($filters['status']) && $filters['status'] !== '') {
            $query->where('status', $filters['status']);
        }

        return $query;
    }

    private function applyCustomerNameFilter(&$query, string $name)
    {
        if (strpos($name, ' ') !== false) {
            [$name, $surname] = explode(' ', $name);
        } else {
            $surname = $name;
        }

        $query->where(function ($query) use ($name, $surname) {
            if ($name == $surname) {
                $query->where(function($q) use($name, $surname) {
                    $q->where('name', 'like', '%' . $name . '%')
                        ->orWhere('surname', 'like', '%' . $surname . '%');
                });

            } else {
                $query->where(function($q) use($name, $surname) {
                    $q->where('name', 'like', '%' . $name . '%')
                        ->where('surname', 'like', '%' . $surname . '%');
                });
            }
        });
    }
}
