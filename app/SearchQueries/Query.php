<?php

namespace App\SearchQueries;

interface Query
{
    public function query($filters = []);
}
