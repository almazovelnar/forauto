<?php

namespace App\SearchQueries;

use Auth;
use App\Models\User;

class UserQuery implements Query
{
    public function query($filters = [])
    {
        $query = User::query()->orderByDesc('id');

        if (Auth::user()->email != 'root@admin.com') {
            $query->where('email', '<>', 'root@admin.com');
        }

        if (!empty($filters['name'])) {
            $query->where('name', 'like', '%' . $filters['name'] . '%');
        }

        if (!empty($filters['email'])) {
            $query->where('email', 'like', '%' . $filters['email'] . '%');
        }

        if (isset($filters['status']) && $filters['status'] !== '') {
            $query->where('status', $filters['status']);
        }

        if (!empty($filters['role'])) {
            $query->whereHas(
        'roles', function($q) use ($filters) {
                    $q->where('name', $filters['role']);
                }
            );
        }

        return $query;
    }
}
