<?php

namespace App\Http\Controllers\Backend;

use App\Enum\Dimension;
use App\Models\Folder;
use App\Models\Photo;
use App\Models\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\manager\PhotoService;
use Spatie\TranslationLoader\TranslationLoaders\Db;

class PhotoController extends Controller
{
    /**
     * @var PhotoService
     */
    private $photoService;

    public function __construct(PhotoService $photoService)
    {
        $this->photoService = $photoService;
    }

    public function index()
    {
        $request = request();

        $folder = Folder::find($request->input('folderId'));

        $photos = Photo::query()->orderByDesc('id')->with('author')->take(25);
        if ($request->input('folderId') && (int)$request->input('folderId') > 0) {
            $photos->where('path', $folder->slug);
        } elseif ($request->input('folderId') && $request->input('folderId') == '-1') {
            $photos->whereNull('path');
        }
        if ($request->input('id')) {
            $photos->where('id', '<', $request->input('id'));
        }
        $photos = $photos->get();
        $photosView = \View::make('backend.photos._index', [
            'photos' => $photos
        ])->render();
        return response()->json([
            'success' => true,
            'photos' => $photosView
        ]);
    }

    public function trashIndex()
    {
        $photos = Photo::query()
            ->whereNotNull('deleted_at')
            ->orderByDesc('deleted_at')
            ->withTrashed()
            ->get();

        $photosView = \View::make('backend.photos._index', [
            'photos' => $photos
        ])->render();

        return response()->json([
            'success' => true,
            'photos' => $photosView
        ]);
    }

    public function create(Request $request)
    {
        $photo = $this->photoService->create($request);
        return response()->json([
            'success' => true,
            'photo' => \View::make('backend.photos._photo', [
                'photo' => $photo
            ])->render(),
        ]);
    }

    public function update()
    {
        $request = request();
        $photo = Photo::find($request->input('id'));

        if ($request->isMethod('post')) {
            try {
                $this->photoService->update($request, $photo);
                return response()->json(['success' => true]);
            } catch (\Exception $e) {
                return response()->json(['success' => false]);
            }
        }

        return \View::make('backend.photos._update', ['photo' => $photo]);
    }

    public function getSize()
    {
        $request = request();
        $photo = Photo::find($request->input('id'));

        if ($request->isMethod('post')) {
            try {
                $dimensions = array_map(function ($data) use ($photo) {
                    if ($size = Size::query()->where('photo_id', $photo->id)->where('type', $data['id'])->first()) {
                        $data['price'] = $size->price;
                    }
                    return $data;
                }, Dimension::getList()->toArray());

                return response()->json([
                    'success' => true,
                    'html' => \View::make('backend.photos._size', ['dimensions' => $dimensions])->render()
                ]);
            } catch (\Exception $e) {
                return response()->json(['success' => false]);
            }
        }
    }

    public function size()
    {
        $request = request();
        $photo = Photo::find($request->input('id'));

        if ($request->isMethod('post')) {
            try {
                $this->photoService->changeSizes($request, $photo);
                return response()->json(['success' => true]);
            } catch (\Exception $e) {
                return response()->json(['success' => false]);
            }
        }

        return \View::make('backend.photos._update', ['photo' => $photo]);
    }

    public function crop()
    {
        $request = request();
        $photo = Photo::find($request->input('id'));

        try {
            $this->photoService->crop($request, $photo);
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => $e->getMessage()]);
        }
    }

    public function renderGallery()
    {
        return \View::make('backend.photos._gallery', ['data' => \request()->input('data')]);
    }

    public function softDelete()
    {
        $request = request();
        $photo = Photo::find($request->input('id'));

        try {
            $this->photoService->softRemove($photo);
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false]);
        }
    }

    public function delete()
    {
        $request = request();
        $photo = Photo::query()->where('id', $request->input('id'))->withTrashed()->first();

        try {
            $this->photoService->remove($photo);
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false]);
        }
    }

    public function restore()
    {
        $request = request();
        $photo = Photo::query()->where('id', $request->input('id'))->withTrashed()->first();

        try {
            $this->photoService->restore($photo);
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false]);
        }
    }

    public function folderCreate()
    {
        $request = request();

        if ($request->isMethod('post')) {
            try {
                $folder = $this->photoService->createFolder($request);
                return response()->json([
                    'success' => true,
                    'folder' => \View::make('backend.photos._folder', ['folder' => $folder])->render()
                ]);
            } catch (\Exception $e) {
                return response()->json(['success' => false]);
            }
        }

        return \View::make('backend.photos._create-folder');
    }

    public function folderUpdate()
    {
        $request = request();

        $folder = Folder::find($request->input('id'));

        if ($request->isMethod('post')) {
            try {
                $folder = $this->photoService->updateFolder($request);
                return response()->json([
                    'success' => true,
                    'folder' => \View::make('backend.photos._folder', ['folder' => $folder])->render()
                ]);
            } catch (\Exception $e) {
                return response()->json(['success' => false]);
            }
        }

        return \View::make('backend.photos._update-folder', ['folder' => $folder, 'folderId' => $folder->id]);
    }

    public function folderDelete()
    {
        try {
            $this->photoService->deleteFolder(request());
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false]);
        }
    }
}
