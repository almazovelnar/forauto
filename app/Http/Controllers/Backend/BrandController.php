<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Brand;
use App\Helpers\NodeHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\BrandService;
use App\Repositories\CategoryRepository;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Brand\{BrandCreateRequest, BrandUpdateRequest};

class BrandController extends Controller
{
    private $brandService;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        BrandService       $brandService
    ) {
        $this->brandService       = $brandService;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $query        = Brand::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.brands.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.brands.create', [
            'parents'    => (new NodeHelper(Brand::class))->map(),
            'categories' => $this->categoryRepository->all()
        ]);
    }

    public function store(BrandCreateRequest $request)
    {
        try {
            $this->brandService->create($request);
            return redirect()->route('admin.brands.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.brands.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function show(Brand $brand)
    {
        return view('backend.brands.view', [
            'brand' => $brand
        ]);
    }

    public function edit(Brand $brand)
    {
        $parents = (new NodeHelper($brand))->map();

        return view('backend.brands.update', [
            'brand'      => $brand,
            'parents'    => $parents,
            'categories' => $this->categoryRepository->all()
        ]);
    }

    public function update(BrandUpdateRequest $request, Brand $brand)
    {
        try {
            $this->brandService->update($request, $brand);
            return redirect()->route('admin.brands.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.brands.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Brand $brand): RedirectResponse
    {
        try {
            $this->brandService->remove($brand);
            return redirect()->route('admin.brands.index')
                ->with('success', trans('form.success.delete'));
        } catch (RuntimeException $e) {
            return redirect()->route('admin.brands.index')
                ->with('error', trans('form.error.delete'));
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $brand = Brand::findOrFail($id);
        ($type == 'up') ? $brand->up() : $brand->down();

        return redirect()->back();
    }
}
