<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Order;
use App\Models\OrderItem;
use App\Enum\OrderStatus;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use App\Services\manager\OrderService;
use App\Http\Requests\Order\OrderUpdateRequest;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class OrderController extends Controller
{
    private $orderService;
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderService $orderService, OrderRepository $orderRepository)
    {
        $this->orderService    = $orderService;
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        $dataProvider = new EloquentDataProvider(Order::query()->orderByDesc('id'));

        return view('backend.orders.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function show(Order $order)
    {
        return view('backend.orders.view', [
            'order' => $order
        ]);
    }

    public function edit(Order $order)
    {
        return view('backend.orders.update', [
            'order' => $order
        ]);
    }

    public function update(OrderUpdateRequest $request, Order $order)
    {
        try {
            $this->orderService->update($request, $order);
            return redirect()->route('admin.orders.index')
                ->with('success', trans('admin.form.success_save'));
        } catch (Exception $e) {
            return redirect()->route('admin.orders.index')
                ->with('error', trans('admin.form.error_save'));
        }
    }

    public function destroy(Order $order)
    {
        try {
            $this->orderService->remove($order);
            return redirect()->route('admin.orders.index')
                ->with('success', trans('admin.form.success_deleted'));
        } catch (Exception $e) {
            return redirect()->route('admin.orders.index')
                ->with('error', trans('admin.form.cant_delete'));
        }
    }

    public function reversal($tr_id)
    {
        $order = $this->orderRepository->getByTransaction($tr_id);

        if ($order && $order->status == OrderStatus::COMPLETED) {
            $client = new Client(['userName' => Payment::USERNAME, 'password' => Payment::PASSWORD]);
            $client->reverseOrder($tr_id);
            $order->status=OrderStatus::REVERSED;
            $order->save();
        }
    }

    public function changeOrderItemCode()
    {
        $orderItem = OrderItem::find(request()->input('id'));
        $orderItem->code = request()->input('code');
        $orderItem->save();
    }


}
