<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\Models\Imports\ProductImport;
use App\Models\Photo;
use App\Models\Property;
use App\Models\PropertyProduct;
use App\Repositories\BrandRepository;
use App\Repositories\CategoryRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use RuntimeException;
use App\Models\Product;
use App\Helpers\NodeHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\ProductService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Product\{ProductCreateRequest, ProductUpdateRequest};

class ProductController extends Controller
{
    private $productService;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var BrandRepository
     */
    private $brandRepository;

    public function __construct(
        ProductService $productService,
        CategoryRepository $categoryRepository,
        BrandRepository $brandRepository
    )
    {
        $this->productService = $productService;
        $this->categoryRepository = $categoryRepository;
        $this->brandRepository = $brandRepository;
    }

    public function index()
    {
        $query = Product::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.products.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $categories = $this->categoryRepository->all();
        $brands = $this->brandRepository->all();
        $properties = Property::query()->where('category_id', old('category_id'))->get();

        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }

        return view('backend.products.create', compact('categories', 'brands', 'properties', 'photos'));
    }

    public function store(ProductCreateRequest $request)
    {
        try {
            $this->productService->create($request);
            return redirect()->route('admin.products.index')
                ->with('success', trans('admin.form.success_save'));
        } catch (Exception $e) {
            return redirect()->route('admin.products.index')
                ->with('error', trans('admin.form.error_save'));
        }
    }

    public function show(Product $product)
    {
        return view('backend.products.view', [
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        $categories = $this->categoryRepository->all();
        $brands = $this->brandRepository->all();
        $propertiesData = \DB::table('property_product as pp')
            ->select(['pp.property_id as id', 'pp.value as value'])
            ->join('properties as p', 'pp.property_id', '=', 'p.id')
            ->where('pp.product_id', $product->id)
            ->get()
            ->pluck('value', 'id');

        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }
        if (empty($photos)) {
            $photos = $product->photos;
        }

        return view('backend.products.update', compact('product', 'categories', 'brands', 'photos', 'propertiesData'));
    }

    public function import()
    {
        return view('backend.products.import');
    }

    public function importProducts(Request $request)
    {
        try {
            $this->productService->import($request);
            return redirect()->route('admin.products.index')
                ->with('success', trans('form.success.import'));
        } catch (Exception $e) {
            return redirect()->route('admin.products.index')
                ->with('error', $e->getMessage());
        }
    }

    public function update(ProductUpdateRequest $request, Product $product)
    {
        try {
            $this->productService->update($request, $product);
            return redirect()->route('admin.products.index')
                ->with('success', trans('admin.form.success_save'));
        } catch (Exception $e) {
            return redirect()->route('admin.products.index')
                ->with('error', $e->getMessage());
        }
    }

    public function destroy(Product $product): RedirectResponse
    {
        try {
            $this->productService->remove($product);
            return redirect()->route('admin.products.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.products.index')->with('error', $e->getMessage());
        }
    }
}
