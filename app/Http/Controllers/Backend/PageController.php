<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Page;
use RuntimeException;
use App\Helpers\NodeHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\PageService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Page\{PageCreateRequest, PageUpdateRequest};

class PageController extends Controller
{
    private $pageService;

    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }

    public function index()
    {
        $query = Page::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.pages.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $parents = (new NodeHelper(Page::class))->map();

        return view('backend.pages.create', compact('parents'));
    }

    public function store(PageCreateRequest $request)
    {
        if ($request->validated()) {
            try {
                $this->pageService->create($request);
                return redirect()->route('admin.pages.index')
                    ->with('success', trans('form.success.save'));
            } catch (Exception $e) {
                dd($e);
            }
        }
    }

    public function show(Page $page)
    {
        return view('backend.pages.view', [
            'page' => $page
        ]);
    }

    public function edit(Page $page)
    {
        $parents = (new NodeHelper(Page::class))->map($page->id);

        return view('backend.pages.update', compact('page', 'parents'));
    }

    public function update(PageUpdateRequest $request, Page $page)
    {
        if ($request->validated()) {
            try {
                $this->pageService->update($request, $page);
                return redirect()->route('admin.pages.index')
                    ->with('success', trans('form.success.save'));
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
    }

    public function destroy(Page $page): RedirectResponse
    {
        try {
            $this->pageService->remove($page);
            return redirect()->route('admin.pages.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.pages.index')->with('error', $e->getMessage());
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Page::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }

    public function deleteImage(int $id)
    {
        try {
            $this->pageService->deleteImage($id);
            return response()->json(trans('admin.success_deleted'));
        } catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}
