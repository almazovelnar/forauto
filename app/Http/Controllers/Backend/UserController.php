<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Auth;
use Exception;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Services\manager\UserService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\User\{UserCreateRequest, UserUpdateRequest};

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        $query = User::query()->orderByDesc('id');
        if (Auth::user()->email != 'root@admin.com') {
            $query->where('email', '<>', 'root@admin.com');
        }
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.users.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.users.create', [
            'roles' => Role::all(),
        ]);
    }

    public function store(UserCreateRequest $request)
    {
        if($request->validated()) {
            try {
                $this->userService->create($request);
                return redirect()->route('admin.users.index')
                    ->with('success', trans('form.success.save'));
            }
            catch(Exception $e) {}
        }
    }

    public function show(User $user)
    {
        return view('backend.users.view', [
            'user' => $user
        ]);
    }

    public function edit(User $user)
    {
        return view('backend.users.update', [
            'roles' => Role::all(),
            'user' => $user
        ]);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        if($request->validated()) {
            try {
                $this->userService->update($request, $user);
                return redirect()->route('admin.users.index')
                    ->with('success', trans('form.success.save'));
            }
            catch(Exception $e) {}
        }
    }

    public function destroy(User $user)
    {
        try {
            $this->userService->remove($user);
            return redirect()->route('admin.users.index');
        }
        catch(Exception $e) {
            return redirect()->route('admin.users.index')
                ->with('error', trans('form.error.cant_remove_this_user'));
        }
    }

    public function deleteThumb(int $id)
    {
        try {
            $this->userService->deleteThumb($id);
            return response()->json(trans('admin.success_deleted'));
        } catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}
