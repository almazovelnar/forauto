<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Category;
use App\Helpers\NodeHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\CategoryService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Category\{CategoryCreateRequest, CategoryUpdateRequest};

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $query = Category::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.categories.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $parents = (new NodeHelper(Category::class))->map();

        return view('backend.categories.create', compact('parents'));
    }

    public function store(CategoryCreateRequest $request)
    {
        try {
            $this->categoryService->create($request);
            return redirect()->route('admin.categories.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {dd($e->getMessage());}
    }

    public function show(Category $category)
    {
        return view('backend.categories.view', [
            'category' => $category
        ]);
    }

    public function edit(Category $category)
    {
        $parents = (new NodeHelper($category))->map();

        return view('backend.categories.update', compact('category', 'parents'));
    }

    public function update(CategoryUpdateRequest $request, Category $category)
    {
        try {
            $this->categoryService->update($request, $category);
            return redirect()->route('admin.categories.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {}
    }

    public function destroy(Category $category): RedirectResponse
    {
        try {
            $this->categoryService->remove($category);
            return redirect()->route('admin.categories.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.categories.index')->with('error', $e->getMessage());
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $category = Category::findOrFail($id);
        ($type == 'up') ? $category->up() : $category->down();

        return redirect()->back();
    }
}
