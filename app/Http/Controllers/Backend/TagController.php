<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\Helpers\DataProvider;
use App\SearchQueries\TagSearchQuery;
use Exception;
use App\Models\Tag;
use RuntimeException;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Services\manager\TagService;
use App\Repositories\PostRepository;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\Tag\{TagCreateRequest, TagUpdateRequest};

class TagController extends BaseController
{
    /**
     * @var TagService
     */
    private $tagService;
    /**
     * @var PostRepository
     */
    private $postRepository;

    public function __construct(
        TagService $tagService,
        PostRepository $postRepository
    )
    {
        $this->tagService = $tagService;
        $this->postRepository = $postRepository;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $query = new TagSearchQuery();
        $dataProvider = new DataProvider($query->search($request->get('filters', [])));

        return view('backend.tags.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('backend.tags.create');
    }

    /**
     * @param TagCreateRequest $request
     * @return RedirectResponse
     */
    public function store(TagCreateRequest $request): RedirectResponse
    {
        try {
            $this->tagService->create($request);
            return $this->redirect('admin.tags.index');
        } catch (Exception $e) {
            report($e);
            return $this->redirect('admin.tags.index', 'error');
        }
    }

    /**
     * @param Tag $tag
     * @return View
     */
    public function edit(Tag $tag): View
    {
        return view('backend.tags.update', [
            'tag' => $tag,
        ]);
    }

    /**
     * @param TagUpdateRequest $request
     * @param Tag $tag
     * @return RedirectResponse
     */
    public function update(TagUpdateRequest $request, Tag $tag): RedirectResponse
    {
        try {
            $this->tagService->update($request, $tag);
            return $this->redirect('admin.tags.index');
        } catch (Exception $e) {
            report($e);
            return $this->redirect('admin.tags.index', 'error');
        }
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            $this->tagService->remove($id);
            return $this->redirect(
                'admin.tags.index',
                'success',
                trans('form.success.delete')
            );
        } catch (RuntimeException $e) {
            report($e);
            return $this->redirect(
                'admin.tags.index',
                'error',
                trans('form.error.delete')
            );
        }
    }
}
