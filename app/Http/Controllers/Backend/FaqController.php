<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Faq\FaqRequest;
use App\Models\Faq;
use App\Http\Controllers\Controller;
use App\Services\manager\FaqService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class FaqController extends Controller
{
    private $faqService;

    public function __construct(FaqService $faqService)
    {
        $this->faqService = $faqService;
    }

    public function index()
    {
        $dataProvider = new EloquentDataProvider(Faq::query()->orderByDesc('id'));
        return view('backend.faqs.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.faqs.create');
    }

    public function store(FaqRequest $request)
    {
        try {
            $this->faqService->create($request);
            return redirect()->route('admin.faq.index')
                ->with('success', trans('form.success.save'));
        }
        catch (\Exception $e) {
            dd($e);
        }
    }

    public function edit(Faq $faq)
    {
        return view('backend.faqs.update', [
            'faq' => $faq
        ]);
    }

    public function update(FaqRequest $request, Faq $faq)
    {
        try {
            $this->faqService->update($faq, $request);
            return redirect()->route('admin.faq.index')
                ->with('success', trans('form.success.save'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function destroy(Faq $faq)
    {
        try {
            $this->faqService->remove($faq);
            return redirect()->route('admin.faq.index');
        } catch (\Exception $e) {}
    }
}
