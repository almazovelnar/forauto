<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\RedirectResponse;

class BaseController
{
    /**
     * @param string $route
     * @param string|null $type
     * @param string|null $message
     * @return RedirectResponse
     */
    protected function redirect(string $route, ?string $type = 'success', ?string $message = null): RedirectResponse
    {
        return redirect()->route($route)
            ->with($type, trans($message ?? "form.{$type}.save"));
    }
}
