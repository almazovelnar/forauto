<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\Repositories\CategoryRepository;
use Exception;
use Illuminate\Http\Request;
use RuntimeException;
use App\Models\Property;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\PropertyService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Property\{PropertyCreateRequest, PropertyUpdateRequest};

class PropertyController extends Controller
{
    /**
     * @var PropertyService
     */
    private $propertyService;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        PropertyService $propertyService
    )
    {
        $this->propertyService = $propertyService;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $query = Property::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.properties.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.properties.create', [
            'categories' => $this->categoryRepository->all()
        ]);
    }

    public function store(PropertyCreateRequest $request)
    {
        try {
            $this->propertyService->create($request);
            return redirect()->route('admin.property.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.property.index')
                ->with('success', trans('form.error.save'));
        }
    }

    public function edit(Property $property)
    {

        return view('backend.properties.update', [
            'property' => $property,
            'categories' => $this->categoryRepository->all()
        ]);
    }

    public function update(PropertyUpdateRequest $request, Property $property)
    {
        try {
            $this->propertyService->update($request, $property);
            return redirect()->route('admin.property.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.property.index')
                ->with('success', trans('form.error.save'));
        }
    }

    public function destroy(Property $property): RedirectResponse
    {
        try {
            $this->propertyService->remove($property);
            return redirect()->route('admin.property.index')
                ->with('success', trans('form.success.delete'));
        } catch (RuntimeException $e) {
            return redirect()->route('admin.property.index')
                ->with('success', trans('form.error.delete'));
        }
    }

    public function getList(Request $request)
    {
        $properties = Property::query()->where('status', true)
            ->where('category_id', $request->get('category_id'))
            ->get();

        return response()->json([
            'success' => true,
            'html' => view()->make('backend.partials.properties', [
                'properties' => $properties
            ])->render()
        ]);
    }
}
