<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Slide;
use RuntimeException;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\SlideService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Slide\{SlideCreateRequest, SlideUpdateRequest};

class SlideController extends Controller
{
    private $slideService;

    public function __construct(SlideService $slideService)
    {
        $this->slideService = $slideService;
    }

    public function index()
    {
        $query = Slide::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.slides.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.slides.create');
    }

    public function store(SlideCreateRequest $request)
    {
        try {
            $this->slideService->create($request);
            return redirect()->route('admin.slides.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            dd($e);
            return redirect()->route('admin.slides.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function show(Slide $slide)
    {
        return view('backend.slides.view', [
            'slide' => $slide
        ]);
    }

    public function edit(Slide $slide)
    {
        return view('backend.slides.update', compact('slide'));
    }

    public function update(SlideUpdateRequest $request, Slide $slide)
    {
        try {
            $this->slideService->update($request, $slide);
            return redirect()->route('admin.slides.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.slides.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Slide $slide): RedirectResponse
    {
        try {
            $this->slideService->remove($slide);
            return redirect()->route('admin.slides.index')
                ->with('success', trans('form.success.delete'));
        } catch (RuntimeException $e) {
            return redirect()->route('admin.slides.index')
                ->with('error', trans('form.error.delete'));

        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $slide = Slide::findOrFail($id);
        ($type == 'up') ? $slide->up() : $slide->down();

        return redirect()->back();
    }
}
