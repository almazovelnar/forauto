<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\DataProvider;
use App\SearchQueries\CustomerQuery;
use Exception;
use App\Models\Customer;
use App\Http\Controllers\Controller;
use App\Services\manager\CustomerService;
use App\Http\Requests\Customer\{CustomerCreateRequest, CustomerUpdateRequest};
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    private $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function index(Request $request)
    {
        $dataProvider = new DataProvider((new CustomerQuery())->query($request->filters));
        return view('backend.customers.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.customers.create');
    }

    public function store(CustomerCreateRequest $request)
    {
        try {
            $this->customerService->create($request);
            return redirect()->route('admin.customers.index')
                ->with('success', trans('admin.form.success_save'));
        } catch(Exception $e) {
            return redirect()->route('admin.customers.index')
                ->with('error', trans('admin.form.error_save'));
        }
    }

    public function edit(Customer $customer)
    {
        return view('backend.customers.update', [
            'customer' => $customer
        ]);
    }

    public function update(CustomerUpdateRequest $request, Customer $customer)
    {
            try {
                $this->customerService->update($request, $customer);
                return redirect()->route('admin.customers.index')
                    ->with('success', trans('admin.form.success_save'));
            } catch(Exception $e) {
                return redirect()->route('admin.customers.index')
                    ->with('error', trans('admin.form.error_save'));
            }
    }

    public function destroy(Customer $customer)
    {
        try {
            $this->customerService->remove($customer);
            return redirect()->route('admin.customers.index')
                ->with('success', trans('admin.form.success_deleted'));
        } catch (Exception $e) {
            return redirect()->route('admin.customers.index')
                ->with('error', trans('admin.form.cant_delete'));
        }
    }
}
