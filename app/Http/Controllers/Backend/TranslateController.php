<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Translate;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Services\manager\TranslateService;
use Illuminate\Contracts\Foundation\Application;
use App\Http\Requests\Translate\TranslateRequest;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class TranslateController extends Controller
{
    private $translateService;

    public function __construct(TranslateService $translateService)
    {
        $this->translateService = $translateService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $dataProvider = new EloquentDataProvider(Translate::query()->orderByDesc('id'));

        return view('backend.translates.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('backend.translates.create');
    }

    /**
     * @param TranslateRequest $request
     * @return RedirectResponse|void
     */
    public function store(TranslateRequest $request)
    {
        if($request->validated()) {
            try {
                $this->translateService->create($request);
                return redirect()->route('admin.translates.index')
                    ->with('success', trans('form.success.save'));
            }
            catch (Exception $e) {}
        }
    }

    /**
     * @param Translate $translate
     * @return Application|Factory|View
     */
    public function edit(Translate $translate)
    {
        return view('backend.translates.update', [
            'translate' => $translate
        ]);
    }

    /**
     * @param TranslateRequest $request
     * @param Translate $translate
     * @return RedirectResponse|void
     */
    public function update(TranslateRequest $request, Translate $translate)
    {
        if($request->validated()) {
            try {
                $this->translateService->update($request, $translate);
                return redirect()->route('admin.translates.index')
                    ->with('success', trans('form.success.save'));
            }
            catch (Exception $e) {}
        }
    }

    /**
     * @param Translate $translate
     * @return RedirectResponse|void
     */
    public function destroy(Translate $translate)
    {
        try {
            $this->translateService->remove($translate);
            return redirect()->route('admin.translates.index');
        }
        catch (Exception $e) {}
    }
}
