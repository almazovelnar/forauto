<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Post;
use App\Models\Photo;
use Illuminate\Http\Request;
use App\Repositories\TagRepository;
use App\Http\Controllers\Controller;
use App\Services\manager\PostService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Post\{PostCreateRequest, PostUpdateRequest};

class PostController extends Controller
{
    /**
     * @var PostService
     */
    private $postService;
    /**
     * @var TagRepository
     */
    private $tagRepository;


    public function __construct(
        PostService   $postService,
        TagRepository $tagRepository
    )
    {
        $this->postService   = $postService;
        $this->tagRepository = $tagRepository;
    }

    public function index()
    {
        $dataProvider = new EloquentDataProvider(Post::query()->orderByDesc('id'));

        return view('backend.posts.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.posts.create');
    }

    public function store(PostCreateRequest $request)
    {
        try {
            $this->postService->create($request);
            return redirect()->route('admin.blog.posts.index')
                ->with('success', trans('admin.form.success_save'));
        } catch (Exception $e) {
            dd($e);
            return redirect()->route('admin.blog.posts.index')
                ->with('error', trans('admin.form.error_save'));
        }
    }

    public function edit(Post $post)
    {
        return view('backend.posts.update', [
            'post' => $post
        ]);
    }

    public function update(PostUpdateRequest $request, Post $post)
    {
        try {
            $this->postService->update($request, $post);
            return redirect()->route('admin.blog.posts.index')
                ->with('success', trans('admin.form.success_save'));
        } catch (Exception $e) {
            return redirect()->route('admin.blog.posts.index')
                ->with('error', trans('admin.form.error_save'));
        }
    }

    public function deleteCover(int $id)
    {
        try {
            $this->postService->deleteImage($id);
            return response()->json(trans('admin.success_deleted'));
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function destroy(Post $post)
    {
        try {
            $this->postService->remove($post);
            return redirect()->route('admin.blog.posts.index')
                ->with('success', trans('admin.form.success_deleted'));
        } catch (Exception $e) {
            return redirect()->route('admin.blog.posts.index')
                ->with('error', trans('admin.form.cant_delete'));
        }
    }

    public function tagList(Request $request)
    {
        $data            = ['results' => ['id' => '', 'name' => '']];
        $posts           = $this->tagRepository->getByQuery($request->q, $request->lang)->toArray();
        $data['results'] = array_values($posts);

        return response()->json($data);
    }
}
