<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\CreateOrderRequest;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Cache;


class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    private $cart;

    public function __construct(OrderService $orderService, OrderRepository $orderRepository)
    {
        $this->orderService    = $orderService;
        $this->orderRepository = $orderRepository;
        $this->cart = Cart::instance('cart');
    }

    public function thankYou()
    {
        $orderId = \request('orderId');
        if (!$orderId) {
            return redirect()->route('history');
        }
        $order = Cache::remember($orderId, Carbon::now()->addDay(), function () use ($orderId) {
            return $this->orderRepository->getByOrderId($orderId);
        });
        if ($order) {
            $this->cart->destroy();
            return view('frontend.order.success', ['order' => $order]);
        } else {
            return view('frontend.order.fail');
        }
    }

    public function send(CreateOrderRequest $request)
    {
        $res = $this->orderService->create($request);

        if ($res['success']) {
            return redirect()->route('order.thankYou', ['orderId' => $res['order_id']]);
        } else {
            return redirect()->back();
        }
    }
}
