<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactMessageRequest;
use App\Repositories\ProductRepository;
use App\Repositories\SlideRepository;
use App\Services\ContactService;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * @var ContactService
     */
    private $contactService;

    public function __construct(
        ContactService $contactService
    )
    {
        $this->contactService = $contactService;
    }

    public function index()
    {
        return view('frontend.contact', [
        ]);
    }

    public function send(ContactMessageRequest $request)
    {
        try {
            $this->contactService->sendMessage($request);
            return redirect()->route('contact')
                ->with('notification', trans('site.contact.message_sent_successfully'));
        } catch (\Exception $e) {
            return redirect()->route('contact')
                ->with('notification', trans('site.contact.message_cant_sent'));
        }
    }
}
