<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Customer\CustomerChangePasswordRequest;
use Hash;
use Auth;
use Exception;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use App\Services\manager\CustomerService;
use App\Http\Requests\Customer\CustomerProfileUpdateRequest;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $customerService;
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(
        OrderRepository $orderRepository,
        CustomerService $customerService
    )
    {
        $this->customerService = $customerService;
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        $user = Auth::user();
        return view('frontend.profile.details', ['customer' => $user]);
    }

    public function history()
    {
        $user = Auth::user();
        $orders = $this->orderRepository->getByCustomer($user->id);

        return view('frontend.profile.orders', ['customer' => $user, 'orders' => $orders]);
    }

    public function changePasswordForm()
    {
        return view('frontend.profile.change-password');
    }

    public function changePassword(CustomerChangePasswordRequest $request)
    {
        try {
            $this->customerService->changePassword($request, Auth::user());
            return redirect()->back()
                ->with('notification', trans('site.profile.success_save'));
        } catch (Exception $e) {
            return redirect()->back()
                ->with('notification', trans('site.profile.error_save'));
        }
    }

    public function historyView($orderId)
    {
        $user = Auth::user();
        $order = $this->orderRepository->getCustomerOrder($orderId, $user->id);

        if (!$order) {
            abort(404);
        }

        $items = $this->orderRepository->getOrderItems($order->id);

        return view('frontend.profile.order-view', ['customer' => $user, 'order' => $order, 'items' => $items]);
    }

    public function edit(CustomerProfileUpdateRequest $request)
    {
        $customer = Auth::user();
        try {
            $this->customerService->updateProfile($request, $customer);
            return redirect()->route('profile')
                ->with('notification', trans('site.profile.success_save'));
        } catch (Exception $e) {
            return redirect()->route('profile')
                ->with('notification', trans('site.profile.error_save'));
        }
    }

    public function delete() {
        $customer = Auth::user();
        try {
            $this->customerService->deleted($customer);
            return redirect()->route('logout');
        }
        catch (Exception $e) {}
    }
}
