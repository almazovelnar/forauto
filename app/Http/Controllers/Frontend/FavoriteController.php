<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;

class FavoriteController extends Controller
{
    private $wishlist;

    public function __construct(
    )
    {
        $this->wishlist = Cart::instance('wishlist');
    }

    public function index()
    {
        $wishlistProducts = $this->wishlist->content();
        $query = Product::query()->whereIn('id', $wishlistProducts->pluck('id'));
        $filter = unserialize(Cookie::get('wishlist-filter') ?? '');

        $query->orderByRaw($filter['sort'] ?? 'created_at desc');

        if ($filter['discount'] ?? false) {
            $query->where('hot', true);
        }

        return view('frontend.wishlist', [
            'products' => $query->get(),
            'filter' => $filter
        ]);
    }

    public function add(Request $request)
    {
        $res = [];
        try {
            $product = Product::query()->where('id', $request->get('id'))->first();
            $this->wishlist->add($product->id, $product->title, 1, $product->price);
            $res['count'] = $this->wishlist->content()->count();
            $res['success'] = true;
        } catch (\Exception $e){
            dd($e);
            $res['success'] = false;
        }

        return $res;
    }

    public function remove(Request $request)
    {
        $id = (int) $request->input('id');
        $res = [];

        try {
            $product = $this->wishlist->content()->where('id', $id)->firstOrFail();
            $this->wishlist->remove($product->rowId);
            $res['count'] = $this->wishlist->content()->count();
            $res['success'] = true;
        } catch (\Exception $e){
            $res['success'] = false;
        }

        return $res;
    }

    public function filter(Request $request): \Illuminate\Http\JsonResponse
    {
        $filter = unserialize(Cookie::get('wishlist-filter') ?? '') ?? [];

        try {
            if ($sort = $request->get('sort')) {
                $filter['sort'] = $sort;
            }
            if ($discount = $request->get('discount')) {
                $filter['discount'] = $discount === 'true';
            }
            Cookie::queue('wishlist-filter', serialize($filter), 60*6);
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            Log::debug($e);
            return response()->json([
                'success' => false
            ]);
        }
    }
}
