<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Services\manager\CustomerService;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Customer\CustomerRegisterRequest;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @var CustomerService
     */
    private $customerService;

    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    public function __construct(CustomerService $customerService)
    {
        $this->middleware('guest');

        $this->customerService = $customerService;
    }

    public function register(CustomerRegisterRequest $request)
    {
        event(new Registered($customer = $this->customerService->register($request)));
        $this->guard()->login($customer);

        return redirect()->home();
    }
}
