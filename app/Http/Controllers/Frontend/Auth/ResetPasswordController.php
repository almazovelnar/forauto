<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use Session;

class ResetPasswordController extends Controller
{

    /** @var CustomerRepository */
    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function showResetPasswordForm(Request $request)
    {
        if ($request->isMethod('get')) {
            $this->whetherAllowedSendCode();
            return view('frontend.auth.reset-password-form');
        }

        if ($request->isMethod('post')) {
            $request->validate(['email' => 'required|email|exists:customers,email']);

            $code = $this->generateCode($request->get('email'));

            // send this code by email
            file_put_contents(__DIR__ . '/code.txt', $code);

            return redirect()->route('show-code-form');
        }

        abort(404);
    }

    public function showCodeInputForm(Request $request)
    {
        if (!Session::has('reset-password')) {
            return redirect()->home();
        }

        if ($request->isMethod('post')) {
            $session = Session::get('reset-password');
            $request->validate(['code' => ['required', 'in:' . $session['code']]]);

            Session::remove('reset-password');
            Session::put('reset-password-successful', $session['email']);

            return redirect()->route('change-password');
        }

        return view('frontend.auth.show-code-form');
    }

    public function changePassword(Request $request)
    {
        if (!Session::has('reset-password-successful')) {
            return redirect()->home();
        }
        if ($request->isMethod('post')) {
            $email = Session::get('reset-password-successful');
            $request->validate([
                'password'        => 'min:6|required_with:passwordConfirm|same:passwordConfirm',
                'passwordConfirm' => 'min:6'
            ]);

            $this->customerRepository->changePassword($email, $request->get('password'));
            Session::remove('reset-password-successful');

            return redirect()->route('login');
        }

        return view('frontend.auth.change-password');
    }

    private function whetherAllowedSendCode(): void
    {
        if (Session::has('reset-password')) {
            $expires = Session::get('reset-password')['expires'] ?? time();
            if (time() < $expires) {
                abort(404);
            }
        }
    }

    private function generateCode(string $email): int
    {
        // generate reset code
        $resetCode = rand(12345, 98765);
        session(['reset-password' => [
            'email'   => $email,
            'code'    => $resetCode,
            'expires' => strtotime('+2 minute')
        ]]);

        return $resetCode;
    }

}
