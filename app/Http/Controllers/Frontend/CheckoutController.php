<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\manager\CustomerService;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    private $customerService;
    private $cart;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
        $this->cart = Cart::instance('cart');
    }

    public function index()
    {
        $user = Auth::user();
        $products = $this->cart->content();
        if ($products->isEmpty()) {
            return redirect()->route('cart');
        }

        return view('frontend.checkout', ['customer' => $user, 'products' => $products]);
    }
}
