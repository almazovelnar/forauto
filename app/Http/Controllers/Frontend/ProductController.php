<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Property;
use App\Services\ProductRead;
use Illuminate\Http\Request;
use App\Services\OrderService;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Builder;

class ProductController extends Controller
{
    const ITEMS_COUNT = 12;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var ProductRead
     */
    private $productRead;

    public function __construct(
        CategoryRepository $categoryRepository,
        ProductRepository  $productRepository,
        OrderService       $orderService,
        ProductRead        $productRead
    ) {
        $this->productRepository  = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->orderService       = $orderService;
        $this->productRead        = $productRead;
    }

    public function index(Request $request)
    {
        $category = $this->categoryRepository->getBySlug(request('slug'));
        $filter   = unserialize(Cookie::get('products-filter') ?? '');

        $query = Product::query()
            ->where('products.status', true)
            ->where('products.category_id', $category->id);

        if (!empty($request->get('brand'))) {
            $brands = Brand::query()->whereIn('slug', $request->get('brand'))->get();
            $query->where(function ($subQuery) use ($brands) {
                foreach ($brands as $brand) {
                    $subQuery->orWhere('products.brand_id', $brand->id);
                }
            });
        }

        if (!empty($request->get('price')['from'])) {
            $price = ($request->get('price')['from'] * 100);
            $query->where(function (Builder $subQuery) use ($price) {
                $subQuery->where(function ($q) use ($price) {
                    $q->where('products.price', '>=', $price)->where('products.sale_price', 0);
                })
                    ->orWhere(function ($q) use ($price) {
                        $q->where('products.sale_price', '>=', $price)->where('products.sale_price', '<>', 0);
                    });
            });
        }

        if (!empty($request->get('price')['to'])) {
            $price = ($request->get('price')['to'] * 100);
            $query->where(function (Builder $subQuery) use ($price) {
                $subQuery->where(function ($q) use ($price) {
                    $q->where('products.price', '<=', $price)->where('products.sale_price', 0);
                })
                    ->orWhere(function ($q) use ($price) {
                        $q->where('products.sale_price', '<=', $price)->where('products.sale_price', '<>', 0);
                    });
            });
        }

        if (!empty($request->get('filter'))) {
            $filter = array_filter($request->get('filter') ?? []);

            $props = Property::query()->whereIn('slug', array_keys($filter))->get();
            foreach ($filter as $slug => $values) {
                $filter[$props->where('slug', $slug)->first()->id] = $values;
                unset($filter[$slug]);
            }

            foreach ($filter as $propertyId => $values) {
                if (!empty(array_filter($values ?? []))) {
                    $alias = 'pp' . $propertyId;
                    $query->join("property_product as {$alias}", function ($join) use ($alias, $propertyId, $values) {
                        $join->on($alias . '.product_id', 'products.id')
                            ->where($alias . '.property_id', $propertyId)->whereIn($alias . '.value', $values);
                    });
                }
            }

        }


        if ($filter['discount'] ?? null) {
            $query->where('products.hot', $filter['discount']);
        }
//dd($query->toSql());
        $products = $query
            ->orderByRaw( 'products.' . ($filter['sort'] ?? 'created_at desc'))
            ->paginate(self::ITEMS_COUNT);

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'html'    => view()->make('frontend.partials._products', [
                    'products' => $products->appends($request->query())
                ])->render()
            ]);
        }

        return view('frontend.products.index', [
            'products' => $products,
            'category' => $category,
            'filter'   => $filter
        ]);
    }

    public function view(string $slug)
    {
        $product = $this->productRepository->getBySlug($slug);
        $this->productRead->incrementViewsCount($product);

        $this->generateMetaTag($product->title, $product->description, $product->meta, true, product_image($product->image));
        return view('frontend.products.single', [
            'product'         => $product,
            'relatedProducts' => $this->productRepository->getRelatedProducts($product->category_id, $product->brand_id, $product->id, 4),
        ]);
    }

    public function fastBuy(Request $request, int $id)
    {
        try {
            $this->orderService->fastBuy($request, $this->productRepository->get($id));
            return redirect()->back()->with('notification', trans('site.product.fast_buy_order_success'));
        } catch (\Exception $exception) {
            Log::debug($exception);
            return redirect()->back()->with('notification', trans('site.product.fast_buy_order_error'));
        }
    }

    public function filter(Request $request): \Illuminate\Http\JsonResponse
    {
        $filter = unserialize(Cookie::get('products-filter') ?? '') ?? [];

        try {
            if ($sort = $request->get('sort')) {
                $filter['sort'] = $sort;
            }
            if ($discount = $request->get('discount')) {
                $filter['discount'] = $discount === 'true';
            }
            Cookie::queue('products-filter', serialize($filter), 60 * 6);
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {
            Log::debug($e);
            return response()->json([
                'success' => false
            ]);
        }
    }
}
