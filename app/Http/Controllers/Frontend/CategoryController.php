<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Repositories\PostRepository;
use App\Services\manager\PostService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Collection;
use LaravelLocalization;
use View;

class CategoryController extends Controller
{
    public const ITEMS_COUNT = 6;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;
    /**
     * @var PostRepository
     */
    protected $postRepository;
    /**
     * @var PostService
     */
    protected $postService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository, PostRepository $postRepository, PostService $postService)
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository     = $postRepository;
        $this->postService        = $postService;
    }

    public function index($slug)
    {
        $category           = $this->categoryRepository->getBySlug($slug);
        $latestPosts        = $category->posts()->orderByDesc('published_at')->limit(6)->get();
        $popularPosts       = $category->posts()->orderByDesc('views')->limit(6)->get();
        $editorsChoicePosts = $category->posts()->orderByDesc('published_at')->limit(6)->where('editor_choice', 1)->get();

        $this->createTranslatedLinks($category);

        return view('frontend.category'
            , [
                'category'           => $category,
                'latestPosts'        => $latestPosts,
                'popularPosts'       => $popularPosts,
                'editorsChoicePosts' => $editorsChoicePosts
            ]
        );
    }

    public function view(Request $request, string $catSlug, string $postSlug)
    {
        $post = $this->postRepository->getBySlug($postSlug);
        if (!$post)
            abort(404);
        $category     = $this->categoryRepository->get($post->category_id);
        $reletedPosts = $this->postRepository->getRelatedPosts($post, 6);
        if ($request->ajax() && $request->has('lastPub')) {
            $views        = [];
            $reletedPosts = $this->getReleatedPosts($category, $request->get('lastPub'));
            foreach ($reletedPosts as $reletedPost) {
                $views[] = [
                    'html'         => view('frontend.partials._post', ['post' => $reletedPost])->render(),
                    'limitReached' => $reletedPosts->count() < self::ITEMS_COUNT
                ];
            }

            return response()->json($views);
        }

        return view('frontend.posts.view', [
            'post'  => $post,
            'posts' => $reletedPosts,
        ]);
    }

    private function getReleatedPosts(Category $category, ?string $lastPub = null): Collection
    {
        $query = $category->posts()
            ->limit(self::ITEMS_COUNT)
            ->orderBy('published_at', 'desc');

        if ($lastPub) {
            $query->where('posts.published_at', '<', $lastPub);
        }
        return $query->get();
    }

    public function latest(Request $request, $categorySlug)
    {
        $category = $this->categoryRepository->getBySlug($categorySlug);
        if (!$category)
            abort(404);
        $posts = $this->getPosts($category, 'latest', self::ITEMS_COUNT);
        if ($request->ajax() && $request->has('lastPub')) {
            $posts = $this->getPosts($category, 'latest', self::ITEMS_COUNT, $request->get('lastPub'));
            $views = [
                'html'         => view('frontend.partials._posts', ['posts' => $posts])->render(),
                'limitReached' => $posts->count() < self::ITEMS_COUNT
            ];
            return response()->json($views);
        }
        return view('frontend.posts.latest', [
            'posts' => $posts,
        ]);
    }

    public function popular(Request $request, $categorySlug)
    {
        $category = $this->categoryRepository->getBySlug($categorySlug);
        if (!$category)
            abort(404);
        $posts = $this->postRepository->getPopularPosts(self::ITEMS_COUNT);
        if ($request->ajax() && $request->has('lastPub')) {
            $posts = $this->getPosts($category, 'popular', self::ITEMS_COUNT, $request->get('lastPub'));
            $views = [
                'html'         => view('frontend.partials._posts', ['posts' => $posts])->render(),
                'limitReached' => $posts->count() < self::ITEMS_COUNT
            ];
            return response()->json($views);
        }
        return view('frontend.posts.popular', [
            'posts' => $posts,
        ]);
    }

    public function editorsChoice(Request $request, $categorySlug)
    {
        $category = $this->categoryRepository->getBySlug($categorySlug);
        if (!$category)
            abort(404);
        $posts = $this->postRepository->getEditorsChoicePosts(self::ITEMS_COUNT);
        if ($request->ajax() && $request->has('lastPub')) {
            $posts = $this->getPosts($category, 'editor', self::ITEMS_COUNT, $request->get('lastPub'));
            $views = [
                'html'         => view('frontend.partials._posts', ['posts' => $posts])->render(),
                'limitReached' => $posts->count() < self::ITEMS_COUNT
            ];
            return response()->json($views);
        }
        return view('frontend.posts.editors-choice', [
            'posts' => $posts,
        ]);
    }

    public function getPosts(Category $category, $filter = null, $limit = self::ITEMS_COUNT, $lastPub = null)
    {

        $query = $category->posts()->limit($limit);
        if ($lastPub) {
            $query->where('published_at', '<', $lastPub);
        }
        if ($filter == 'popular') {
            $query->orderByDesc('views');
        } else if ($filter == 'editor') {
            $query->where('editor_choice', 1);
        } else {
            $query->orderByDesc('published_at');
        }
        return $query->get();
    }

    private function createTranslatedLinks(Category $category): void
    {
        $translatedLinks = [];
        foreach (LaravelLocalization::getSupportedLanguagesKeys() as $locale) {
            if ($category->hasTranslation('slug', $locale)) {
                $translatedLinks[$locale] = LaravelLocalization::localizeUrl(route('posts.category',
                    ['catSlug' => $category->getTranslation('slug', $locale)]), $locale);
            }
        }
        View::share('translatedLinks', $translatedLinks);
    }


}
