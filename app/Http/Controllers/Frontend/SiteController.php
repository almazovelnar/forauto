<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\SlideRepository;
use App\Repositories\ProductRepository;

class SiteController extends Controller
{
    /**
     * @var SlideRepository
     */
    private $slideRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        SlideRepository   $slideRepository,
        ProductRepository $productRepository
    ) {
        $this->slideRepository   = $slideRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $slides         = $this->slideRepository->all();
        $topProducts    = $this->productRepository->getTopProducts(8);
        $hotProducts    = $this->productRepository->getHotProducts(4);
        $seasonProducts = $this->productRepository->getSeasonProducts();


        $this->generateMetaTag(site_info('title'), null, site_info('meta'), false);
        return view('frontend.home', [
            'slides'      => $slides,
            'topProducts' => $topProducts,
            'hotProducts' => $hotProducts,
            'seasonProducts' => $seasonProducts,
        ]);
    }
}
