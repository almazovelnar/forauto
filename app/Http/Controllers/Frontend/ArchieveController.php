<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\ArchiveRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;


class ArchieveController extends Controller
{
    private $archieveRepository;

    public function __construct(ArchiveRepository $archieveRepository)
    {
        $this->archieveRepository = $archieveRepository;
    }

    public const ITEMS_COUNT = 6;


    public function index(Request $request)
    {

        $archieves = $this->archieveRepository->getArchieves(self::ITEMS_COUNT);
        if ($request->ajax() && $request->has('lastPub')) {
            $views     = [];
            $archieves = $this->getArchieves($request->get('lastPub'));
            $views     = [
                'html'         => view('frontend.partials._archieves', ['archieves' => $archieves])->render(),
                'limitReached' => $archieves->count() < self::ITEMS_COUNT];
            return response()->json($views);
        }


        return view('frontend.archieve', compact('archieves'));
    }

    public function getArchieves(?string $lastPub = null): Collection
    {
        $query = $this->archieveRepository->archieveQuery()
            ->limit(self::ITEMS_COUNT)
            ->orderBy('published_at', 'desc');
        if ($lastPub) {
            $query->where('published_at', '<', $lastPub);
        }
        return $query->get();
    }


    public function getFile($id)
    {
        $archieve = $this->archieveRepository->get($id);
        if (!$archieve)
            abort(404);
        $filename = $archieve->file;
        $path     = 'archieves/files/' . $filename;
        $storage  = \Storage::disk('public');
        if (!$filename || !($storage->has($path))) {
            return redirect()->back();
        }
        return response()->file($storage->path($path));
    }

}
