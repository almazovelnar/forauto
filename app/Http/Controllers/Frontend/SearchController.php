<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Helpers\StringHelper;
use App\Repositories\PostRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;


class SearchController extends Controller
{
    public const ITEMS_COUNT = 6;
    /**
     * @var ProductRepository
     */
    private $productRepository;


    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(Request $request)
    {
        $q = StringHelper::clearSearchQuery($request->get('query'));
        if ($q != $request->get('query')) {
            return redirect()->route('search', ['q' => $q]);
        }
        if (!$q) return redirect()->back();

        $filter = unserialize(Cookie::get('search-filter') ?? '');
        $products = $this->productRepository
            ->getByQuery($q, $filter['sort'] ?? 'created_at desc', $filter['discount'] ?? null)
            ->appends(['query' => $q]);

        return view('frontend.search', [
            'query' => $q,
            'products' => $products,
            'filter' => $filter
        ]);
    }

    public function filter(Request $request): \Illuminate\Http\JsonResponse
    {
        $filter = unserialize(Cookie::get('search-filter') ?? '') ?? [];

        try {
            if ($sort = $request->get('sort')) {
                $filter['sort'] = $sort;
            }
            if ($discount = $request->get('discount')) {
                $filter['discount'] = $discount === 'true';
            }
            Cookie::queue('search-filter', serialize($filter), 60*6);
            return response()->json([
                'success' => true
            ]);
        } catch (\Exception $e) {;
            Log::debug($e);
            return response()->json([
                'success' => false
            ]);
        }
    }
}
