<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\PageRepository;
use App\Repositories\PostCategoryRepository;
use App\Repositories\PostRepository;
use App\Repositories\TagRepository;

class BlogController extends Controller
{
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var PostCategoryRepository
     */
    private $postCategoryRepository;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    public function __construct(
        PostCategoryRepository $postCategoryRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->postCategoryRepository = $postCategoryRepository;
        $this->tagRepository = $tagRepository;
    }

    public function index(?string $slug = null)
    {
        $category = null;
        if ($slug) {
            $category = $this->postCategoryRepository->getBySlug($slug);
            $posts = $this->postRepository->getPostsByCatWithPaginate($category->id, 12);
        } else {
            $posts = $this->postRepository->getPostsWithPaginate(12);
        }

        return view('frontend.blog.index', [
            'category' => $category,
            'posts' => $posts
        ]);
    }

    public function tag(string $slug)
    {
        $tag = $this->tagRepository->get($slug, app()->getLocale());
        $posts = $this->postRepository->getPostsByTagWithPaginate($tag->id, 12);

        return view('frontend.blog.tag', [
            'tag' => $tag,
            'posts' => $posts
        ]);
    }

    public function view(string $slug)
    {
        $post = $this->postRepository->getBySlug($slug);
        return view('frontend.blog.single', [
            'post' => $post,
            'relatedPosts' => $this->postRepository->getRelatedPosts($post->category_id, $post->id, 3)
        ]);
    }
}
