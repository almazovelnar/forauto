<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Page;
use App\Repositories\PageRepository;

class StaticController extends Controller
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    public function __construct(
        PageRepository $pageRepository
    ) {
        $this->pageRepository = $pageRepository;
    }

    public function view(string $slug)
    {
        $page = $this->pageRepository->getBySlug($slug);

        return view('frontend.static.' . ($page->template == Page::SERVICE ? 'service' : 'default'), [
            'otherPages' => $this->pageRepository->all(),
            'page'       => $page
        ]);
    }

    public function about()
    {
        return view('frontend.about', [
            'page' => $this->pageRepository->getBySlug('about-us')
        ]);
    }

    public function faq()
    {
        return view('frontend.static.faq', [
            'questions' => Faq::query()->where('status', true)->orderBy('question')->get(),
        ]);
    }
}
