<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Repositories\SlideRepository;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    private $cart;

    public function __construct(
    )
    {

        $this->cart = Cart::instance('cart');
    }

    public function index()
    {
        $cartProducts = $this->cart->content();

        return view('frontend.cart', [
            'cartProducts' => $cartProducts,
            'products' => Product::query()->whereIn('id', $cartProducts->pluck('id'))->get()
        ]);
    }

    public function add(Request $request)
    {
        $id = (int) $request->input('id');
        $qty = (int) $request->input('qty');

        try {
            $product = Product::query()->where('id', $id)->firstOrFail();
            $price = !empty($product->sale_price) ? $product->sale_price : $product->price;
            $this->cart->add($product->id, $product->title, $qty ?: 1, $price);
            $res['count'] = $this->cart->count();
            $res['price'] = get_price($this->cart->priceTotal());
            $res['text'] = trans('site.product.remove_from_cart');
            $res['notification'] = trans('site.product.product_added_to_cart', ['count' => $qty]);
            $res['success'] = true;
        } catch (\Exception $e){
            $res['success'] = false;
        }

        return $res;
    }

    public function update(Request $request)
    {
        $products = $request->input('products');
        $qty = (int) $request->input('qty');
        try {
            foreach ($products as $id => $qty) {
                $product = $this->cart->content()->where('id', $id)->first();
                if ($product) {
                    $this->cart->update($product->rowId, $qty);
                }
            }

            $res['success'] = true;
        } catch (\Exception $e){
            $res['success'] = false;
        }

        return $res;
    }

    public function remove(Request $request)
    {
        $id = (int) $request->input('id');

        try {
            $product = $this->cart->content()->where('id', $id)->firstOrFail();
            $this->cart->remove($product->rowId);
            $res['count'] = $this->cart->count();
            $res['price'] = get_price($this->cart->total());
            $res['text'] = trans('site.product.add_to_cart');
            $res['notification'] = trans('site.product.product_removed_from_cart');
            $res['success'] = true;
        } catch (\Exception $e){
            $res['success'] = false;
        }

        return $res;
    }
}
