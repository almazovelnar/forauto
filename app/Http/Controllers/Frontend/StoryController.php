<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;


use App\Models\Story;
use App\Repositories\PostRepository;
use App\Repositories\StoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelLocalization;
use View;

class StoryController extends Controller
{
    public const ITEMS_COUNT = 18;

    /**
     * @var PostRepository
     */
    protected $postRepository;
    /**
     * @var StoryRepository
     */
    protected $storyRepository;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $postRepository, StoryRepository $storyRepository)
    {
        $this->postRepository  = $postRepository;
        $this->storyRepository = $storyRepository;
    }


    public function index(Request $request, $slug)
    {
        $story = $this->storyRepository->getBySlug($slug);
        $posts = $story->posts()->limit(self::ITEMS_COUNT)->orderByDesc('published_at')->get();
        $this->createTranslatedLinks($story);

        if ($request->ajax() && $request->has('lastPub')) {
            $posts = $this->getPosts($story, $request->get('lastPub'));
            $views = [
                'html'         => view('frontend.partials._posts', ['posts' => $posts])->render(),
                'limitReached' => $posts->count() < self::ITEMS_COUNT
            ];
            return response()->json($views);
        }
        return view('frontend.posts.story', [
            'posts' => $posts,
            'story' => $story,
        ]);
    }

    public function getPosts(Story $story, $lastPub = null, $limit = self::ITEMS_COUNT)
    {
        $query = $story->posts()->limit($limit);
        if ($lastPub) {
            $query->where('published_at', '<', $lastPub);
        } else {
            $query->orderByDesc('published_at');
        }
        return $query->get();
    }

    private function createTranslatedLinks(Story $story): void
    {
        $translatedLinks = [];
        foreach (LaravelLocalization::getSupportedLanguagesKeys() as $locale) {
            if ($story->hasTranslation('slug', $locale)) {
                $translatedLinks[$locale] = LaravelLocalization::localizeUrl(route('story',
                    ['slug' => $story->getTranslation('slug', $locale)]), $locale);
            }
        }
        View::share('translatedLinks', $translatedLinks);
    }


}
