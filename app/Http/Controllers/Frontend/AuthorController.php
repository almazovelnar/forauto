<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    const ITEMS_COUNT = 12;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {

        return view('frontend.authors.index',[
            'authors'=>$this->userRepository->getAuthors(20)
        ]);
    }

    public function posts(Request $request, $id)
    {
        $author = $this->userRepository->get($id);
        $posts  = $author->posts()->paginate(self::ITEMS_COUNT)->where('language',app()->getLocale());
        if ($request->ajax() && $request->has('lastId')) {
            $posts = $this->getPosts($author, $request->get('lastPub'));
            return response()
                ->json([
                    'html'         => view('frontend.partials._posts', ['posts' => $posts])->render(),
                    'limitReached' => $posts->count() < self::ITEMS_COUNT
                ]);
        }

        return view('frontend.authors.posts', compact('author', 'posts'));

    }

    private function getPosts(User $author, $lastPub = null): Collection
    {
        $query = $author->posts()
            ->limit(self::ITEMS_COUNT);
        if ($lastPub) {
            $query->where('published_at', '<', $lastPub);
        }
        return $query->get();
    }

}
