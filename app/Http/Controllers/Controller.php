<?php

namespace App\Http\Controllers;

use MetaTag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests,
        ValidatesRequests,
        DispatchesJobs;

    public function generateMetaTag($title, $description = null, $meta = null, ?bool $siteInfo = true, $image = null)
    {
        $keywords = '';
        if ($meta) {
            $meta = (array) $meta;
            $description = $meta['description'] ?? strip_tags($description);
            $title       = $meta['title'] ?? $title;
            $keywords    = $meta['keywords'] ?? '';
        }
        MetaTag::set('description', html_entity_decode($description ? strip_tags($description) : site_info('description')));
        MetaTag::set('keywords', html_entity_decode($keywords ? strip_tags($keywords) : site_info('keywords')));
        MetaTag::set('og:title', html_entity_decode($title));
        MetaTag::set('og:description', html_entity_decode($description));
        MetaTag::set('og:image', $image ?? '');

        if ($siteInfo) {
            $title .= ' | ' . site_info('title');
        }

        \View::share('title', html_entity_decode($title));
    }
}
