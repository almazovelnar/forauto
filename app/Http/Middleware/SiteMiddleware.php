<?php

namespace App\Http\Middleware;

use Auth;
use Cookie;
use Closure;
use Illuminate\Http\Request;

class SiteMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        config()->set(['auth.defaults.guard' => 'siteCustomer', 'auth.defaults.passwords' => 'customers']);

        $response = $next($request);

        if (!Auth::check() && Cookie::has('cart')) {
            return $response->withCookie(Cookie::forget('cart'));
        }

        return $response;
    }
}
