<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Helpers\StringHelper;

class SearchQuery
{
    public function handle(Request $request, \Closure $next)
    {
        $request->validate([
            'query' => ['string', 'min:3']
        ]);

        $query         = $request->get('query');
        $filteredQuery = StringHelper::clearSearchQuery($request->get('query'));
        if (strcmp($query, $filteredQuery) <> 0) {
            return redirect()->route('search', ['query' => $filteredQuery]);
        }

        return $next($request);
    }

}
