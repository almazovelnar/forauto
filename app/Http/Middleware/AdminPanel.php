<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;

class AdminPanel
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        App::setLocale(config('app.admin_locale'));
        return $next($request);
    }
}
