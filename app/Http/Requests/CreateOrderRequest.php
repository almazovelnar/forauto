<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'    => 'required|min:3',
            'surname' => 'required|min:3',
            'phone'   => 'required|min:6',
//            'address' => 'required|min:3',
//            'email'   => 'nullable|email|min:6'
        ];
    }

    public function attributes()
    {
        return [
            'name'    => trans('site.checkout.name'),
            'surname' => trans('site.checkout.surname'),
            'phone'   => trans('site.checkout.phone'),
            'address' => trans('site.checkout.address'),
            'email'   => trans('site.checkout.email'),
        ];
    }
}
