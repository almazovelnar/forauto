<?php

namespace App\Http\Requests\MainInfo;

use Illuminate\Foundation\Http\FormRequest;

class MainInfoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'         => 'array',
            'title.*'       => 'required',
            'description'   => 'array',
            'description.*' => ['required', 'min:6'],
            'keywords'      => 'array',
            'address'       => 'array',
            'name'          => 'required',
            'email'         => 'array',
            'email.*'       => ['nullable', 'email'],
            'phone'         => 'array',
            'phone.*'       => ['nullable', 'string', 'min:6'],
            'fax'           => ['nullable', 'string'],
            'location'      => ['nullable', 'string'],
            'logo'          => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')],
            'favicon'       => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
