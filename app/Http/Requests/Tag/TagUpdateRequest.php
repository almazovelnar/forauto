<?php

namespace App\Http\Requests\Tag;

class TagUpdateRequest extends TagRequest
{
    public function rules()
    {
        return [
            'name'                  => ['required', 'max:50', 'min:3'],
        ];
    }
}
