<?php

namespace App\Http\Requests\Brand;

use Illuminate\Foundation\Http\FormRequest;

class BrandCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title'         => ['array'],
            'title.*'       => ['required', 'min:3'],
            'parent_id'     => ['integer', 'nullable'],
//            'image'         => ['required', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
