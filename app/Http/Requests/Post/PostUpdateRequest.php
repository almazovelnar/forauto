<?php

namespace App\Http\Requests\Post;

class PostUpdateRequest extends PostRequest
{
    public function rules()
    {
        return [
            'image'                 => ['image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')],
            'title'                 => ['array','array','min:1'],
//            'title.*'               => ['required', 'min:3'],
            'description'           => ['array','array','min:1'],
//            'description.*'         => ['required', 'min:10'],
            'category'              => ['required'],
            'tags'                  => ['array'],
        ];
    }
}
