<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'category.0.required'     => trans('admin.validation.minimum_one_category'),
            'chosenPhotos.*.required' => trans('admin.validation.minimum_one_image'),
            'tags.*.min'                  => trans('admin.validation.minimum_tags', ['min' => 3])
        ];
    }

    public function attributes()
    {
        return [
            'tags.*'                => trans('admin.post.tags'),
            'title.*'               => trans('admin.post.title'),
            'description.*'         => trans('admin.post.description'),
            'image_title.*.*'       => trans('admin.post.title'),
            'image_description.*.*' => trans('admin.post.image_description'),
            'price.*'               => trans('admin.post.image_price'),
            'image_author.*'        => trans('admin.post.image_author'),
            'category.0'            => trans('admin.post.category'),
        ];
    }
}
