<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactMessageRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'    => 'required|min:3',
            'surname' => 'required|min:3',
            'email'   => 'nullable|min:3|email',
            'phone'   => 'required|min:6',
            'message' => 'required|min:10',
        ];
    }

    public function attributes()
    {
        return [
            'name'    => trans('site.contact.name'),
            'surname'    => trans('site.contact.surname'),
            'email'   => trans('site.contact.email'),
            'phone' => trans('site.contact.phone'),
            'message' => trans('site.contact.message'),
        ];
    }
}
