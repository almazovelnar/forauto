<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerChangePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password'        => 'required|min:8|same:passwordConfirm',
            'passwordConfirm' => 'required|min:8'
        ];
    }
}
