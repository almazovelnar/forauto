<?php

namespace App\Http\Requests\Customer;

class CustomerRegisterRequest extends CustomerRequest
{

    public function rules(): array
    {
        return [
            'email'           => ['nullable', 'max:60', 'unique:customers,email'],
            'name'            => ['required', 'string', 'max:30'],
            'surname'         => ['required', 'string', 'max:30'],
            'phone'           => ['required', 'max:60', 'unique:customers,phone'],
            'password'        => 'min:8|required_with:passwordConfirm|same:passwordConfirm',
            'passwordConfirm' => 'min:8'
        ];
    }


    public function attributes()
    {
        return [
            'email'           => trans('site.register.email'),
            'name'            => trans('site.register.name'),
            'surname'         => trans('site.register.surname'),
            'phone'           => trans('site.register.phone'),
            'password'        => trans('site.register.password'),
            'passwordConfirm' => trans('site.register.passwordConfirm'),
        ];
    }

//    public function messages()
//    {
//        return [
//            '*.required'      => trans('site.validation.required', ['attribute' => ':attribute']),
//            '*.min'           => trans('site.validation.min', ['attribute' => ':attribute']),
//            '*.max'           => trans('site.validation.max', ['attribute' => ':attribute']),
//            '*.email'         => trans('site.validation.is_email', ['attribute' => ':attribute']),
//            '*.unique'        => trans('site.validation.unique', ['attribute' => ':attribute']),
//            '*.required_with' => trans('site.validation.required_with', ['attribute' => ':attribute']),
//            '*.same'          => trans('site.validation.same', ['attribute' => ':attribute']),
//        ];
//    }

}

