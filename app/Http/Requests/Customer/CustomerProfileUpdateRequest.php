<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerProfileUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $customer = \Auth::user();

        return [
            'email'           => ['nullable', 'email', 'max:60', 'unique:customers,email,' . $customer->id],
            'name'            => ['required', 'string', 'max:30'],
            'surname'         => ['required', 'string', 'max:30'],
            'phone'           => ['nullable', 'regex:/^\+[\d\s]{10,20}$/', 'unique:customers,phone,' . $customer->id],
//            'password'        => 'nullable|min:8|required_with:passwordConfirm|same:passwordConfirm',
//            'passwordConfirm' => 'nullable|min:8'
        ];
    }
}
