<?php

namespace App\Http\Requests\Customer;

use App\Models\Customer;

/**
 * @property Customer $customer
 */
class CustomerUpdateRequest extends CustomerRequest
{
    public function rules()
    {
        return [
            'name'                  => 'required|min:3',
            'surname'               => 'required|min:3',
            'phone'                 => 'required|min:6',
            'email'                 => ['required', 'min:6', 'unique:customers,email,' . $this->customer->id],
            'password_confirmation' => 'same:password'
        ];
    }
}

