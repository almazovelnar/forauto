<?php

namespace App\Http\Requests\Customer;

class CustomerCreateRequest extends CustomerRequest
{
    public function rules()
    {
        return [
            'name'                  => 'required|min:3',
            'surname'               => 'required|min:3',
            'email'                 => 'required|email|unique:customers',
            'phone'                 => 'required|numeric|min:6',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ];
    }
}
