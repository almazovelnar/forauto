<?php

namespace App\Http\Requests\User;

use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class UserUpdateRequest extends UserRequest
{
    public function rules()
    {
        return [
            'name'                  => ['required', 'max:50', 'min:3'],
            'email'                 => ['required', 'email', 'min:6', 'unique:users,email,' . $this->user->id],
            'password_confirmation' => ['same:password'],
            'thumb'                 => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
