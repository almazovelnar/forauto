<div class="social-net">
@foreach($shareUrls as $name => $url)
    <a href="{{str_replace(['{url}', '{text}'], [url()->current(), $post->title], $url) }}">
        <img src="/assets/frontend/img/icon/{{$name}}-footer.svg" alt="{{$name}}">
    </a>
    @endforeach

</div>
