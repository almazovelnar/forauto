@php
    $lang = app()->getLocale();
@endphp
<div class="header__lang no-icon">
    <div class="header__lang__selected">
        <span><img src="/assets/front/images/{{$lang}}.svg" alt=""></span>
        @lang('site.lang.' . $lang)
    </div>
    @if(count($langs = LaravelLocalization::getSupportedLanguagesKeys()) > 1)
        <ul class="header__lang__list">
            @foreach($langs as $locale)
                @if($locale != $lang)
                    <li>
                        <a href="{{ LaravelLocalization::localizeURL(url()->current(), $locale) }}">
                            <span><img src="/assets/front/images/{{$locale}}.svg" alt="@lang('site.lang.' . $locale)"></span> @lang('site.lang.' . $locale)
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    @endif
</div>
