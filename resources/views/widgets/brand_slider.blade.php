@if(!$brands->isEmpty())
    <!-- Partners -->
    <section class="partners m-top">
        <div class="container-fluid">
            <h2 class="section-title">@lang('site.layout.brands')</h2>
            <div class="row partners__slider m-0">
                @foreach($brands as $brand)
                    <div class="col-md-2">
                        <a href="{{ $brand->link }}"><img src="{{ asset('storage/brands/' . $brand->image) }}" alt="{{ $brand->title }}"></a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Partners -->
@endif
