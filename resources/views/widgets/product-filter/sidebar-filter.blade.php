<label for="filter_toggle" class="filter-bar__blur d-md-none"></label>
<div class="filter">
    @if($brands->isNotEmpty())
        <div class="filter__item">
            <div class="filter__head filter__head--with-sub">@lang('site.product.manufacturer')</div>
            <div class="filter__body">
                @foreach($brands as $brand)
                    <div class="form-group form-group--checkbox">
                        <input type="checkbox" class="d-none" id="{{ $brand->slug }}" name="brand[]" value="{{ $brand->slug }}"  @if(in_array($brand->slug, request()->get("brand") ?? [])) checked @endif>
                        <label for="{{ $brand->slug }}">{{ $brand->title }}</label>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    <div class="filter__item">
        <div class="filter__head">@lang('site.product.price_range')</div>
        <div class="filter__body">
            <div class="d-flex">
                <div class="form-group mb-0">
                    <input type="text" class="form-control" name="price[from]" placeholder="@lang('site.product.price_from')" value="{{ request()->get("price")['from'] ?? null }}">
                </div>
                <div class="form-group mb-0">
                    <input type="text" class="form-control" name="price[to]" placeholder="@lang('site.product.price_to')" value="{{ request()->get("price")['to'] ?? null }}">
                </div>
            </div>
        </div>
    </div>

    @foreach($properties as $property)
        @if($property->relates->isNotEmpty())
            <div class="filter__item">
                <div class="filter__head filter__head--with-sub">{{ $property->title }}</div>
                <div class="filter__body">
                    @foreach($property->relates as $relate)
                        <div class="form-group form-group--checkbox">
                            <input type="checkbox" class="d-none" id="{{$property->slug}}-{{$relate->value}}" name="filter[{{$property->slug}}][]" value="{{$relate->value}}" @if(in_array($relate->value, request()->get("filter")[$property->slug] ?? [])) checked @endif>
                            <label for="{{$property->slug}}-{{$relate->value}}">{{$relate->value}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    @endforeach
</div>
