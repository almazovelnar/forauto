<ul>
    <li>
        <div class="products__filter-result__item">
            Shell
            <a href="#" aria-label="close">
                <svg xmlns="http://www.w3.org/2000/svg" width="1rem" height="1rem">
                    <use xlink:href="#svg-close"></use>
                </svg>
            </a>
        </div>
    </li>
    <li>
        <div class="products__filter-result__item">
            0W-20
            <a href="#" aria-label="close">
                <svg xmlns="http://www.w3.org/2000/svg" width="1rem" height="1rem">
                    <use xlink:href="#svg-close"></use>
                </svg>
            </a>
        </div>
    </li>
    <li>
        <a href="{{url()->current()}}" class="btn btn-orange btn--sm">@lang('site.filter.clear')</a>
    </li>
</ul>
