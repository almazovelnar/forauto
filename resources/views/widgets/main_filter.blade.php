<div class="main__filter">
    @if($categories->isNotEmpty())
        <div class="main__filter__tab">
            @foreach($categories as $key => $category)
                <a href="javascript:void(0);" class="@if($key == 0) active @endif btn btn-gray btn--sm tab__item" data-tab="tab{{ $category->id}}">{{ $category->title}}</a>
            @endforeach
        </div>
        <div class="tab-content">
            @foreach($categories as $key => $category)
                <form class="tab-content__item @if($key == 0) active @endif" data-tab="tab{{ $category->id}}" method="GET" action="{{ route('products', $category->slug) }}">
                    <div class="form-group">
                        <label>@lang('site.filter.brand')</label>
                        <select class="form-control  custom-select" name="brand[]">
                            <option value>@lang('site.filter.choose')</option>
                            @foreach($category->brands as $brand)
                                <option value="{{ $brand->slug }}">{{ $brand->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    @foreach($category->mainProperties as $property)
                        <div class="form-group">
                            <label>{{ $property->title }}</label>
                            <select class="form-control custom-select" name="filter[{{ $property->slug }}][]">
                                <option value>@lang('site.filter.choose')</option>
                                @foreach($property->relates as $relate)
                                    <option value="{{ $relate->value }}">{{ $relate->value }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endforeach
                    <div class="main__filter__btns">
                        <button type="submit" class="btn btn--sm btn-orange">@lang('site.filter.search')</button>
                        <button type="reset" class="btn btn--sm btn-orange-transparent">@lang('site.filter.reset')</button>
                    </div>
                </form>
            @endforeach
        </div>
    @endif
</div>
