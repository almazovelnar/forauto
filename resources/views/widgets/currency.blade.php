@if(!empty($currencies))
    <div class="currency-block">
        <div class="container">
            <div class="owl-carousel owl-theme currency-slider">
                @foreach($currencies as $code => $currency)
                    @if(isset($currency[1]) && $currency[0] < $currency[1])
                        @php
                            /**@var $currency **/
                                $decrement=round(($currency[1]-$currency[0])/$currency[1]*100,2)
                        @endphp
                        <div class="cur-box down d-flex item">
                            <i class="arrow"></i>
                            <p>{{$code}} <span>{{$currency[0]}}</span></p>
                        </div>
                    @else
                        <div class="cur-box up d-flex item">
                            <i class="arrow"></i>
                            <p>{{$code}} <span>{{$currency[0]}}</span></p>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </div>
@endif
