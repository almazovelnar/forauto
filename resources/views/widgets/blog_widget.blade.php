@if($posts->count())
    <!-- Blog -->
    <section class="blog m-top">
        <div class="container-fluid">
            <h2 class="section-title">@lang('site.layout.blog')</h2>
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-4">
                        <a href="{{ route('blog.post', $post->slug) }}" class="blog__item">
                            <div class="blog__image"><img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}"></div>
                            <div class="blog__text">
                                <h2>{{ $post->title }}</h2>
                                <p>{{ \App\Helpers\StringHelper::clearContent($post->description) }}</p>
                                <span>
                                    @lang('site.blog.read_more')
                                    <svg xmlns="http://www.w3.org/2000/svg" width="1.4rem" height="1.4rem">
                                        <use xlink:href="#svg-arrow-right"></use>
                                    </svg>
                                </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Blog -->
@endif
