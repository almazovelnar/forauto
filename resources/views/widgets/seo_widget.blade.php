<!-- Tags -->
<section class="tags">
    <div class="container-fluid">
        @if($tags->isNotEmpty())
            <ul class="tags__list">
                @foreach($tags as $tag)
                    <li><a href="{{ $tag->link }}">{{ $tag->name }}</a></li>
                @endforeach
            </ul>
        @endif
        <div class="tags__text">
            {!! site_info('description') !!}
        </div>
    </div>
</section>
<!-- End Tags -->
