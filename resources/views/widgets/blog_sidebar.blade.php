
<div class="blog_sidebar_widget">

{{--    <div class="widget_list widget_search">--}}
{{--        <div class="widget_title">--}}
{{--            <h3>@lang('site.blog.search_title')</h3>--}}
{{--        </div>--}}
{{--        <form action="{{ route('blog.search') }}">--}}
{{--            <input name="query" placeholder="@lang('site.blog.write_search')" type="text">--}}
{{--            <button type="submit">@lang('site.blog.search')</button>--}}
{{--        </form>--}}
{{--    </div>--}}

    @if(!$topPosts->isEmpty())
        <div class="widget_list widget_post">
            <div class="widget_title">
                <h3>@lang('site.blog.popular_posts')</h3>
            </div>
            @foreach($topPosts as $topPost)
                <div class="post_wrapper">
                    <div class="post_thumb">
                        <a href="{{ route('blog.post', $topPost->slug) }}"><img src="{{ post_image($topPost->image) }}" alt="{{ $topPost->title }}"></a>
                    </div>
                    <div class="post_info">
                        <h4><a href="{{ route('blog.post', $topPost->slug) }}">{{ $topPost->title }}</a></h4>
                        <span>{{ $topPost->published_at }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

    @if(!$categories->isEmpty())
        <div class="widget_list widget_categories">
            <div class="widget_title">
                <h3>@lang('site.blog.categories')</h3>
            </div>
            <ul>
                @foreach($categories as $category)
                    <li><a href="{{ route('blog.category', $category->slug) }}">{{ $category->title }}</a></li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!$tags->isEmpty())
        <div class="widget_list widget_tag">
            <div class="widget_title">
                <h3>@lang('site.blog.tags')</h3>
            </div>
            <div class="tag_widget">
                <ul>
                    @foreach($tags as $tag)
                        <li><a href="{{ route('blog.tag', $tag->slug) }}">{{ $tag->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
</div>
