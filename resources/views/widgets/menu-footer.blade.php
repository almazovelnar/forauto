<div class="col d-md-flex">
    @if($servicePages->isNotEmpty())
        <div class="footer__item">
            <h3 class="footer__title">@lang('site.layout.services')</h3>
            <ul class="footer__menu">
                @foreach($servicePages as $servicePage)
                    <li><a href="{{ route('page', $servicePage->slug) }}">{{ $servicePage->title }}</a></li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="footer__item">
        <h3 class="footer__title">@lang('site.layout.about_company')</h3>
        <ul class="footer__menu">
            @if($defaultPages->isNotEmpty())
                @foreach($defaultPages as $defaultPage)
                    <li><a href="{{ route('page', $defaultPage->slug) }}">{{ $defaultPage->title }}</a></li>
                @endforeach
            @endif
            <li><a href="{{ route('blog.index') }}">@lang('site.layout.blog')</a></li>
            <li><a href="{{ route('faq') }}">@lang('site.layout.faq')</a></li>
            <li><a href="{{ route('contact') }}">@lang('site.layout.contact')</a></li>
        </ul>
    </div>
    <div class="footer__item">
        <h3 class="footer__title">@lang('site.layout.contact_info')</h3>
        <ul class="footer__menu">
            @foreach(site_info('email') as $email)
                <li><a href="mailto:{{ $email }}">{{ $email }}</a></li>
            @endforeach
            @foreach(site_info('phone') as $phone)
                <li><a href="tel:{{ $phone }}">{{ $phone }}</a></li><br>
            @endforeach
            {{ site_info('address') }}
        </ul>
    </div>
</div>
