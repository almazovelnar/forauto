
<ul class="offcanvas_main_menu">
    <li class="menu-item-has-children"><a class="active"  href="{{ route('home') }}">@lang('site.layout.home')</a></li>
    <li class="menu-item-has-children"><a href="{{ route('products') }}">@lang('site.layout.products')</a></li>
    <li class="menu-item-has-children"><a href="{{ route('blog.index') }}">@lang('site.layout.blog')</a></li>

    @if(!$pages->isEmpty())
        @foreach($pages as $page)
            <li class="menu-item-has-children"><a href="{{ route('page', $page->slug) }}">{{ $page->title }}</a></li>
        @endforeach
    @endif

    <li class="menu-item-has-children"><a href="{{ route('contact') }}">@lang('site.layout.contact-us')</a></li>
</ul>
