
<!--sidebar widget start-->
<aside class="sidebar_widget filters" data-url="{{ route('products', ['search' => request()->get('search')]) }}">

    <div class="widget_list widget_filter">
        <h3>@lang('site.product.filter_price')</h3>
        <form action="#">
            <div id="slider-range"></div>
            <input type="text" name="price" id="amount"  data-max="{{ get_price($maxPrice) }}"
                   data-start-value="{{ request()->input('filters.startPrice') }}"
                   data-end-value="{{ request()->input('filters.endPrice') }}"
            />
        </form>
    </div>
    @if(!$brands->isEmpty())
        <div class="widget_list widget_categories">
            <h3>@lang('site.product.filter_manufacturers')</h3>
            <ul>
                @foreach($brands as $brand)
                    <li>
                        <input id="{{ $brand->slug }}" value="{{ $brand->slug }}" @if(in_array($brand->slug, request()->input('filters.brand') ?? [])) checked @endif type="checkbox" name="brand[{{ $brand->id }}]" class="filter-brand">
                        <label for="{{ $brand->slug }}">{{ $brand->title }}</label>
                        <span class="checkmark"></span>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="widget_list widget_filter">
        <button type="button" class="submitFilter">@lang('site.product.filter')</button>
    </div>

</aside>
<!--sidebar widget end-->
