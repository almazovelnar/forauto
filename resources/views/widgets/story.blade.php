@if(!empty($story))
<div class="virus-news-link">
    <a href="{{route('story',$story->slug)}}">
        <em><img src="{{asset('storage/stories/'.$story->image)}}" alt="icon"></em>
        <span>{{$story->title}}</span>
        <i></i>
    </a>
</div>
@endif
