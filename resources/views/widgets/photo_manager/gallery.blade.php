<link href="{{ asset('vendor/photo-manager/css/cropper.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/photo-manager/css/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/photo-manager/css/magnific-popup.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/photo-manager/css/main.css') }}" rel="stylesheet">

<div class="form-group form-file-upload form-file-multiple">
    <input type="hidden" value="" name="photos" id="initial" data-main="image">
    <!--suppress HtmlFormInputWithoutLabel -->
    <input style="display: none" name="image" type="radio" value="" id="empty-main">
    <div class="btn-group" role="group">
        <button class="btn btn-primary mr-2" data-toggle="modal" data-target="#photo-manager" type="button">
            <i class="fa fa-file"></i>
            @lang('photo.open_photo_manager')
        </button>
    </div>
</div>

<div class="post-images" data-url="{{ route('admin.photos.sort') }}">
    @if(!empty($photos))
        @foreach($photos as $i => $item)
            <div class="photo-thumb-wrapper">
                <div class="card bg-grey-100 photo-thumb">
                    <input type="hidden" value="{{ $item->id }}" name="photos[]">
                    <img class="card-img-top img-fluid sort" src="{{ get_image($item->filename, $item->path,'list') }}"
                         alt="Card image cap">

                    <div class="btn-group btn-group-justified">
{{--                        <div class="btn-group" role="group">--}}
{{--                            <button type="button"--}}
{{--                                    class="btn btn-primary btn-insert"--}}
{{--                                    data-id="{{ $item->id }}"--}}
{{--                                    title="insert"--}}
{{--                                    data-url="{{ route('admin.photos.get-size') }}"--}}
{{--                            >--}}
{{--                                <i class="fa fa-plus" aria-hidden="true"></i>--}}
{{--                            </button>--}}
{{--                        </div>--}}

{{--                        <div class="btn-group" role="group">--}}
{{--                            <button type="button"--}}
{{--                                    class="btn btn-info trigger-update"--}}
{{--                                    data-url="{{ route('admin.photos.update', ['id' => $item->id]) }}"--}}
{{--                                    title="Update"--}}
{{--                            >--}}
{{--                                <i class="fa fa-edit" aria-hidden="true"></i>--}}
{{--                            </button>--}}
{{--                        </div>--}}

                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-danger btn-remove-card" title="@lang('photo.remove')">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>

{!! $modal !!}

@section('additional_partial_scripts')
    <script src="{{ asset('vendor/photo-manager/js/cropper.min.js') }}"></script>
    <script src="{{ asset('vendor/photo-manager/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('vendor/photo-manager/js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('vendor/photo-manager/js/index.js') }}"></script>
@endsection
