<link href="{{ asset('vendor/photo-manager/css/cropper.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/photo-manager/css/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/photo-manager/css/magnific-popup.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/photo-manager/css/main.css') }}" rel="stylesheet">

<div class="form-group form-file-upload form-file-multiple">
    <input type="hidden" value="{{ $filename }}" id="photo-input">
    <div class="btn-group" role="group">
        <button class="btn btn-primary mr-2" data-toggle="modal" data-target="#photo-manager" type="button">
            <i class="md-file"></i>
            @lang('photo.open_photo_manager')
        </button>
    </div>
</div>

<div class="post-images">
    <img class="img-fluid"
         alt="photo"
         id="photo-img"
         src="{{ !empty($filename) ? get_image($filename) : 'https://dummyimage.com/600x400/cccccc/ffffff&text=no+image' }}"
    >
</div>

{!! $modal !!}

@section('additional_scripts')
    <script src="{{ asset('vendor/photo-manager/js/cropper.min.js') }}"></script>
    <script src="{{ asset('vendor/photo-manager/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('vendor/photo-manager/js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('vendor/photo-manager/js/index.js') }}"></script>
@endsection
