<li><a class="@if(Route::is('home') || Route::currentRouteName() == '') active @endif"  href="{{ route('home') }}">@lang('site.layout.home')</a></li>
@foreach($categories as $category)
    <li><a href="{{ route('products', $category->slug) }}" class="@if(request('slug') == $category->slug) active @endif">{{ $category->title }}</a></li>
@endforeach
