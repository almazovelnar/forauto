@php
    /** @var \Itstructure\GridView\Columns\BaseColumn[] $columnObjects */
    /** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
    /** @var boolean $useFilters */
    $checkboxesExist = false;
@endphp

<div class="card">
    <div class="card-header">
        <div class="float-left">
            <div class="row">
                @if(!$hideCreateButton)
                    <div class="col-auto">
                        @if(!$createUrl)
                            <a href="{{ url()->current() }}/create" class="btn btn-primary">@lang('admin.createButton')</a>
                        @else
                            <a href="{{ $createUrl }}" class="btn btn-primary">@lang('admin.createButton')</a>
                        @endif
                    </div>
                @endif
                @if($langSelect)
                    <div class="col-auto">
                            <select id="langSelector" class="form-control">

                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <option value="{{ $lang }}" @if((cache()->get('current-lang') ?? request()->input("filters.language")) == $lang) selected @endif>{{ strtoupper($lang) }}</option>
                                @endforeach
                            </select>
                    </div>
                @endif
                @if($langCreate)
                    <div class="col-auto">
                            <div class="create-langs">
                                <div class="btn-group">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <a href="{{route('admin.post.create', 'lang='.$lang)}}" class="langCreateBtn btn btn-info" title="@lang('admin.createButton')">
                                            <i style="background-image: url(/images/flag/{{$lang}}.svg)"></i> +
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                    </div>
                @endif
                @if($headerContent)
                    <div class="col-auto">{!! $headerContent !!}</div>
                @endif
                @if ($useFilterSearchButton)
                    <div class="col-auto">
                        <button id="grid_view_search_button" type="button" class="btn btn-info">{{ $searchButtonLabel }}</button>

                    </div>
                @endif
                @if ($useFilterResetButton)
                    <div class="col-auto">
                        <button id="grid_view_reset_button" type="button" class="btn btn-warning">{{ $resetButtonLabel }}</button>
                    </div>
                @endif
            </div>
        </div>

        <div class="float-right">
            @if ($paginator->onFirstPage())
                {!! trans('grid_view::grid.page-info', [
                    'start' => '<b>1</b>',
                    'end' => '<b>' . $paginator->perPage() . '</b>',
                    'total' => '<b>' . $paginator->total() . '</b>',
                ]) !!}
            @elseif ($paginator->currentPage() == $paginator->lastPage())
                {!! trans('grid_view::grid.page-info', [
                    'start' => '<b>' . (($paginator->currentPage() - 1) * $paginator->perPage() + 1) . '</b>',
                    'end' => '<b>' . $paginator->total() . '</b>',
                    'total' => '<b>' . $paginator->total() . '</b>',
                ]) !!}
            @else
                {!! trans('grid_view::grid.page-info', [
                    'start' => '<b>' . (($paginator->currentPage() - 1) * $paginator->perPage() + 1) . '</b>',
                    'end' => '<b>' . (($paginator->currentPage()) * $paginator->perPage()) . '</b>',
                    'total' => '<b>' . $paginator->total() . '</b>',
                ]) !!}
            @endif
        </div>
    </div>
    <div class="card-body">
        <table class="table @if($tableBordered) table-bordered @endif @if($tableStriped) table-striped @endif @if($tableHover) table-hover @endif @if($tableSmall) table-sm @endif">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    @foreach($columnObjects as $column_obj)
                        <th {!! $column_obj->buildHtmlAttributes() !!}>

                            @if($column_obj->getSort() === false || $column_obj instanceof \Itstructure\GridView\Columns\ActionColumn)
                                {{ $column_obj->getLabel() }}

                            @elseif($column_obj instanceof \Itstructure\GridView\Columns\CheckboxColumn)
                                @php($checkboxesExist = true)
                                @if($useFilters)
                                    {{ $column_obj->getLabel() }}
                                @else
                                    <input type="checkbox" id="grid_view_checkbox_main" class="form-control form-control-sm" @if($paginator->count() == 0) disabled="disabled" @endif />
                                @endif

                            @else
                                <a href="{{ \Itstructure\GridView\Helpers\SortHelper::getSortableLink(request(), $column_obj) }}">{{ $column_obj->getLabel() }}</a>
                            @endif

                        </th>
                    @endforeach
                </tr>
                @if ($useFilters)
                    <tr>
                        <form action="" method="get" id="grid_view_filters_form">
                            <td></td>
                            @foreach($columnObjects as $column_obj)
                                <td>
                                    @if($column_obj instanceof \Itstructure\GridView\Columns\CheckboxColumn)
                                        <input type="checkbox" id="grid_view_checkbox_main" class="form-control form-control-sm" @if($paginator->count() == 0) disabled="disabled" @endif />
                                    @else
                                        {!! $column_obj->getFilter()->render() !!}
                                    @endif
                                </td>
                            @endforeach
                            <input type="submit" class="d-none">
                        </form>
                    </tr>
                @endif
            </thead>

            <form action="{{ $rowsFormAction }}" method="post" id="grid_view_rows_form">
                <tbody>
                    @foreach($paginator->items() as $key => $row)
                        <tr>
                            <td>{{ ($paginator->currentPage() - 1) * $paginator->perPage() + $key + 1 }}</td>
                            @foreach($columnObjects as $column_obj)
                                <td>{!! $column_obj->render($row) !!}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="{{ count($columnObjects) + 1 }}">
                            <div class="mx-1">
                                <div class="row">
                                    <div class="col-12 col-xl-8 text-center text-xl-left">
                                        {{ $paginator->render('grid_view::pagination') }}
                                    </div>
                                    <div class="col-12 col-xl-4 text-center text-xl-right">
                                        @if (($checkboxesExist || $useSendButtonAnyway) && $paginator->count() > 0)
                                            <button type="submit" class="btn btn-danger">{{ $sendButtonLabel }}</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tfoot>

                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
            </form>
        </table>
    </div>
</div>
<form method="POST" action="" id="delete-form">
    @csrf
    @method('DELETE')
</form>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
        $('#grid_view_checkbox_main').click(function (event) {
            $('input[role="grid-view-checkbox-item"]').prop('checked', event.target.checked);
        });

        $('#grid_view_search_button').click(function () {
            $('#grid_view_filters_form').submit();
        });

        $('#grid_view_reset_button').click(function () {
            $('input[role="grid-view-filter-item"]').val('');
            $('select[role="grid-view-filter-item"]').prop('selectedIndex', 0);
        });
    });
</script>
