<a class="btn btn-primary btn-sm" href="{!! $url !!}" @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif >
    <i class="fas fa-eye"></i>
</a>&nbsp;
