@extends('backend.layouts.app')

@section('title', trans('admin.home_page'))

@section('content')
{{--    <div class="row mb-3">--}}
{{--        <div class="col-12">--}}
{{--            <h5>@lang('admin.statistic.common_info')</h5>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-orange">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $pagesCount }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.pages') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-person-add"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.pages.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-pink">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $categoryCount }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.categories') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-stats-bars"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.categories.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-warning">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $customersCount }}</h3>--}}

{{--                    <p>{{ trans('admin.customers') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-person-add"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.customers.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-danger">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $authorsCount }}</h3>--}}

{{--                    <p>{{ trans('admin.authors') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-pie-graph"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.authors.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--    </div>--}}

{{--    <div class="row mb-3">--}}
{{--        <div class="col-12">--}}
{{--            <h5>@lang('admin.statistic.media_info')</h5>--}}
{{--        </div>--}}

{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-success">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $postsCount }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.posts') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-stats-bars"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.posts.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-success">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $photosCount }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.photos') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-person-add"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.posts.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-success">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $videosCount }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.videos') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-pie-graph"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.posts.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--    </div>--}}

{{--    <div class="row mb-3">--}}
{{--        <div class="col-12">--}}
{{--            <h5>@lang('admin.statistic.orders_info')</h5>--}}
{{--        </div>--}}

{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-info">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $ordersCount }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.total_orders') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-bag"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.orders.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-info">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $ordersCountForDay }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.last_day_orders') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-bag"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.orders.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-info">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $ordersCountForWeek }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.last_week_orders') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-person-add"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.orders.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-info">--}}
{{--                <div class="inner">--}}
{{--                    <h3>{{ $ordersCountForMonth }}</h3>--}}

{{--                    <p>{{ trans('admin.statistic.last_month_orders') }}</p>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-pie-graph"></i>--}}
{{--                </div>--}}
{{--                <a href="{{ route('admin.orders.index') }}" class="small-box-footer">@lang('admin.read_more') <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- ./col -->--}}
{{--    </div>--}}

{{--    <div class="row mb-3 pt-3">--}}

{{--        @if(!$topSales->isEmpty())--}}
{{--            <div class="col-md-4">--}}
{{--                <div class="card card-widget widget-user-2">--}}
{{--                    <div class="widget-user-header bg-gray">--}}
{{--                        <h5 class="widget-user-desc statistic-widget-user-desc">@lang('admin.statistic.top_sales')</h5>--}}
{{--                    </div>--}}

{{--                    <div class="card-footer p-0">--}}
{{--                        <ul class="nav flex-column">--}}
{{--                            @foreach($topSales as $key => $topSale)--}}
{{--                                @if($key == 0)--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a href="javascript:void(0);" class="nav-link"><span class="badge bg-success">@lang('admin.title')</span>--}}

{{--                                        @foreach(\App\Models\Size::where('is_demo', false)--}}
{{--                                                        ->where('type', \App\Enum\Type::PHOTO)--}}
{{--                                                        ->get() as $size)--}}
{{--                                                <span class="float-right badge bg-success ml-2" style="width: 60px;">{{ $size->title }}</span>--}}
{{--                                        @endforeach--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a href="{{ route('photo', ['code' => $topSale->code]) }}" class="nav-link">--}}
{{--                                        <span class="col-lg-6">--}}
{{--                                            {{ $topSale->title }}--}}
{{--                                        </span>--}}

{{--                                        @foreach(\App\Models\Size::where('is_demo', false)--}}
{{--                                                    ->where('type', \App\Enum\Type::PHOTO)--}}
{{--                                                    ->get() as $size)--}}

{{--                                            @if($topSale->type($size->id))--}}
{{--                                                <span class="float-right badge bg-primary ml-2" style="width: 60px;">{{ $topSale->type($size->id)->bought }}</span>--}}
{{--                                            @else--}}
{{--                                                <span class="float-right badge bg-primary ml-2" style="width: 60px;">-</span>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}

{{--        @if(!$lastMedias->isEmpty())--}}
{{--            <div class="col-md-4">--}}
{{--                <div class="card card-widget widget-user-2">--}}
{{--                    <div class="widget-user-header bg-gray">--}}
{{--                        <h5 class="widget-user-desc statistic-widget-user-desc">@lang('admin.statistic.last_medias')</h5>--}}
{{--                    </div>--}}

{{--                    <div class="card-footer p-0">--}}
{{--                        <ul class="nav flex-column">--}}
{{--                            @foreach($lastMedias as $key => $lastMedia)--}}

{{--                                @if($key == 0)--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a href="javascript:void(0);" class="nav-link">--}}
{{--                                            <span class="badge bg-success">@lang('admin.title')</span>--}}
{{--                                            <span class="float-right badge bg-success ml-2" style="width: 60px;">@lang('admin.date')</span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                @endif--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a href="{{ route('photo', ['code' => $lastMedia->code]) }}" class="nav-link">--}}
{{--                                        {{ $lastMedia->title }}--}}
{{--                                        <span class="float-right badge bg-primary">{{ $lastMedia->created_at->format('Y-m-d H:i') }}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}
{{--    </div>--}}


@endsection
