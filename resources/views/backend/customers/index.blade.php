@extends('backend.layouts.app')

@section('title', trans('admin.customers'))

@section('content')
    {!!
        grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'label' => trans('admin.name'),
                    'value' => function ($data) {
                        return $data->full_name;
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\TextFilter::class,
                        'name' => 'title'
                    ]
                ],
                [
                    'label' => trans('admin.phone'),
                    'attribute' => 'phone',
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\TextFilter::class,
                        'name' => 'phone'
                    ]
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.customers.destroy', $data->id);
                        }
                    ]
                ]
            ]
        ])
    !!}
@endsection

