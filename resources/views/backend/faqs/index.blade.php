@extends('backend.layouts.app')

@section('title', trans('admin.faq'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'columnFields' => [
                [
                    'label' => trans('admin.question'),
                    'attribute' => 'question'
                ],
                [
                    'label' => trans('admin.language'),
                    'attribute' => 'language'
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.config.destroy', $data->id);
                        }
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
