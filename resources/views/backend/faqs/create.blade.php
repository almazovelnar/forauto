@extends('backend.layouts.app')

@section('title', trans('admin.new_faq'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.faq.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="language">@lang('admin.language')</label>
                                    <select id="language" class="form-control @error('language') is-invalid @enderror" name="language">
                                        @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $locale)
                                            <option value="{{$locale}}" @if($locale == old('language')) selected @endif>{{ $locale }}</option>
                                        @endforeach
                                    </select>
                                    @error('language')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="question">@lang('admin.question')</label>
                                    <input type="text" id="question" class="form-control @error('question') is-invalid @enderror" name="question" value="{{ old('question') }}">
                                    @error('question')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="answer">@lang('admin.answer')</label>
                                    <textarea id="answer" class="form-control @error('answer') is-invalid @enderror" name="answer" cols="30" rows="10">{{ old('answer') }}</textarea>
                                    @error('answer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="float-right">
                                        <label for="status">@lang('admin.status.status')</label>
                                        <label class="switch switch-success">
                                            <input type="checkbox" name="status" id="status"
                                                   @if((bool) old('status')==1) checked @endif>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
