<script>
    let _title = $('.autoGenerateSlug');

    _title.on('keyup', function() {
        _lang=$('.tab-pane.fade.show.active').data('lang');
        let _slug = $('#slug-'+_lang);
        _slug.val();
        let txt=convertToSlug($(this).val());
        _slug.val(txt)
    });
    function convertToSlug(text) {
        return text.toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '');
    }
</script>
