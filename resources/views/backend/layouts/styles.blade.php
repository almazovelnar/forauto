<link rel="stylesheet" href="{{ asset('assets/backend/css/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/fileinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('assets/backend/css/admin.css') }}">
