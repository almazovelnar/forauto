<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class  with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('admin.pages.index') }}" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    @lang('admin.menu.pages')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.categories.index') }}" class="nav-link">
                <i class="nav-icon fas fa-align-left"></i>
                <p>
                    @lang('admin.menu.categories')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.brands.index') }}" class="nav-link">
                <i class="nav-icon fas fa-align-left"></i>
                <p>
                    @lang('admin.menu.brands')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.property.index') }}" class="nav-link">
                <i class="nav-icon fas fa-list-alt"></i>
                <p>
                    @lang('admin.menu.properties')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.products.index') }}" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p>
                    @lang('admin.menu.products')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.orders.index') }}" class="nav-link">
                <i class="nav-icon fas fa-shopping-basket"></i>
                <p>
                    @lang('admin.menu.orders')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.customers.index') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    @lang('admin.menu.customers')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.slides.index') }}" class="nav-link">
                <i class="nav-icon fas fa-image"></i>
                <p>
                    @lang('admin.menu.slides')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.faq.index') }}" class="nav-link">
                <i class="nav-icon fas fa-question"></i>
                <p>
                    @lang('admin.menu.faq')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.users.index') }}" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    @lang('admin.menu.users')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.tags.index') }}" class="nav-link">
                <i class="nav-icon fas fa-tags"></i>
                <p>
                    @lang('admin.menu.tags')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0);" class="nav-link">
                <i class="nav-icon fas fa-newspaper"></i>
                <p>
                    @lang('admin.menu.blog')
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('admin.blog.categories.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.categories')
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.blog.posts.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.posts')
                        </p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0);" class="nav-link">
                <i class="nav-icon fas fa-cogs"></i>
                <p>
                    @lang('admin.menu.settings')
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('admin.main-info') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.main-info')
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.config.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.configs')
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.translates.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.translates')
                        </p>
                    </a>
                </li>
            </ul>
        </li>
    </ul>

    <a data-href="{{ route('admin.logout') }}" id="logout-button">{{ trans('admin.logout') }}</a>
</nav>
