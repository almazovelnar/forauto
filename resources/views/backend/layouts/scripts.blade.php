<script src="{{ asset('assets/backend/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>

<script src="{{ asset('assets/backend/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/jquery.knob.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/adminlte.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/backend/js/fileinput.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/admin.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $('.datetimepicker').datetimepicker({
            icons: {time: 'far fa-clock'},
            format: 'YYYY-MM-DD HH:mm:ss',
            pickDate: false,
            pickSeconds: false,
            pick12HourFormat: false,
            ignoreReadonly: true
        });

        if ($('.ckEditor').length > 0) {
            $('.ckEditor').each(function () {
                CKEDITOR.replace($(this).prop('id'), {
                    filebrowserUploadUrl: "{{route('admin.ckeupload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form',
                    height: 400,
                    removeButtons: 'Image'
                });
            });
        }

        $('.delete-btn').click(function () {
            let res = confirm("{{ trans('admin.delete_item_confirm') }}");
            if (!res) return false;

            let url = $(this).data('action');
            let form = $('#delete-form');
            form.attr('action', url).submit()
        });

        $('select:not(#langSelector)').on('change', function () {
            $('#grid_view_filters_form').submit()
        })


        if ($(".file_uploader").length > 0) {
            $(".file_uploader").each(function () {
                let fileUploader = $(this)
                let fileUploaderOptions = {};
                if ((fileUploaderValue = fileUploader.attr('value'))) {
                    fileUploaderOptions.initialPreview = fileUploaderValue
                }
                fileUploaderOptions.initialPreviewShowDelete = false;
                if ((fileUploaderDeleteThumbUrl = fileUploader.data('delete'))) {
                    fileUploaderOptions.deleteUrl = fileUploaderDeleteThumbUrl
                    fileUploaderOptions.initialPreviewShowDelete = true;
                }
                fileUploaderOptions.showRemove = false;
                fileUploaderOptions.maxFileCount = 5;
                fileUploaderOptions.initialPreviewDownloadUrl = fileUploaderValue;
                fileUploaderOptions.initialPreviewAsData = true;
                fileUploaderOptions.overwriteInitial = false;
                fileUploaderOptions.showUpload = false;
                fileUploaderOptions.showCancel = false;
                fileUploaderOptions.showClose = false;
                fileUploaderOptions.initialPreviewConfig = [{
                    type: fileUploader.data('type'),
                },]

                fileUploader.fileinput(fileUploaderOptions);
            })

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        $('#logout-button').click(function () {
            let w = window, confirm = w.confirm("{{ trans('admin.logout') }}");
            if (confirm) {
                w.location.href = $(this).data('href');
            }
        });

        $('.marked_words').select2({
            tags: true,
            allowClear: true,
            minimumInputLength: 3,
        });

        $('.tags').select2({
            tags: true,
            allowClear: true,
            minimumInputLength: 3,
            ajax: {
                type: 'GET',
                url: "{{ route('admin.posts.tag-list') }}",
                dataType: 'json',
                data: function (params) {
                    return {lang: $(this).closest('.tab-pane').data('lang'), q: params.term};
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.name
                            }
                        })
                    };


                },
            }
        });

        let _alert = $(".alert");
        if (_alert.length)
            _alert.animate({opacity: 1.0}, 2000).fadeOut("slow");

        $('#category_id').on('change', function (e) {
            e.preventDefault();
            let that = $(this);
            let categoryId = that.val();

            $.ajax({
                url: "{{ route('admin.property.get-list') }}",
                data: {category_id: categoryId},
                success: function (res) {
                    $('.properties-block').html(res.html);
                }
            })
        })
    })
</script>

@yield('additional_scripts')
@yield('additional_partial_scripts')
