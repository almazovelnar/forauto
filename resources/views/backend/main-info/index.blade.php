@extends('backend.layouts.app')

@section('title', trans('admin.main_info'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.main-info.update') }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-md-8">

                        <div class="card card-primary card-outline card-tabs">
                            <div class="card-header p-0 pt-1 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                            <div class="form-group">
                                                <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                                <input type="text" id="title-{{ $lang }}" class="form-control @error('title.' . $lang) is-invalid @enderror" name="title[{{ $lang }}]" value="{{ $info->getTranslation('title', $lang) }}">
                                                @error('title.' . $lang)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                                <textarea
                                                    name="description[{{ $lang }}]"
                                                    id="description-{{ $lang }}"
                                                    class="form-control @error('description.' . $lang) is-invalid @enderror"
                                                    rows="4"
                                                >{{ $info->getTranslation('description', $lang) }}</textarea>
                                                @error('description.' . $lang)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="keywords-{{ $lang }}">@lang('admin.keywords')</label>
                                                <textarea
                                                    name="keywords[{{ $lang }}]"
                                                    id="keywords-{{ $lang }}"
                                                    class="form-control @error('keywords.' . $lang) is-invalid @enderror"
                                                    rows="4"
                                                >{{ $info->getTranslation('keywords', $lang) }}</textarea>
                                                @error('keywords.' . $lang)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="address-{{ $lang }}">@lang('admin.address')</label>
                                                <textarea id="address-{{ $lang }}" rows="4" class="form-control @error('address.' . $lang) is-invalid @enderror" name="address[{{ $lang }}]">{{ $info->getTranslation('address', $lang) }}</textarea>
                                                @error('address.' . $lang)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="card card-primary card-tabs">
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    <div class="form-group">
                                        <label for="name">@lang('admin.name')</label>
                                        <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $info->name }}">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group email-inputs">
                                        <label for="email">@lang('admin.email')</label>
                                        <a class="inputCreator" data-make-type="email" data-make-name="email[]"><img src="/images/plus-circle.svg"></a>
                                        @foreach($info->email as $index => $email)
                                            @if($index === 0)
                                                <input type="text" id="email" class="form-control @error('email') is-invalid @enderror" name="email[]" value="{{ $email ?? '' }}">
                                            @else
                                                <input type="text" value="{{ $email }}" class="form-control new-input" name="email[]">
                                            @endif
                                        @endforeach

                                        @error("email")
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                    <div class="form-group phone-inputs">
                                        <label for="phone">@lang('admin.phone')</label>
                                        <a class="inputCreator" data-make-type="phone" data-make-name="phone[]"><img src="/images/plus-circle.svg"></a>
                                        @foreach($info->phone as $index => $phone)
                                            @if($index === 0)
                                                <input type="text" id="phone" class="form-control @error('phone') is-invalid @enderror" name="phone[]" value="{{ $phone ?? '' }}">
                                            @else
                                                <input type="text" value="{{ $phone }}" class="form-control new-input" name="phone[]">
                                            @endif
                                        @endforeach

                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="fax">@lang('admin.fax')</label>
                                        <input type="text" id="fax" class="form-control @error('fax') is-invalid @enderror" name="fax" value="{{ $info->fax }}">
                                        @error('fax')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="location">@lang('admin.location')</label>
                                        <input type="text" id="location" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ $info->location }}">
                                        @error('location')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="logo">@lang('admin.logo')</label>

                                        <input
                                            value="{{ $info->logo ? asset('storage/main-info/' . $info->logo) : null }}"
                                            name="logo"
                                            id="input-id"
                                            type="file"
                                            class="file_uploader @error('info') is-invalid @enderror"
                                            data-preview-file-type="text"
                                            data-delete="{{ route('admin.main-info.delete-logo', $info->id) }}"
                                            accept="image/*"
                                        >
                                        @error('logo')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="favicon">@lang('admin.favicon')</label>
                                        <input
                                            value="{{ $info->favicon ? asset('storage/main-info/' . $info->favicon) : null }}"
                                            name="favicon"
                                            id="input-id"
                                            type="file"
                                            class="file_uploader @error('info') is-invalid @enderror"
                                            data-delete="{{ route('admin.main-info.delete-favicon', $info->id) }}"
                                            data-preview-file-type="text"
                                            accept="image/*"
                                        >
                                        @error('favicon')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
