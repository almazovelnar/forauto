@extends('backend.layouts.app')

@section('title', trans('admin.products'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
//            'headerContent' => "<a class='btn btn-warning' href='" . route('admin.products.import') . "'>" . trans('admin.import') . "</a>",
            'columnFields' => [
                [
                    'label' => trans('admin.image'),
                    'attribute' => 'image',
                    'value' => function ($data) {
                        return $data->image ? "<img src='" . asset("storage/products/{$data->image}") . "' style='width:100px'>" : "";
                    },
                    'filter' => false,
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.code'),
                    'attribute' => 'code'
                ],
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title',
                    'value' => function (\App\Models\Product $model) {
                        return $model->title;
                    }
                ],
                [
                    'label' => trans('admin.price'),
                    'attribute' => 'price',
                    'value' => function (\App\Models\Product $model) {
                        return get_price($model->price);
                    }
                ],
                [
                    'label' => trans('admin.count'),
                    'attribute' => 'count'
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.products.destroy', $data->id);
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
