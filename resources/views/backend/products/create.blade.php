@extends('backend.layouts.app')

@section('title', trans('admin.new_product'))

@section('content')
    <div>
        <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}"
                                           data-toggle="pill" href="#tablink-{{ $lang }}" role="tab"
                                           aria-controls="tablink-{{ $lang }}"
                                           aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif"
                                         id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}" data-lang="{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" name="title[{{ $lang }}]" id="title-{{ $lang }}"
                                                   class="form-control autoGenerateSlug @error('title.' . $lang) is-invalid @enderror"
                                                   value="{{ old('title.' . $lang) }}">

                                            @error('title.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}"
                                                      class="form-control" rows="4">{{ old('description.' . $lang) }}</textarea>

                                            @error('description.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="specifications-{{ $lang }}">@lang('admin.specifications')</label>
                                            <textarea name="specifications[{{ $lang }}]" id="specifications-{{ $lang }}"
                                                      class="ckEditor form-control">{{ old('specifications.' . $lang) }}</textarea>

                                            @error('specifications.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        @include('backend.partials._meta_create')

                                    </div>
                                @endforeach

                                <div class="form-group">
                                    @widget('PhotoManager\PhotoManager', ['photos' => $photos])

                                    @error('photos')
                                    <span class="text-danger">{{ $message}}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="category_id">@lang('admin.category')</label>
                                <select id="category_id" class="form-control custom-select" name="category_id">
                                    <option value=""> - </option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" @if(old('category_id') == $category->id) selected @endif>
                                            {{ $category->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="properties-block">
                                @if(old('property'))
                                    @include('backend.partials.properties', [
                                        'properties' => $properties,
                                        'data' => old('property')
                                    ])
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="brand_id">@lang('admin.brand')</label>
                                <select id="brand_id" class="form-control custom-select" name="brand_id">
                                    <option value=""> - </option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand->id }}" @if(old('brand_id') == $brand->id) selected @endif>
                                            {{ $brand->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="code">@lang('admin.code')</label>
                                <input type="text" name="code" id="code"
                                       class="form-control @error('code') is-invalid @enderror"
                                       value="{{ old('code') }}">

                                @error('code')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="package">@lang('admin.package')</label>
                                <input type="text" name="package" id="package"
                                       class="form-control @error('package') is-invalid @enderror"
                                       value="{{ old('package') }}">

                                @error('package')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="count">@lang('admin.count')</label>
                                <input type="number" name="count" id="count"
                                       class="form-control @error('count') is-invalid @enderror"
                                       value="{{ old('count') }}">

                                @error('count')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="price">@lang('admin.price')</label>
                                <input type="text" name="price" id="price"
                                       class="form-control @error('price') is-invalid @enderror"
                                       value="{{ old('price') }}">

                                @error('price')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="sale_price">@lang('admin.sale_price')</label>
                                <input type="text" name="sale_price" id="sale_price"
                                       class="form-control @error('sale_price') is-invalid @enderror"
                                       value="{{ old('sale_price') }}">

                                @error('sale_price')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="stars">@lang('admin.stars')</label>
                                <input type="text" name="stars" id="stars" max="5" min="0"
                                       class="form-control @error('stars') is-invalid @enderror"
                                       value="{{ old('stars') }}">

                                @error('stars')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input name="image" id="input-id" type="file" class="file_uploader"
                                       data-preview-file-type="text">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group d-none">
                                <label for="season_banner">@lang('admin.season_banner')</label>
                                <input name="season_banner" id="season_banner" type="file" class="file_uploader" data-preview-file-type="text">
                                @error('season_banner')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="hot">@lang('admin.is_hot')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="hot" name="hot"
                                               @if((bool) old('hot')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="is_season">@lang('admin.is_season')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="is_season" id="is_season"
                                               @if((bool) old('is_season')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="chosen">@lang('admin.chosen')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="chosen" name="chosen"
                                               @if((bool) old('chosen')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="is_new">@lang('admin.is_new')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="is_new" id="is_new"
                                               @if((bool) old('is_new')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="float-right flex-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status"
                                               @if((bool) old('status')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')"
                                           class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
