@extends('backend.layouts.app')

@section('title', trans('admin.import_products'))

@section('content')
    <div>
        <form action="{{ route('admin.products.import-products') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group">
                        <label for="file">@lang('admin.file')</label>
                        <input name="file" id="input-id" type="file" class="file_uploader"
                               data-preview-file-type="text">
                        @error('file')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <br>

                    <div class="clearfix"></div>

                    <div class="form-group">
                        <div class="float-left">
                            <input type="submit" value="@lang('admin.submit')" class="btn btn-success">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
