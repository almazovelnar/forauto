@extends('backend.layouts.app')

@section('title', trans('admin.orders'))

@section('content')
    {!!
        grid([
            'dataProvider' => $dataProvider,
            'hideCreateButton' => true,
            'columnFields' => [
                [
                    'label' => trans('admin.name'),
                    'attribute' => 'customer_name',
                    'value' => function ($data) {
                        return $data->full_name;
                    },
                    'filter' => false
                ],
                [
                    'label' => trans('admin.email'),
                    'attribute' => 'email'
                ],
                [
                    'label' => trans('admin.total_price'),
                    'attribute' => 'total_price',
                    'value' => function($data) {
                        return get_price($data->total_price) . ' AZN';
                    }
                ],
                [
                    'label' => trans('admin.total_count'),
                    'attribute' => 'total_count',
                    'value' => function($data) {
                        return $data->total_count;
                    }
                ],
                [
                    'label' => trans('admin.created_at'),
                    'attribute' => 'created_at',
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\OrderStatus::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\OrderStatus::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'view' => function ($data) {
                            return route('admin.orders.show', $data->id);
                        },
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.orders.destroy', $data->id);
                        }
                    ]
                ]
            ]
        ])
    !!}
@endsection

