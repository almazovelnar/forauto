@extends('backend.layouts.app')

@section('title', trans('admin.order', ['title' => $order->order_id]))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.orders.update', $order->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-secondary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <select id="status" class="form-control custom-select" name="status">
                                        <option value="2" @if($order->status==2) selected @endif>@lang('admin.status.canceled')</option>
                                        <option value="1" @if($order->status==1) selected @endif>@lang('admin.status.completed')</option>
                                        <option value="0" @if($order->status==0) selected @endif>@lang('admin.status.pending')</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <input type="submit" name="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
