@extends('backend.layouts.app')

@section('title', trans('admin.order', ['id' => $order->order_id]))

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>{{ $order->full_name}}</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="text-muted">
                        <p><strong>@lang('admin.phone')</strong></p>
                        {{ $order->phone }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-muted">
                        <p><strong>@lang('admin.email')</strong></p>
                        {{ $order->email }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-muted">
                        <p><strong>@lang('admin.status.status')</strong></p>
                        @if($order->status == '0')
                            {{ trans('admin.status.pending') }}
                        @elseif($order->status == '1')
                            {{ trans('admin.status.completed') }}
                        @else
                            {{ trans('admin.status.canceled') }}
                        @endif
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <div class="text-muted">
                        <p><strong>@lang('admin.total_count')</strong></p>
                        {{ $order->total_count }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-muted">
                        <p><strong>@lang('admin.total_price')</strong></p>
                        {{ get_price($order->total_price) }} AZN
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-muted">
                        <p><strong>@lang('admin.date')</strong></p>
                        {{ $order->created_at }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

    <div class="card card-gray">
        <div class="card-header">
            <h3 class="card-title">{{ trans('admin.order_items') }}</h3>
        </div>
        <div class="card-body">
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>
                                <span>@lang('admin.image')</span>
                            </th>
                            <th>
                                <span>@lang('admin.product')</span>
                            </th>
                            <th>
                                <span>@lang('admin.count')</span>
                            </th>
                            <th>
                                <span>@lang('admin.price')</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order->items as $key => $item)
                            @php if(!($product = $item->product)) continue; @endphp
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td><a href="{{ route('product', $product->slug) }}"><img src="{{ product_image($product->image) }}" alt="{{ $product->title }}" height="100"></a></td>
                                <td><a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a></td>
                                <td>{{ $item->count }}</td>
                                <td>{{ get_price($item->price) }} AZN</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
