<hr>
<h2>Meta</h2>
@if($lang)
<div class="form-group">
    <label for="meta-title-{{ $lang }}">@lang('admin.meta.title')</label>
    <input type="text" name="meta[{{ $lang }}][title]"
           id="meta-{{ $lang }}-title"
           class="form-control autoGenerateSlug @error('meta.' . $lang.'.title') is-invalid @enderror"
           value="{{ old('meta.' . $lang.'.title',$data->getTranslation('meta', $lang)['title']) }}">

    @error('meta.' . $lang.'.title')
    <span class="text-danger">{{ $message . '-' . $lang }}</span>
    @enderror
</div>

<div class="form-group">
    <label
        for="meta-{{ $lang }}-description">@lang('admin.meta.description')</label>
    <textarea name="meta[{{ $lang }}][description]"
              id="meta-{{ $lang }}-description" class="form-control"
              rows="4">{{ old('meta.' . $lang.'.description',$data->getTranslation('meta', $lang)['description']) }}</textarea>
    @error('meta.' . $lang.'.description')
    <span class="text-danger">{{ $message . '-' . $lang.'.description' }}</span>
    @enderror
</div>
<div class="form-group">
    <label for="meta-{{ $lang }}-keywords">@lang('admin.meta.keywords')</label>
    <input type="text" name="meta[{{ $lang }}][keywords]"
           id="meta-{{ $lang }}-keywords"
           class="form-control autoGenerateSlug @error('meta.' . $lang.'.keywords') is-invalid @enderror"
           value="{{ old('meta.' . $lang.'.keywords',$data->getTranslation('meta', $lang)['keywords']) }}">
    @error('meta.' . $lang.'.keywords')
    <span class="text-danger">{{ $message . '-' . $lang.'.keywords' }}</span>
    @enderror
</div>
@else
    <div class="form-group" >
        <label for="meta-title">@lang('admin.meta.title')</label>
        <input type="text" name="meta[title]" id="meta-title" class="form-control autoGenerateSlug @error('meta.title') is-invalid @enderror" value="{{ old('meta.title',$data->meta()->title) }}" >

        @error('meta.title')
        <span class="text-danger">{{ $message  }}</span>
        @enderror
    </div>

    <div class="form-group">
        <label for="meta-description">@lang('admin.meta.description')</label>
        <textarea name="meta[description]" id="meta-description" class="form-control" rows="4">{{ old('meta.description',$data->meta()->description) }}</textarea>

        @error('meta.description')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group" >
        <label for="meta-keywords">@lang('admin.meta.keywords')</label>
        <input type="text" name="meta[keywords]" id="meta-keywords" class="form-control  @error('meta.keywords') is-invalid @enderror" value="{{ old('meta.keywords',$data->meta()->keywords) }}" >

        @error('meta.keywords')
        <span class="text-danger">{{ $message  }}</span>
        @enderror
    </div>
@endif
