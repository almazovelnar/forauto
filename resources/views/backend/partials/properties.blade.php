@foreach($properties as $property)
    <div class="form-group">
        <label for="{{ $property->slug }}">{{ $property->title }}</label>
        <input type="text" name="property[{{ $property->id }}]" id="{{ $property->slug }}"
               class="form-control @error("property.{$property->id}") is-invalid @enderror"
               value="{{ old("property.{$property->id}", $data[$property->id] ?? null) }}">

        @error("property.{$property->id}")
        <span class="text-danger">{{ $message}}</span>
        @enderror
    </div>
@endforeach

<hr>
