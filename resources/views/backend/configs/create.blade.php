@extends('backend.layouts.app')

@section('title', trans('admin.new_config'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.config.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="param">@lang('admin.config_param')</label>
                                    <input type="text" id="param" class="form-control @error('param') is-invalid @enderror" name="param" value="{{ old('param') }}">
                                    @error('param')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="value">@lang('admin.config_value')</label>
                                    <textarea name="value" id="value" rows="4" class="form-control @error('value') is-invalid @enderror">{{ old('value') }}</textarea>
                                    @error('value')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="default">@lang('admin.config_default')</label>
                                    <textarea name="default" id="default" rows="4" class="form-control @error('default') is-invalid @enderror">{{ old('default') }}</textarea>

                                    @error('default')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="label">@lang('admin.config_label')</label>
                                    <input type="text" id="label" class="form-control @error('label') is-invalid @enderror" name="label" value="{{ old('label') }}">
                                    @error('label')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="type">@lang('admin.config_type')</label>
                                    <input type="text" id="type" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}">
                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
