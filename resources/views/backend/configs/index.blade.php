@extends('backend.layouts.app')

@section('title', trans('admin.configs'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'columnFields' => [
                [
                    'label' => trans('admin.config_param'),
                    'attribute' => 'param'
                ],
                [
                    'label' => trans('admin.config_value'),
                    'attribute' => 'value'
                ],
                [
                    'label' => trans('admin.config_default'),
                    'attribute' => 'default'
                ],
                [
                    'label' => trans('admin.config_label'),
                    'attribute' => 'label'
                ],
                [
                    'label' => trans('admin.config_type'),
                    'attribute' => 'type'
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.config.destroy', $data->id);
                        }
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
