@extends('backend.layouts.app')

@section('title', trans('admin.new_property'))

@section('content')
    <div>
        <form action="{{ route('admin.property.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" name="title[{{ $lang }}]" id="title-{{ $lang }}" class="form-control @error('title.' . $lang) is-invalid @enderror" value="{{ old('title.' . $lang) }}">

                                            @error('title.' . $lang)
                                                <span class="text-danger">{{ $message . '-' . $lang }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="category">@lang('admin.category')</label>
                                <select name="category_id" id="category" class="form-control @error('category_id') is-invalid @enderror">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" @if(old('category_id') == $category->id) selected @endif>{{ $category->title }}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="slug">@lang('admin.slug')</label>
                                <input type="text" name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}">
                                @error('slug')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                            <div class="form-group">
                                <div class="float-left">
                                    <label for="on_filter">@lang('admin.on_main_filter')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="on_filter" id="on_filter" @if(old('on_filter') =='on') checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if(old('status') =='on') checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
