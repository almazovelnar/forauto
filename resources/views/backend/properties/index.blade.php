@extends('backend.layouts.app')

@section('title', trans('admin.properties'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title',
                    'value' => function (\App\Models\Property $model) {
                        return $model->title;
                    }
                ],
                [
                    'label' => trans('admin.category'),
                    'attribute' => 'title',
                    'value' => function (\App\Models\Property $model) {
                        return $model->category->title;
                    }
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.property.destroy', $data->id);
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
