@extends('backend.layouts.app')

@section('title', $slide->title)

@section('content')
    <div>
        <form action="{{ route('admin.slides.update', $slide->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ $slide->getTranslation('title', $lang) }}" name="title[{{ $lang }}]" id="title-{{ $lang }}" class="form-control @error('title.' . $lang) is-invalid @enderror">

                                            @error('title.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="link-{{ $lang }}">@lang('admin.link')</label>
                                            <input type="text" value="{{ $slide->getTranslation('link', $lang) }}" name="link[{{ $lang }}]" id="link-{{ $lang }}" class="form-control @error('link.' . $lang) is-invalid @enderror">

                                            @error('link.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="info-{{ $lang }}">@lang('admin.info')</label>
                                            <input type="text" value="{{ $slide->getTranslation('info', $lang) }}" name="info[{{ $lang }}]" id="info-{{ $lang }}" class="form-control @error('info.' . $lang) is-invalid @enderror">

                                            @error('info.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}" class="form-control" rows="4">{!! $slide->getTranslation('description', $lang) !!}</textarea>

                                            @error('link.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input
                                    value="{{ $slide->image ? asset('storage/slides/' . $slide->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('info') is-invalid @enderror"
                                    data-preview-file-type="text">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>


                            <!-- Date and time -->
                            <div class="form-group">
                                <label for="published_at">@lang('admin.date')</label>
                                <div class="input-group date datetimepicker" id="published_at" data-target-input="nearest">
                                    <input type="text" name="published_at" class="form-control datetimepicker-input" data-target="#published_at" value="{{ old('published_at', $slide->published_at) ?? now() }}"/>
                                    <div class="input-group-append" data-target="#published_at" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($slide->status==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
