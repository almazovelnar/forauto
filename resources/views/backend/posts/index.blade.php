@extends('backend.layouts.app')

@section('title', trans('admin.posts'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'label' => trans('admin.image'),
                    'attribute' => 'image',
                    'value' => function ($data) {
                        return "<img src='" . asset("storage/posts/{$data->image}") . "' style='width:100px'>";
                    },
                    'filter' => false,
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title'
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.blog.posts.destroy', $data->id);
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
