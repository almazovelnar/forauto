@extends('backend.layouts.app')

@section('title', trans('admin.post_title', ['title' => $post->title]))


@section('content')
    <div class="pb-3">
        <form action="{{ route('admin.blog.posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}" data-lang="{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ old('title.' . $lang) ?? $post->getTranslation('title', $lang) }}"
                                                   name="title[{{ $lang }}]" id="title-{{ $lang }}"
                                                   class="autoGenerateSlug form-control @error('title.' . $lang) is-invalid @enderror">
                                            @error('title.' . $lang)
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="slug-{{ $lang }}">@lang('admin.slug')</label>
                                            <input type="text" value="{{ old('slug.' . $lang) ?? $post->getTranslation('slug', $lang) }}" name="slug[{{ $lang }}]"
                                                   id="slug-{{ $lang }}" class="generatedSlug form-control @error('slug.' . $lang) is-invalid @enderror">
                                            @error('slug.' . $lang)
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}" class="form-control" rows="20">{{ old('description.' . $lang) ?? $post->getTranslation('description', $lang) }}</textarea>

                                            @error('description.' . $lang)
                                            <span class="text-danger">{{ $message}}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="tags-{{ $lang }}">@lang('admin.tags')</label>
                                            <select name="tags[{{ $lang }}][]" id="tags-{{ $lang }}" class="tags form-control" multiple data-placeholder="@lang('admin.all_tags')">
                                                @if(old('tags.' . $lang))
                                                    @foreach(old('tags.' . $lang) as $tag)
                                                        <option value="{{ $tag }}" selected>{{ $tag }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach($post->tags->where('language', $lang) as $tag)
                                                        <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('tags')
                                            <span class="text-danger">{{ $message}}</span>
                                            @enderror

                                            @error('tags.' . $lang)
                                            <span class="text-danger">{{ $message}}</span>
                                            @enderror
                                        </div>

                                        @if($post->getTranslation('meta', $lang))
                                            @include('backend.partials._meta_update',['data' => $post])
                                        @else
                                            @include('backend.partials._meta_create')
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="category">@lang('admin.post_category')</label>
                                <select name="category" id="category" class="form-control select2">
                                    @foreach(\App\Models\PostCategory::all() as $category)
                                            <option value="{{ $category->id }}" @if(old('category', $post->category_id) == $category->id) selected @endif>{{ $category->title }}</option>
                                    @endforeach
                                </select>

                                @error('category')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                @error('category.0')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!-- Date and time -->
                            <div class="form-group">
                                <label for="date">@lang('admin.date')</label>
                                <div class="input-group date datetimepicker" id="date" data-target-input="nearest">
                                    <input type="text" name="date" class="form-control datetimepicker-input" data-target="#date" value="{{ old('date', $post->date) ?? now() }}"/>
                                    <div class="input-group-append" data-target="#date" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input
                                    value="{{ $post->image ? asset('storage/posts/' . $post->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('image') is-invalid @enderror"
                                    data-preview-file-type="text">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="chosen">@lang('admin.chosen')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="chosen"  name="chosen" @if($post->chosen==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($post->status==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="float-right">
                                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
