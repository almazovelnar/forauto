@foreach($dimensions as $dimension)
    <div class="form-group">
        <label for="dimension-{{ $dimension['id'] }}">{{ $dimension['title'][config('app.admin_locale')] }}</label>
        <input id="dimension-{{ $dimension['id'] }}" type="text" value="{{ get_price($dimension['price']) }}" name="price[{{ $dimension['id'] }}]" class="form-control">
    </div>
@endforeach
