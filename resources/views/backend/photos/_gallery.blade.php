@foreach($data as $i => $item)
    <div class="photo-thumb-wrapper">
        <div class="card bg-grey-100 photo-thumb">
            <input name="{{ $item['name'] }}[]" type="hidden" value="{{ $item['value'] }}">
            <img class="card-img-top img-fluid sort" src="{{ get_image($item['filename'], $item['path'],'list') }}"
                 alt="Card image cap">
            <div class="btn-group btn-group-justified">
{{--                <div class="btn-group" role="group">--}}
{{--                    <button type="button" class="btn btn-primary btn-insert" data-id="{{ $item['value'] }}"--}}
{{--                            data-url="{{ route('admin.photos.get-size') }}">--}}
{{--                        <i class="fa fa-plus" aria-hidden="true" ></i>--}}
{{--                    </button>--}}
{{--                </div>--}}

{{--                <div class="btn-group" role="group">--}}
{{--                    <button type="button"--}}
{{--                            class="btn btn-info trigger-update"--}}
{{--                            data-url="{{ route('admin.photos.update', ['id' => $item['value']]) }}"--}}
{{--                    >--}}
{{--                        <i class="fa fa-edit" aria-hidden="true"></i>--}}
{{--                    </button>--}}
{{--                </div>--}}

                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-danger btn-remove-card">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endforeach
