<form action="{{ route('admin.photos.update.submit', ['id' => $photo->id]) }}" id="update-photo-form" class="update-photo-form" method="POST">
    <div class="modal-header">
        <h4 class="modal-title">@lang('photo.update_photo')</h4>

        <button type="button" class="close" data-dismiss="modal">
            <i class="icon md-close"></i>
        </button>
    </div>

    <div class="modal-body">
        <ul class="nav nav-tabs nav-tabs-line">
            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $languageCode)
                <li class="nav-item">
                    <a href="#pm-{{ $languageCode }}"
                       class="nav-link{{ $languageCode == config('app.admin_locale')  ? ' active' : '' }}"
                       data-toggle="tab">{{ mb_strtoupper($languageCode) }}</a>
                </li>
            @endforeach
        </ul>
        <!-- Nav tabs -->
        <div class="tab-content pt-15">

            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $languageCode)
                <div class="tab-pane {{ $languageCode == config('app.admin_locale')  ? ' active' : '' }}"
                     role="tabpanel"
                     id="pm-{{ $languageCode }}"
                >
                    <br>
                    <div class="form-group">
                        <input class="form-control" type="text" name="title[{{ $languageCode }}]" placeholder="@lang('photo.title')" value="{{ $photo->getTranslation('title', $languageCode) }}">
                    </div>
                    <div class="form-group">
                        <textarea name="description[{{ $languageCode }}]" id="" rows="6" class="form-control" placeholder="@lang('photo.description')">{{ $photo->getTranslation('description', $languageCode) }}</textarea>
                    </div>
                </div>
            @endforeach

            <div class="form-group">
                <select name="author" id="author" class="form-control">
                    <option value="">@lang('admin.select_author')</option>
                    @foreach(\App\Models\User::query()->whereHas(
                            'roles', function($q){
                                $q->where('name', 'author');
                            }
                        )->get() as $author)
                        <option value="{{ $author->id }}" @if($photo->author_id == $author->id) selected @endif>{{ $author->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Date and time -->
            <div class="form-group">
                <label for="published_at">@lang('admin.date')</label>
                <div class="input-group date datetimepicker" id="published_at" data-target-input="nearest">
                    <input type="text" name="published_at" class="form-control datetimepicker-input" data-target="#published_at" value="{{ $photo->published_at }}"/>
                    <div class="input-group-append" data-target="#published_at" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>

{{--            <div class="form-group">--}}
{{--                <div class="float-right fr">--}}
{{--                    <label for="is_exclusive">@lang('admin.is_exclusive')</label>--}}
{{--                    <label class="switch switch-success">--}}
{{--                        <input type="checkbox" name="is_exclusive" id="is_exclusive" @if($photo->is_exclusive === 1) checked @endif>--}}
{{--                        <span class="slider round"></span>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--                <div class="clearfix"></div>--}}
{{--            </div>--}}
        </div>
        <!-- Tab content -->
    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-info">@lang('photo.save')</button>
        <button class="btn btn-secondary" type="button" data-dismiss="modal">@lang('photo.cancel')</button>
    </div>
</form>
