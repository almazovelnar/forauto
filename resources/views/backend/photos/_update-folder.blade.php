<form action="{{ route('admin.photos.update-folder.submit') }}" id="update-folder-form" class="update-folder-form" method="POST">
    <div class="modal-header">
        <h4 class="modal-title">@lang('photo.update_folder')</h4>

        <button type="button" class="close" data-dismiss="modal">
            <i class="icon md-close"></i>
        </button>
    </div>

    <div class="modal-body">
        <ul class="nav nav-tabs nav-tabs-line">
            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $languageCode)
                <li class="nav-item">
                    <a href="#pm-{{ $languageCode }}"
                       class="nav-link{{ $languageCode == config('app.admin_locale')  ? ' active' : '' }}"
                       data-toggle="tab">{{ mb_strtoupper($languageCode) }}</a>
                </li>
            @endforeach
        </ul>
        <!-- Nav tabs -->
        <div class="tab-content pt-15">
            <input type="hidden" name="folderId" value="{{ $folderId }}">
            @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $languageCode)
                <div class="tab-pane {{ $languageCode == config('app.admin_locale')  ? ' active' : '' }}"
                     role="tabpanel" id="pm-{{ $languageCode }}">
                    <br>
                    <div class="form-group">
                        <input class="form-control" type="text" name="title[{{ $languageCode }}]" placeholder="@lang('photo.title')" value="{{ $folder->getTranslation('title', $languageCode) }}">
                    </div>

                </div>
            @endforeach
            <hr>

        </div>
        <!-- Tab content -->
    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-info">@lang('photo.save')</button>
        <button class="btn btn-secondary" type="button" data-dismiss="modal">@lang('photo.cancel')</button>
    </div>
</form>
