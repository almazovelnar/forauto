@extends('backend.layouts.app')

@section('title', trans('admin.page_title', ['title' => $page->title]))

@section('content')
    <div>
        <form action="{{ route('admin.pages.update', $page->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ $page->getTranslation('title', $lang) }}" name="title[{{ $lang }}]" id="title-{{ $lang }}" class="form-control @error('title.' . $lang) is-invalid @enderror">
                                            @error('title.' . $lang)
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}" class="ckeditor form-control" rows="4">
                                                 {!! $page->getTranslation('description', $lang) !!}
                                            </textarea>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input
                                    value="{{ $page->image ? asset('storage/pages/' . $page->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('info') is-invalid @enderror"
                                    data-delete="{{route('admin.pages.delete-image', $page->id)}}"
                                    data-preview-file-type="text">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="slug">@lang('admin.template')</label>
                                <select name="template" id="template" class="form-control @error('template') is-invalid @enderror" >
                                    <option value="{{ \App\Models\Page::DEFAULT }}" @if(old('template', $page->template) == \App\Models\Page::DEFAULT) selected @endif>@lang('admin.template-default')</option>
                                    <option value="{{ \App\Models\Page::SERVICE }}" @if(old('template', $page->template) == \App\Models\Page::SERVICE) selected @endif>@lang('admin.template-service')</option>
                                </select>
                                @error('template')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="slug">@lang('admin.slug')</label>
                                <input type="text" name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ $page->slug  }}">
                                @error('slug')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
{{--                                <div class="float-left">--}}
{{--                                    <label for="on_menu">@lang('admin.on_menu')</label>--}}
{{--                                    <label class="switch switch-success">--}}
{{--                                        <input type="checkbox" name="on_menu" id="on_menu" @if($page->on_menu == 1) checked @endif>--}}
{{--                                        <span class="slider round"></span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($page->status == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
