@extends('backend.layouts.app')

@section('title', trans('admin.new_tag'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.tags.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card custom-card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">@lang('admin.name')</label>
                                    <input type="text" value="{{ old('name')  }}" name="name" id="name" class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="slug">@lang('admin.slug')</label>
                                    <input type="text" value="{{ old('slug')  }}" name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror">
                                    @error('slug')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="link">@lang('admin.link')</label>
                                    <input type="text" value="{{ old('link')  }}" name="link" id="link" class="form-control @error('link') is-invalid @enderror">
                                    @error('link')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-md-4">
                        <div class="card custom-card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="language">@lang('admin.language')</label>
                                    <select id="language" class="form-control custom-select" name="language">
                                        @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $locale)
                                            <option value="{{ $locale }}" @if(old('language')==$locale) selected @endif>{{ strtoupper($locale) }}</option>
                                        @endforeach
                                    </select>
                                    @error('language')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="float-right">
                                        <label for="chosen">@lang('admin.chosen')</label>
                                        <label class="switch switch-success">
                                            <input type="checkbox" name="chosen" id="chosen"
                                                   @if(old('chosen')=='on') checked @endif>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <input type="submit" name="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
