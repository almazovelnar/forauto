@extends('backend.layouts.app')

@section('title', trans('admin.tags'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'gridHeader' => view('backend.tags._partial')->render(),
                'columnFields' => [
                [
                    'label' => trans('admin.name'),
                    'attribute' => 'name',
                    'sort' => false
                ],
                [
                    'label' => '',
                    'class' => \Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        [
                            'class' => \Itstructure\GridView\Actions\Delete::class,
                            'url' => function ($data) {
                                if ($data->posts->count() == 0) {
                                    return route('admin.tags.destroy', $data->id);
                                }
                            }
                        ]
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
