@extends('backend.layouts.app')

@section('title', trans('admin.translates'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'langSelect' => true,
                'columnFields' => [
                [
                    'label' => trans('admin.translate_group'),
                    'attribute' => 'group'
                ],
                [
                    'label' => trans('admin.translate_key'),
                    'attribute' => 'key'
                ],
                [
                    'label' => trans('admin.translation'),
                    'attribute' => 'text->' . app()->getLocale(),
                    'value' => function ($model) {
                        return $model->getTranslation(request()->get('lang') ?? config('app.admin_locale'), 'text');
                    }
                ],
                [
                    'label' => '',
                    'class' => Itstructure\GridView\Columns\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.translates.destroy', $data->id);
                        }
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
