@extends('frontend.layouts.app')

@section('title', __('Not Found'))

@section('content')

    <!-- 404 not found -->
    <section class="not-found">
        <div class="container-fluid">
            <h1><b>404</b> <br> @lang('site.layout.page_not_found')</h1>
        </div>
    </section>
    <!-- 404 not found -->
@endsection
