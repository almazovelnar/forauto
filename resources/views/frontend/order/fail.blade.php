@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.order_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Msg -->
    <section class="msg">
        <div class="container-fluid">
            <div class="empty">
                <div class="empty__icon empty__icon--error">
                    <svg xmlns="http://www.w3.org/2000/svg" width="4rem" height="4rem">
                        <use xlink:href="#svg-error"></use>
                    </svg>
                </div>

                <h1>@lang('site.order.dear_customer')</h1>
                <p>@lang('site.order.error_message')</p>
            </div>
        </div>
    </section>
    <!-- End Msg -->
@endsection
