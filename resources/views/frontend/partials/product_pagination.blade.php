@php
    /**
     * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
     * @var array[] $elements
     */
@endphp

@if ($paginator->hasPages())
    <ul class="pagination">
        <li>
            <span class="@if($paginator->onFirstPage()) disabled @endif">
                <svg xmlns="http://www.w3.org/2000/svg" width="1.4rem" height="1.4rem">
                    <use xlink:href="#svg-prev"></use>
                </svg>
            </span>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            @if($i <= 5)
                <li>
                    <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endif
        @endfor
        @if ($paginator->hasMorePages())
            <li>
                <a class="next" href="{{ $paginator->nextPageUrl() }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1.4rem" height="1.4rem">
                        <use xlink:href="#svg-next"></use>
                    </svg>
                </a>
            </li>
        @endif
    </ul>
@endif
