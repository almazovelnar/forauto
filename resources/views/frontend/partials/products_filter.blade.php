<label for="filter_toggle" class="filter-bar__blur d-md-none"></label>
<div class="filter">
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">İstehsalçı</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter1">
                <label for="filter1">Agat</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter2">
                <label for="filter2">Akross</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter3">
                <label for="filter3">Alpine</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter4">
                <label for="filter4">Amsoil</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter5">
                <label for="filter5">Ardeca</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter6">
                <label for="filter6">Akross</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter7">
                <label for="filter7">Alpine</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter8">
                <label for="filter8">Amsoil</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter9">
                <label for="filter9">Ardeca</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter10">
                <label for="filter10">Akross</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter11">
                <label for="filter11">Alpine</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter12">
                <label for="filter12">Amsoil</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter13">
                <label for="filter13">Ardeca</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter14">
                <label for="filter14">Akross</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter15">
                <label for="filter15">Alpine</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter16">
                <label for="filter16">Amsoil</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter17">
                <label for="filter17">Ardeca</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter18">
                <label for="filter18">Akross</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter19">
                <label for="filter19">Alpine</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter20">
                <label for="filter20">Amsoil</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter21">
                <label for="filter21">Ardeca</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head">Qiymət aralığı</div>
        <div class="filter__body">
            <div class="d-flex">
                <div class="form-group mb-0">
                    <input type="text" class="form-control" placeholder="məbləğ-dən">
                </div>
                <div class="form-group mb-0">
                    <input type="text" class="form-control" placeholder="məbləğ-ə">
                </div>
            </div>
        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">Yağ növü</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter21">
                <label for="filter21">Hidrokrekinq</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter22">
                <label for="filter22">Mineral</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter23">
                <label for="filter23">Sintetik</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter24">
                <label for="filter24">Yarısintetik</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">Təyinatı</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter31">
                <label for="filter31">Avtobuslar</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter32">
                <label for="filter32">Bağtexnikası</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter33">
                <label for="filter33">Gəmilər</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter34">
                <label for="filter34">Kənd təsərrüfatı maşınları</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter35">
                <label for="filter35">Mikroavtobuslar</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">Mühərrik tipi</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter41">
                <label for="filter41">Benzin</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter42">
                <label for="filter42">Bütün tiplər</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter43">
                <label for="filter43">Dizel</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter44">
                <label for="filter44">Qaz</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter45">
                <label for="filter45">Turbodizel</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">Mühərrikin dövriyyəsi</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter51">
                <label for="filter51">Dördtaktlı</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter52">
                <label for="filter52">İkitaktlı</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">Yağın qatılığı</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter61">
                <label for="filter61">0W-20</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter62">
                <label for="filter62">0W-30</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter63">
                <label for="filter63">10W-40</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter64">
                <label for="filter64">10W-60</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter65">
                <label for="filter65">5W-30</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">API standartı</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter71">
                <label for="filter71">A5</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter72">
                <label for="filter72">B5</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter73">
                <label for="filter73">C2</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter74">
                <label for="filter74">C5</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter75">
                <label for="filter75">CC</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">ILSAC standartı</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter81">
                <label for="filter81">A3</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter82">
                <label for="filter82">B3</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter83">
                <label for="filter83">B4</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter84">
                <label for="filter84">C3</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter85">
                <label for="filter85">GF-3</label>
            </div>

        </div>
    </div>
    <div class="filter__item">
        <div class="filter__head filter__head--with-sub">İstehsalçı ölkə</div>
        <div class="filter__body">

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter91">
                <label for="filter91">ABŞ</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter92">
                <label for="filter92">Almaniya</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter93">
                <label for="filter93">Azərbaycan</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter94">
                <label for="filter94">Belarusiya</label>
            </div>

            <div class="form-group form-group--checkbox">
                <input type="checkbox" class="d-none" id="filter95">
                <label for="filter95">Belçika</label>
            </div>

        </div>
    </div>
</div>
