@php
    /**
     * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
     * @var array[] $elements
     */
@endphp

@if ($paginator->hasPages())
    <div class="blog_pagination">
        <div class="pagination">
            <ul>
                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    <li class="{{ ($paginator->currentPage() == $i) ? ' current' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor
                @if ($paginator->hasMorePages())
                    <li><a class="next" href="{{ $paginator->nextPageUrl() }}">></a></li>
                @endif
            </ul>
        </div>
    </div>
@endif

