@if(!$authors->isEmpty())
<div class="authors-list">
    @foreach($authors as $author)
    <div class="author col-lg-3">
        <span><img src="{{asset('storage/users/'.$author->thumb)}}" alt="{{$author->fullname}}"></span>
       <a href="{{route('authors.posts',$author->id)}}"> <p>{{$author->fullname}}</p></a>
    </div>
    @endforeach
</div>
@endif
