<div class="empty">
    <div class="empty__icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="4.0rem" height="2.7rem">
            <use xlink:href="#svg-empty"></use>
        </svg>
    </div>
    <h1>@lang('site.empty.dear_customer')</h1>
    <p>@lang('site.empty.products')</p>
    <a href="{{ route('home') }}" class="btn btn-orange">@lang('site.empty.order_products')</a>
</div>
