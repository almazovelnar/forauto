<figure>
    <img src="{{get_image($photo->filename, $photo->path, 'large')}}" alt="{{$photo->title}}">
</figure>
