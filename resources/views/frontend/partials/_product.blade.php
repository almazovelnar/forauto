<div class="products__item" data-id="{{ $product->id }}">
    @if($product->hot && $product->sale_price)
        <p class="products__label products__label--sale">@lang('site.product.discount', ['percent' => get_discount($product->price, $product->sale_price)])</p>
    @elseif($product->is_new)
        <p class="products__label">@lang('site.product.is_new')</p>
    @endif
    <a href="javascript:void(0);"
       class="products__wishlist addToWishlist @if(in_array($product->id, wishlist_ids())) active @endif"
       aria-label="wishlist" title="@lang('site.product.add_to_wishlist')">
        <svg xmlns="http://www.w3.org/2000/svg" width="1.8rem" height="1.6rem">
            <use xlink:href="#svg-heart"></use>
        </svg>
    </a>
    <a href="{{ route('product', $product->slug) }}" class="products__image"><img loading="lazy" src="{{ product_image($product->image) }}"
                                                                                     alt="{{ $product->title }}"></a>
    <div class="products__text">
        <a href="{{ route('product', $product->slug) }}" class="products__title">{{ $product->title }}</a>
        <span class="products__category">{{ $product->category->title }}</span>
        @if($product->stars)
            <ul class="products__star">
                @foreach(range(1,5) as $value)
                    <li class="@if($product->stars >= $value) active @endif">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1.6rem" height="1.6rem">
                            <use xlink:href="#svg-star"></use>
                        </svg>
                    </li>
                @endforeach
            </ul>
        @endif
        <div class="products__price">
            @if($product->sale_price)
                <span
                    class="products__price__new">{{ get_price($product->sale_price) }}</span>
                <span
                    class="products__price__old">{{ get_price($product->price) }}</span>
            @else
                <span
                    class="products__price__new">{{ get_price($product->price) }}</span>
            @endif
        </div>

        @if(!in_array($product->id, cart_ids()))
            <a href="javascript:void(0);" class="btn w-100 addToCart"
               title="@lang('site.product.add_to_cart')">
                <svg xmlns="http://www.w3.org/2000/svg" width="2.2rem" height="2rem">
                    <use xlink:href="#svg-basket"></use>
                </svg>
                <span>@lang('site.product.add_to_cart')</span>
            </a>
        @else
            <a href="javascript:void(0);" class="btn w-100 removeFromCart btn-orange"
               title="@lang('site.product.remove_from_cart')">
                <svg xmlns="http://www.w3.org/2000/svg" width="2.2rem" height="2rem">
                    <use xlink:href="#svg-basket"></use>
                </svg>
                <span>@lang('site.product.remove_from_cart')</span>
            </a>
        @endif
    </div>
</div>
