@if(!$posts->isEmpty())
    @foreach($posts as $post)
        <div class="news-col col-lg-4">
            <a href="{{route('posts.view',$post->slug)}}" data-pub="{{ $post->published_at }}" class="post-block">
                <div class="col-inner">
                                <span>
                                     <img src="{{get_image($post->image,$post->path,'list')}}" alt="{{$post->title}}">
                                </span>
                    <div class="short-info">
                        <span>{{getParsedDate($post->published_at)}}</span>
                        <p>
                            {{$post->title}}
                        </p>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
@endif
