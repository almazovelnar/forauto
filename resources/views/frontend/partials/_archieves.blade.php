@if(!$archieves->isEmpty())
    @foreach($archieves as $archieve)
        <div class="archieve-box col-lg-4 post-block" data-pub="{{ $archieve->published_at }}">
            <a href="{{route('archieve.getFile',$archieve->id)}}" class="bg-shade-grey">
                            <span>
                                <img src="{{asset('storage/archieves/'.$archieve->image)}}" alt="picture">
                            </span>
            </a>
            <p>{{$archieve->title}}</p>
        </div>
    @endforeach
@endif
