<div class="profile__side-bar">
    <ul>
        <li>
            <a href="{{ route('profile') }}" class="@if(Route::is('profile')) active @endif">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.1rem" height="1.8rem">
                                        <use xlink:href="#svg-home"></use>
                                    </svg>
                                </span>
                @lang('site.layout.details_page')
            </a>
        </li>
        <li>
            <a href="{{ route('history') }}" class="@if(Route::is('history')) active @endif">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="1.6rem">
                                        <use xlink:href="#svg-order"></use>
                                    </svg>
                                </span>
                @lang('site.layout.orders_page')
            </a>
        </li>
        <li>
            <a href="{{ route('profile.change-password') }}" class="@if(Route::is('profile.change-password')) active @endif">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="1.5rem" height="1.8rem">
                                        <use xlink:href="#svg-password"></use>
                                    </svg>
                                </span>
                @lang('site.layout.change_password_page')
            </a>
        </li>
        <li>
            <a href="javascript:void(0);" class="profileLogout">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="1.8rem" height="1.6rem">
                                        <use xlink:href="#svg-exit"></use>
                                    </svg>
                                </span>
                @lang('site.profile.logout')
            </a>
        </li>
    </ul>
</div>


@section('addJs')
    <script>
        $('.profileLogout').on('click', function (e) {
            e.preventDefault();
            if (confirm("{{ trans('site.profile.confirm_logout') }}")) {
                window.location.href = "{{ route('logout') }}";
            }
        })
    </script>
@append
