@foreach($products as $product)
    <div class="col-md-4">
        @include('frontend.partials._product', ['product' => $product])
    </div>
@endforeach

<div class="col-12">
    {{ $products->links('frontend.partials.product_pagination') }}
</div>
