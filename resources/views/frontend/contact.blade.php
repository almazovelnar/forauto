@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{ route('home') }}">@lang('site.layout.home')</a></li>
            <li>@lang('site.layout.contact_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Contact -->
    <section class="contact">
        <div class="container-fluid">
            <h1 class="page-title page-title--lg mb-4">@lang('site.layout.contact_us')</h1>
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <form action="{{ route('contact.send_message') }}" method="POST" class="contact__form">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">@lang('site.contact.name')</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="@lang('site.contact.name_placeholder')" value="{{ old('name') }}">
                                    @error('name')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="surname">@lang('site.contact.surname')</label>
                                    <input type="text" class="form-control" name="surname" id="surname" placeholder="@lang('site.contact.surname_placeholder')" value="{{ old('surname') }}">
                                    @error('surname')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="phone">@lang('site.contact.phone')</label>
                                    <input type="text" class="form-control phone-input" name="phone" id="phone"
                                           placeholder="@lang('site.contact.phone_placeholder')" value="{{ old('phone') }}">
                                    @error('phone')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="email">@lang('site.contact.email')</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="@lang('site.contact.email_placeholder')" value="{{ old('email') }}">
                                    @error('email')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="message">@lang('site.contact.message')</label>
                                    <textarea class="form-control" rows="6" name="message" id="message" placeholder="@lang('site.contact.message_placeholder')">{{ old('message') }}</textarea>

                                    @error('message')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn w-100" type="submit">@lang('site.contact.submit')</button>
                            </div>
                        </div>
                    </form>
                </div>
                @if(site_info('location'))
                    <div class="col-lg-7 col-md-6">
                        <div class="contact__map">
                            <iframe width="100%" height="100%"
                                    src="https://maps.google.com/maps?q={{site_info('location')}}&hl=en&z=14&output=embed"></iframe>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row contact__row">
                <div class="col-md-4">
                    <div class="contact__item">
                        <span class="contact__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="3.8rem" height="3.8rem">
                                <use xlink:href="#svg-mail"></use>
                            </svg>
                        </span>
                        <h2>@lang('site.contact.title_1')😇</h2>
                        <p>@lang('site.contact.text_1')</p>
                        @foreach(site_info('email') as $email)
                            <a href="mailto:{{$email}}">{{ $email }}</a>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact__item">
                        <span class="contact__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="3.8rem" height="3.8rem">
                                <use xlink:href="#svg-store"></use>
                            </svg>
                        </span>
                        <h2>@lang('site.contact.title_2')🤩</h2>
                        <p>@lang('site.contact.text_2')</p>
                        <p class="text-black">{{ site_info('address') }}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact__item">
                        <span class="contact__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="3.8rem" height="3.8rem">
                                <use xlink:href="#svg-phone"></use>
                            </svg>
                        </span>
                        <h2>@lang('site.contact.title_3')🤗</h2>
                        <p>@lang('site.contact.text_3')</p>
                        @foreach(site_info('phone') as $phone)
                            <a href="tel:{{$phone}}">{{ $phone }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact -->
@endsection
