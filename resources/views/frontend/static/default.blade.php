@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{ route('home') }}">@lang('site.layout.home')</a></li>
            <li>{{ $page->title }}</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- About -->
    <section class="about">
        <div class="container-fluid">
            <h1 class="page-title page-title--lg mb-5">{{ $page->title }}</h1>
            <div class="content">
                @if($page->image)
                    <img src="{{ asset('storage/pages/' . $page->image) }}" alt="{{ $page->title }}">
                @endif
                {!! $page->description !!}
            </div>
            @if($page->slug == 'about-us')
                <div class="about__banner" style="background-image: url('/assets/front/images/about_bg.jpg')">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="about__item">
                                <h2>@lang('site.about.count_1')</h2>
                                <h3>@lang('site.about.title_1')</h3>
                                <p>@lang('site.about.text_1')</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="about__item">
                                <h2>@lang('site.about.count_2')</h2>
                                <h3>@lang('site.about.title_2')</h3>
                                <p>@lang('site.about.text_2')</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="about__item">
                                <h2>@lang('site.about.count_3')</h2>
                                <h3>@lang('site.about.title_3')</h3>
                                <p>@lang('site.about.text_3')</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <!-- End About -->

    <!-- customer login end -->
    @widget('brand_slider')
@endsection
