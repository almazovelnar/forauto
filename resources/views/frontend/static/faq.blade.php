@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.faq_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Faq -->
    <section class="faq">
        <div class="container-fluid">
            <h1 class="page-title page-title--lg">@lang('site.layout.faq')</h1>
            <div class="faq__container">

                @if($questions->isNotEmpty())
                    @foreach($questions as $key => $faq)
                        <div class="faq__item">
                            <div class="faq__head">{{ $faq->question }}</div>
                            <div class="faq__desc">
                                <p>{{ $faq->answer }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
    <!-- End Faq -->
@endsection
