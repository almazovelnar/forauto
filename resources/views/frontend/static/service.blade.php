@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{ route('home') }}">@lang('site.layout.home')</a></li>
            <li>{{ $page->title }}</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <section class="content-page">
        <div class="container-fluid">
            <h1 class="page-title page-title--lg mb-5">{{ $page->title }}</h1>
            <div class="content-page__row">
                <ul class="content-page__side-bar">
                    @foreach($otherPages as $otherPage)
                        <li><a href="{{ route('page', $otherPage->slug) }}" class="@if(request('slug') == $otherPage->slug) active @endif">{{ $otherPage->title }}</a></li>
                    @endforeach
                </ul>
                <div class="content">
                    {!! $page->description !!}
                </div>
            </div>
        </div>
    </section>
@endsection


