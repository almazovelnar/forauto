@php
    $lang = app()->getLocale();
@endphp
<div class="notification @if(session()->has('notification')) active @endif">
    <a href="javascript:void(0);" class="notification__close">
        <svg xmlns="http://www.w3.org/2000/svg" width="1.8rem" height="1.6rem">
            <use xlink:href="#svg-close"></use>
        </svg>
    </a>
    <p>{{ session('notification') }}</p>
</div>
<header class="header">
    <input type="checkbox" id="burger_toggle" class="d-none">

    <div class="header__top">
        <div class="container-fluid">
            <form class="header__search d-md-none" action="{{ route('search') }}" method="GET">
                <div class="form-group">
                    <input value="{{ request()->get('query') }}" name="query" placeholder="@lang('site.layout.search_product')"  type="text" class="form-control">
                    <button type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                            <use xlink:href="#svg-search"></use>
                        </svg>
                    </button>
                </div>
            </form>
            <ul class="header__menu d-md-none">
                @widget('menu-header')
            </ul>
            <ul class="header__info">
                <li>
                    <a href="mailto:{{ site_info('email')[0] }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                            <use xlink:href="#svg-mail"></use>
                        </svg>
                        {{ site_info('email')[0] }}
                    </a>
                </li>
                <li>
                    <a href="tel:{{ site_info('phone')[0] }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                            <use xlink:href="#svg-phone"></use>
                        </svg>
                        {{ site_info('phone')[0] }}
                    </a>
                </li>
            </ul>
            <a href="{{ route('home') }}" class="header__logo d-none d-md-block" aria-label="logo">
                <img src="/assets/front/images/logo.png" width="87" height="33" alt="">
            </a>

            <ul class="header__tools">
                <li class="header__tools__item">
                    @widget('lang-switcher')
                </li>
                <li class="header__tools__item">
                    @if(auth()->check())
                        <a href="{{ route('profile') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                <use xlink:href="#svg-user"></use>
                            </svg>@lang('site.layout.profile')
                        </a>
                    @else
                        <a href="{{ route('login') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                <use xlink:href="#svg-user"></use>
                            </svg>
                            @lang('site.layout.login')
                        </a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
    <div class="header__bottom">
        <div class="container-fluid">
            <a href="{{ route('home') }}" class="header__logo d-md-none" aria-label="logo">
                <img src="/assets/front/images/logo.png" width="87" height="33" alt="{{ site_info('title') }}">
            </a>
            <ul class="header__menu d-none d-md-flex">
                @widget('menu-header')
            </ul>
            <form action="{{ route('search') }}" method="GET" class="header__search d-none d-md-flex">
                <div class="form-group">
                    <input value="{{ request()->get('query') }}" name="query" placeholder="@lang('site.layout.search_product')" type="text" class="form-control" >
                    <button type="submit">
                        <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                            <use xlink:href="#svg-search"></use>
                        </svg>
                    </button>
                </div>
            </form>
            <ul class="header__bottom__tools">
                <li>
                    <a href="{{ route('cart') }}">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="2.2rem" height="2rem">
                                <use xlink:href="#svg-basket"></use>
                            </svg>
                        </span>
                        @lang('site.layout.cart')
                    </a>
                    @widget('cart-widget')
                </li>
                <li class="header_wishlist">
                    <a href="{{ route('favorites') }}">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="2rem" height="2rem">
                                <use xlink:href="#svg-heart"></use>
                            </svg>
                            <i class="wishlist_count @if(!($count = wishlist()->count())) d-none @endif">{{ $count }}</i>
                        </span>
                        @lang('site.layout.wishlist')
                    </a>
                </li>
            </ul>
            <label for="burger_toggle" class="header__burger d-md-none"><i></i></label>
        </div>
    </div>
</header>
