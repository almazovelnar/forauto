{{--<script src="/vendor/sweetalert2/sweetalert2.all.min.js"></script>--}}

<script src="/assets/front/js/main.js"></script>
<script src="/assets/front/js/filter.js"></script>
<script src="/assets/front/js/wishlist.js"
        data-wishlist-add-url="{{ route('favorite.add') }}"
        data-wishlist-remove-url="{{ route('favorite.remove') }}"
></script>
<script src="/assets/front/js/cart.js"
        data-cart-add-url="{{ route('cart.add') }}"
        data-cart-update-url="{{ route('cart.update') }}"
        data-cart-remove-url="{{ route('cart.remove') }}"
></script>

@yield('addJs')
