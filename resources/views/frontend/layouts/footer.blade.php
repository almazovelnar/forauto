<!-- footer -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="footer__desc">
                    <div class="footer__logo"><img src="/assets/front/images/logo-white.png" width="131" height="51"
                                                   alt="{{ site_info('title') }}">
                    </div>
                    <p>@lang('site.layout.footer_text')</p>
                    <ul class="footer__socials">
                        @if(site_config('FACEBOOK'))
                            <li>
                                <a href="{{ site_config('FACEBOOK') }}" arial-label="facebook" target="_blank">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                        <use xlink:href="#svg-facebook"></use>
                                    </svg>
                                </a>
                            </li>
                        @endif
                        @if(site_config('INSTAGRAM'))
                            <li>
                                <a href="{{ site_config('INSTAGRAM') }}" arial-label="instagram" target="_blank">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                        <use xlink:href="#svg-instagram"></use>
                                    </svg>
                                </a>
                            </li>
                        @endif
                        @if(site_config('TWITTER'))
                            <li>
                                <a href="{{ site_config('TWITTER') }}" arial-label="twitter" target="_blank">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                        <use xlink:href="#svg-twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        @endif
                        @if(site_config('TIKTOK'))
                            <li>
                                <a href="{{ site_config('TIKTOK') }}" aria-label="tiktok" target="_blank">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                        <use xlink:href="#svg-tiktok"></use>
                                    </svg>
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            @widget('menu-footer')
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container-fluid">
            <p>Forauto © 2023. @lang('site.layout.all_rights_reserved')</p>
        </div>
    </div>
</footer>

<a href="https://wa.me/{{ site_config('WHATSAPP') }}" target="_blank" class="wp-fixed" aria-label="whatsapp">
    <svg xmlns="http://www.w3.org/2000/svg" width="4rem" height="4rem">
        <use xlink:href="#svg-whatsapp"></use>
    </svg>
</a>
<!-- end footer -->

