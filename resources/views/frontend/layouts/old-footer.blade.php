<!--footer area start-->
<footer class="footer_widgets color_three">
    <div class="shipping_area">
        <div class="container">
            <div class="shipping_inner">
                <div class="single_shipping">
                    <div class="shipping_icone">
                        <img src="/assets/front/img/about/shipping1.png" alt="icon">
                    </div>
                    <div class="shipping_content">
                        <h4>@lang('site.layout.shipping_1')</h4>
                        <p>@lang('site.layout.shipping_text_1')</p>
                    </div>
                </div>
                <div class="single_shipping">
                    <div class="shipping_icone">
                        <img src="/assets/front/img/about/shipping2.png" alt="icon">
                    </div>
                    <div class="shipping_content">
                        <h4>@lang('site.layout.shipping_2')</h4>
                        <p>@lang('site.layout.shipping_text_2')</p>
                    </div>
                </div>
                <div class="single_shipping">
                    <div class="shipping_icone">
                        <img src="/assets/front/img/about/shipping3.png" alt="icon">
                    </div>
                    <div class="shipping_content">
                        <h4>@lang('site.layout.shipping_3')</h4>
                        <p>@lang('site.layout.shipping_text_3')</p>
                    </div>
                </div>
                <div class="single_shipping">
                    <div class="shipping_icone">
                        <img src="/assets/front/img/about/shipping4.png" alt="icon">
                    </div>
                    <div class="shipping_content">
                        <h4>@lang('site.layout.shipping_4')</h4>
                        <p>@lang('site.layout.shipping_text_4')</p>
                    </div>
                </div>
                <div class="single_shipping">
                    <div class="shipping_icone">
                        <img src="/assets/front/img/about/shipping5.png" alt="icon">
                    </div>
                    <div class="shipping_content">
                        <h4>@lang('site.layout.shipping_5')</h4>
                        <p>@lang('site.layout.shipping_text_5')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="newsletter_container widgets_container ">
                        <h3>@lang('site.layout.follow_us')</h3>
                        <p style="color: #fff;">@lang('site.layout.follow_us_text')</p>
                        <div class="footer_social">
                            <ul>
                                @if(site_config('FACEBOOK'))
                                    <li><a class="facebook" href="{{ site_config('FACEBOOK') }}" target="_blank"><i class="icon-facebook"></i></a></li>
                                @endif
                                @if(site_config('WHATSAPP'))
                                    <li><a class="whatsapp" href="{{ site_config('WHATSAPP') }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                                @endif
                                @if(site_config('INSTAGRAM'))
                                    <li><a class="instagram2" href="{{ site_config('INSTAGRAM') }}" target="_blank"><i class="icon-instagram2"></i></a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="footer_col_container">
                        <div class="widgets_container widget_menu">
                            <h3>@lang('site.layout.info')</h3>
                            <div class="footer_menu">
                                @widget('menu-footer')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widgets_container">
                        <h3>@lang('site.layout.contact_info')</h3>
                        <div class="footer_contact">
                            <div class="footer_contact_inner">
                                <div class="contact_icone">
                                    <img src="/assets/front/img/icon/icon-phone2.png" alt="icon">
                                </div>

                                <div class="contact_text">
                                    <p>
                                    @foreach(site_info('phone') as $phone)
                                        <strong><a href="tel:{{ $phone }}">{{ $phone }}</a> </strong><br>
                                    @endforeach
                                    @foreach(site_info('email') as $email)
                                        <strong><a href="mailto:{{ $email }}">{{ $email }}</a> </strong>
                                    @endforeach
                                    </p>
                                </div>
                            </div>
                            <p>{{ site_info('address') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="copyright_area">
                        <p>&copy; 2022 <a href="https://forauto.az" class="text-uppercase">FORAUTO.AZ</a>. Created by <a target="_blank" href="https://almazovs.com">almazov`s</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
<!--footer area end-->
