<!DOCTYPE html>
<html lang="az">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>For Auto</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="/assets/front/css/font/stylesheet.css">
    <link rel="stylesheet" href="/assets/front/css/style.css?v=696">
    <script src="/assets/front/js/svg-symbols.js?v=696"></script>
</head>
<body>

<div class="svg-placeholder" style="border: 0; clip: rect(0 0 0 0); height: 1px;
        margin: -1px; overflow: hidden; padding: 0;
        position: absolute; width: 1px;"></div>
<script>
    document
        .querySelector('.svg-placeholder')
        .innerHTML = SVG_SPRITE;
</script>

<header class="header d-md-none">
    <a href="{{ route('home') }}" class="header__logo" aria-label="logo">
        <img src="/assets/front/images/logo.png" width="87" height="33" alt="{{ site_info('title') }}">
    </a>
</header>

@yield('content')


<script src="/assets/front/js/main.js?v=696"></script>
</body>
</html>
