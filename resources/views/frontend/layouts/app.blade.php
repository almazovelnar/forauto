<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(isset($title))
        <title>{{ $title }}</title>
    @else
        <title>@yield('title')</title>
    @endif

    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::tag('og:title') !!}
    {!! MetaTag::tag('og:description') !!}
    {!! MetaTag::tag('og:image') !!}

    <link rel="icon" type="image/x-icon" href="/images/favicon.svg">
    <script src="/assets/front/js/svg-symbols.js"></script>
    @include('frontend.layouts.styles')
    <script>
        var translations = {
            itemAddedToCart: "@lang('site.layout.itemAddedToCart')",
            itemAddedToWishlist: "@lang('site.layout.itemAddedToWishlist')",
            itemRemovedFromCart: "@lang('site.layout.itemRemovedFromCart')",
            itemRemovedFromWishlist: "@lang('site.layout.itemRemovedFromWishlist')",
        };
    </script>
</head>
<body>

<div class="svg-placeholder" style="border: 0; clip: rect(0 0 0 0); height: 1px;
        margin: -1px; overflow: hidden; padding: 0;
        position: absolute; width: 1px;"></div>
<script>
    document
        .querySelector('.svg-placeholder')
        .innerHTML = SVG_SPRITE;
</script>

    <div id="all_parts">
    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')

    @include('frontend.layouts.scripts')
    </div>
</body>
</html>
