@php
    $lang = app()->getLocale();
@endphp
<!--offcanvas menu area start-->
<div class="off_canvars_overlay"></div>

<div class="offcanvas_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="canvas_open">
                    <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                </div>
                <div class="offcanvas_menu_wrapper">
                    <div class="canvas_close">
                        <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
                    </div>
                    <div class="call_support">
                        <p><i class="icon-phone-call" aria-hidden="true"></i> <span>@lang('site.layout.tel') <a href="tel:{{ site_info('phone')[0] }}">{{ site_info('phone')[0] }}</a></span></p>

                    </div>
                    <div class="header_account">
                        <ul>
                            <li class="language"><a href="javascript:void(0);"> {{app()->getLocale()}} <i class="ion-chevron-down"></i></a>
                                <ul class="dropdown_language">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $locale)
                                        @if($locale != $lang)
                                            <li><a href="{{ LaravelLocalization::localizeURL(url()->current(), $locale) }}">{{ strtoupper($locale) }}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header_top_links">
                        <ul>
                            @if(auth()->check())
                                <li><a href="{{ route('profile') }}">@lang('site.layout.profile')</a></li>
                            @else
                                <li><a href="{{ route('register') }}">@lang('site.layout.register')</a></li>
                                <li><a href="{{ route('login') }}">@lang('site.layout.login')</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="search_container">
                        <form action="{{ route('products') }}" method="GET">
                            <div class="search_box">
                                <input value="{{ request()->get('search') }}" name="search" placeholder="@lang('site.layout.search_product')" type="text">
                                <button type="submit">@lang('site.layout.search')</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu" class="text-left ">
                        @widget('menu-mobile-header')
                    </div>
                    <div class="offcanvas_footer">
                        <ul>
                            @if(site_config('FACEBOOK'))
                                <li class="facebook"><a href="{{ site_config('FACEBOOK') }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            @endif
                            @if(site_config('WHATSAPP'))
                                <li class="whatsapp"><a href="https://wa.me/{{ site_config('WHATSAPP') }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            @endif
                            @if(site_config('INSTAGRAM'))
                                <li class="instagram"><a href="{{ site_config('INSTAGRAM') }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--offcanvas menu area end-->


<header>
    <div class="main_header header_two">
        <!--header top start-->
        <div class="header_top">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-5">
                        <div class="header_account">
                            <ul>
                                <li class="language"><a href="javascript:void(0);">{{strtoupper($lang)}} <i class="ion-chevron-down"></i></a>
                                    <ul class="dropdown_language">
                                        @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $locale)
                                            @if($locale != $lang)
                                                <li><a href="{{ LaravelLocalization::localizeURL(url()->current(), $locale) }}">{{ strtoupper($locale) }}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="header_top_links text-right">
                            <ul>
                                @if(auth()->check())
                                    <li><a href="{{ route('profile') }}">@lang('site.layout.profile')</a></li>
                                @else
                                    <li><a href="{{ route('register') }}">@lang('site.layout.register')</a></li>
                                    <li><a href="{{ route('login') }}">@lang('site.layout.login')</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header top start-->

        <!--header middel start-->
        <div class="header_middle h_middle_two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-md-4 col-sm-4 col-4">
                        <div class="logo">
                            <a href="{{ route('home') }}"><img src="{{ asset('assets/front/img/logo/logoW.png') }}" alt="{{ site_info('title') }}"></a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-6 col-sm-6 col-6">
                        <div class="header_right_box">

                            <div class="search_container">
                                <form action="{{ route('products') }}" method="GET">
                                    <div class="search_box">
                                        <input value="{{ request()->get('search') }}" name="search" placeholder="@lang('site.layout.search_product')" type="text">
                                        <button type="submit">@lang('site.layout.search')</button>
                                    </div>
                                </form>
                            </div>

                            <div class="header_configure_area">
                                <div class="header_wishlist">
                                    <a href="{{ route('favorites') }}"><i class="icon-heart"></i>
                                            <span class="wishlist_count @if(!($count = wishlist()->count())) d-none @endif" >{{ $count }}</span>
                                    </a>
                                </div>
                                <div class="mini_cart_wrapper header_cart">
                                    <a href="{{ route('cart') }}">
                                        <i class="icon-shopping-bag2"></i>
                                        <span class="cart_price">{{ get_price(cart()->priceTotal()) }} AZN</span>
                                        <span class="cart_count @if(!($count = cart()->count())) d-none @endif">{{ $count }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header middel end-->

        <!--header bottom satrt-->
        <div class="header_bottom h_bottom_two sticky-header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="main_menu menu_two menu_position text-left">
                            @widget('menu-header')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header bottom end-->
    </div>
</header>
<!--header area end-->
