@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{ route('home') }}">@lang('site.layout.home')</a></li>
            <li>@lang('site.layout.change_password_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Orders -->
    <section class="profile">
        <div class="container-fluid">
            <div class="profile__row">
                @include('frontend.partials.profile_sidebar')
                <div class="profile__body">
                    <div class="profile__info">
                        <div class="profile__head">
                            <h1 class="profile__title">@lang('site.layout.change_password_page')</h1>
                        </div>
                        <form action="{{ route('profile.change-password.submit') }}" class="profile__form" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('site.profile.password')<span class="text-red">*</span></label>
                                        <input type="password" name="password" class="form-control" placeholder="@lang('site.profile.password_placeholder')">

                                        @error('password')
                                        <span class="error-text">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@lang('site.profile.password_confirm')<span class="text-red">*</span></label>
                                        <input type="password" name="passwordConfirm" class="form-control"
                                               placeholder="@lang('site.profile.password_confirm_placeholder')">

                                        @error('passwordConfirm')
                                        <span class="error-text">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn">@lang('site.profile.save_button')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Orders -->

@endsection
