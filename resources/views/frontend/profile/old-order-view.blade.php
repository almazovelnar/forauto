@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li><a href="{{route('history')}}">@lang('site.layout.orders_page')</a></li>
                            <li>#{{$order->order_id}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- my account start  -->
    <div class="account_page_bg">
        <div class="container">
            <section class="main_content_area">
                <div class="account_dashboard">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3">
                            <!-- Nav tabs -->
                            <div class="dashboard_tab_button">
                                <ul role="tablist" class="nav flex-column dashboard-list">
                                    <li><a href="{{ route('profile') }}" class="nav-link">@lang('site.profile.account_details')</a></li>
                                    <li><a href="{{ route('history') }}" class="nav-link active">@lang('site.profile.orders')</a></li>
                                    <li><a href="javascript:void(0);" data-url="{{ route('logout') }}" class="profileLogout nav-link">@lang('site.profile.logout')</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-9 col-lg-9">
                            <!-- Tab panes -->
                            <div class="tab-content dashboard_content">
                                <div class="tab-pane fade show active" id="orders">
                                    <h3>@lang('site.layout.order')  #{{$order->order_id}} ({{ getParsedDate($order->created_at) }})</h3>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>@lang('site.profile.product_code')</th>
                                                <th>@lang('site.profile.product_name')</th>
                                                <th>@lang('site.profile.price')</th>
                                                <th>@lang('site.profile.count')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($items as $key => $item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td><a href="{{route('product', $item->product->slug)}}">{{ $item->code }}</a></td>
                                                    <td>{{ $item->product->title }}</td>
                                                    <td>{{ get_price($item->price) }} AZN</td>
                                                    <td>{{ $item->count }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- my account end   -->

    @widget('brand_slider')
@endsection

@section('addJs')
    <script>
        $('.profileLogout').on('click', function (e) {
            e.preventDefault();
            if (confirm("{{ trans('site.profile.confirm_logout') }}")) {
                window.location.href = "{{ route('logout') }}";
            }
        })
    </script>
@endsection
