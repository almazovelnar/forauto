@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li>@lang('site.profile.page_title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- my account start  -->
    <div class="account_page_bg">
        <div class="container">
            <section class="main_content_area">
                <div class="account_dashboard">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3">
                            <!-- Nav tabs -->
                            <div class="dashboard_tab_button">
                                <ul role="tablist" class="nav flex-column dashboard-list">
                                    <li><a href="{{ route('profile') }}" class="nav-link">@lang('site.profile.account_details')</a></li>
                                    <li><a href="{{ route('history') }}" class="nav-link active">@lang('site.profile.orders')</a></li>
                                    <li><a href="javascript:void(0);" data-url="{{ route('logout') }}" class="profileLogout nav-link">@lang('site.profile.logout')</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-9 col-lg-9">
                            <!-- Tab panes -->
                            <div class="tab-content dashboard_content">
                                <div class="tab-pane fade show active" id="orders">
                                    <h3>@lang('site.profile.orders')</h3>
                                    @if(!$orders->isEmpty())
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>@lang('site.profile.order_id')</th>
                                                    <th>@lang('site.profile.date')</th>
                                                    <th>@lang('site.profile.status')</th>
                                                    <th>@lang('site.profile.total_price')</th>
                                                    <th>@lang('site.profile.total_count')</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($orders as $key => $order)
                                                    <tr>
                                                        <td>{{ $order->order_id }}</td>
                                                        <td>{{ getParsedDate($order->created_at) }}</td>
                                                        <td><span class="success">{{ \App\Enum\OrderStatus::get($order->status) }}</span></td>
                                                        <td>{{ get_price($order->total_price) }} AZN</td>
                                                        <td>{{ $order->total_count }}</td>
                                                        <td><a href="{{ route('history.view', $order->order_id) }}" class="view">@lang('site.profile.view')</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <h4>@lang('site.profile.orders_is_empty')</h4>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- my account end   -->

    @widget('brand_slider')
@endsection

@section('addJs')
    <script>
        $('.profileLogout').on('click', function (e) {
            e.preventDefault();
            if (confirm("{{ trans('site.profile.confirm_logout') }}")) {
                window.location.href = "{{ route('logout') }}";
            }
        })
    </script>
@endsection
