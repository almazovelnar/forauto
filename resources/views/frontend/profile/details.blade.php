@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->

    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{ route('home') }}">@lang('site.layout.home')</a></li>
            <li>@lang('site.layout.details_page')</li>
        </ul>
    </div>

    <!-- End Breadcrumbs -->

    <!-- Orders -->
    <section class="profile">
        <div class="container-fluid">
            <div class="profile__row">
                @include('frontend.partials.profile_sidebar')
                <div class="profile__body">
                    <div class="profile__info">
                        <div class="profile__head">
                            <h1 class="profile__title">@lang('site.profile.personal_information')</h1>
                            {{--                            <a href="#">--}}
                            {{--                                <svg xmlns="http://www.w3.org/2000/svg" width="1.7rem" height="1.7rem">--}}
                            {{--                                    <use xlink:href="#svg-edit"></use>--}}
                            {{--                                </svg>--}}
                            {{--                                Düzəliş et--}}
                            {{--                            </a>--}}
                        </div>
                        <form action="{{ route('profile.edit') }}" class="profile__form row" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('site.profile.name')</label>
                                    <input type="text" class="form-control" name="name"
                                           value="{{ old('name', $customer->name) }}"
                                           placeholder="@lang('site.profile.name_placeholder')">

                                    @error('name')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('site.profile.surname')</label>
                                    <input type="text" class="form-control" name="surname"
                                           value="{{ old('surname', $customer->surname) }}"
                                           placeholder="@lang('site.profile.surname_placeholder')">


                                    @error('surname')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('site.profile.email')</label>
                                    <input type="text" class="form-control" name="email"
                                           value="{{ old('email', $customer->email) }}"
                                           placeholder="@lang('site.profile.email_placeholder')">

                                    @error('email')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('site.profile.phone')</label>
                                    <input type="text" class="form-control phone-input" name="phone"
                                           value="{{ old('phone', $customer->phone) }}"
                                           placeholder="@lang('site.profile.phone_placeholder')">

                                    @error('phone')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn" type="submit">@lang('site.profile.save_button')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Orders -->

@endsection
