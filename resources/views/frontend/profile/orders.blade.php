@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->
        <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.profile_orders_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Orders -->
        <section class="profile">
        <div class="container-fluid">
            <div class="profile__row">
                @include('frontend.partials.profile_sidebar')
                <div class="profile__body">
                    @if($orders->isNotEmpty())
                        <div class="orders">
                        <table>
                            <thead>
                            <tr>
                                <th>@lang('site.profile.order_id')</th>
                                <th>@lang('site.profile.date')</th>
                                <th>@lang('site.profile.status')</th>
                                <th>@lang('site.profile.total_count')</th>
                                <th>@lang('site.profile.total_price')</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $key => $order)
                                    <tr>
                                        <td>{{ $order->order_id }}</td>
                                        <td>{{ getParsedDate($order->created_at) }}</td>
                                        <td><span class="success">{{ \App\Enum\OrderStatus::get($order->status) }}</span></td>
                                        <td>{{ $order->total_count }}</td>
                                        <td>{{ get_price($order->total_price) }}</td>
                                        <td><a href="{{ route('history.view', $order->order_id) }}" class="view">@lang('site.profile.view')</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                        <div class="empty">
                            <div class="empty__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="4.0rem" height="2.7rem">
                                    <use xlink:href="#svg-empty"></use>
                                </svg>
                            </div>
                            <h1>@lang('site.empty.dear_customer')</h1>
                            <p>@lang('site.empty.products')</p>
                            <a href="{{ route('home') }}" class="btn btn-orange">@lang('site.empty.order_products')</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End Orders -->
@endsection
