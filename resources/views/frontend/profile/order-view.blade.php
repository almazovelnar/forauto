@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li><a href="{{route('history')}}">@lang('site.layout.orders_page')</a></li>
            <li>#{{$order->order_id}}</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Orders -->
    <section class="profile">
        <div class="container-fluid">
            <div class="profile__row">
                @include('frontend.partials.profile_sidebar')
                <div class="profile__body">
                    <div class="orders">
                        <table>
                            <thead>
                            <tr>
                                <th></th>
                                <th>@lang('site.profile.product_code')</th>
                                <th>@lang('site.profile.product_name')</th>
                                <th>@lang('site.profile.count')</th>
                                <th>@lang('site.profile.price')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="{{route('product', $item->product->slug)}}" class="text-black">{{ $item->code }}</a></td>
                                    <td><a href="{{route('product', $item->product->slug)}}" class="text-black">{{ $item->product->title }}</a></td>
                                    <td>{{ $item->count }}</td>
                                    <td>{{ get_price($item->price) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Orders -->
@endsection
