@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li>@lang('site.profile.page_title')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- my account start  -->
    <div class="account_page_bg">
        <div class="container">
            <section class="main_content_area">
                <div class="account_dashboard">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 col-lg-3">
                            <!-- Nav tabs -->
                            <div class="dashboard_tab_button">
                                <ul role="tablist" class="nav flex-column dashboard-list">
                                    <li><a href="{{ route('profile') }}" class="nav-link active">@lang('site.profile.account_details')</a></li>
                                    <li><a href="{{ route('history') }}" class="nav-link">@lang('site.profile.orders')</a></li>
                                    <li><a href="javascript:void(0);" data-url="{{ route('logout') }}" class="profileLogout nav-link">@lang('site.profile.logout')</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-9 col-lg-9">
                            <!-- Tab panes -->
                            <div class="tab-content dashboard_content">
                                <div class="tab-pane fade show active" id="account-details">
                                    <h3>@lang('site.profile.account_details')</h3>
                                    <div class="login">
                                        <div class="login_form_container">
                                            <div class="account_login_form">
                                                <p>{{ session()->get('notification') }}</p>
                                                <form action="{{ route('profile.edit') }}" method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <label>@lang('site.profile.email')</label>
                                                    <input type="email" name="email" value="{{ $customer->email }}" class="@error('email') has-error @enderror">
                                                    <label>@lang('site.profile.name')</label>
                                                    <input type="text" name="name" value="{{ $customer->name }}" class="@error('name') has-error @enderror">
                                                    <label>@lang('site.profile.surname')</label>
                                                    <input type="text" name="surname" value="{{ $customer->surname }}" class="@error('surname') has-error @enderror">
                                                    <label>@lang('site.profile.phone')</label>
                                                    <input type="text" name="phone" value="{{ $customer->phone }}" class="@error('phone') has-error @enderror">
                                                    <label>@lang('site.profile.city')</label>
                                                    <input type="text" name="city" value="{{ $customer->city }}" class="@error('city') has-error @enderror">
                                                    <label>@lang('site.profile.address')</label>
                                                    <input type="text" name="address" value="{{ $customer->address }}" class="@error('address') has-error @enderror">
                                                    <label>@lang('site.profile.new_password')</label>
                                                    <input type="password" name="password" class="@error('password') has-error @enderror">
                                                    <label>@lang('site.profile.repeat_password')</label>
                                                    <input type="password" name="passwordConfirm" class="@error('passwordConfirm') has-error @enderror">
                                                    <div class="save_button primary_btn default_button">
                                                        <button type="submit">@lang('site.profile.save')</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- my account end   -->

    @widget('brand_slider')
@endsection

@section('addJs')
    <script>
        $('.profileLogout').on('click', function (e) {
            e.preventDefault();
            if (confirm("{{ trans('site.profile.confirm_logout') }}")) {
                window.location.href = "{{ route('logout') }}";
            }
        })
    </script>
@endsection
