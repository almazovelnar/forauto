@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.wishlist_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Products -->
    <section class="products">
        <div class="d-none filter-url" data-url="{{ route('favorite.filter') }}"></div>
        <div class="container-fluid">
            <h1 class="page-title mb-5">@lang('site.layout.wishlist') <span class="text-gray">({{ $products->count() }})</span></h1>
            <div class="products__row">
                <div class="products__body ps-0">
                    @if($products->isNotEmpty())
                        <div class="products__body__head">
                            <div class="form-group mb-md-0">
                                <select class="form-control filter-sort">
                                    @foreach(\App\Enum\Sort::getList() as $key => $sortName)
                                        <option value="{{ $key }}" @if(isset($filter['sort']) && $key == $filter['sort']) selected @endif>{{ $sortName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group--checkbox">
                                <input type="checkbox" class="d-none filter-discount" id="sale" @if(isset($filter['discount']) && $filter['discount']) checked @endif>
                                <label for="sale" class="text-gray">@lang('site.filter.discount')</label>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($products as $product)
                                <div class="col-md-3">
                                    <div class="products__item products__item--favorite" data-id="{{ $product->id }}">
{{--                                        <p class="products__label">Stokda yeni</p>--}}
                                        <a href="javascript:void(0);" class="products__wishlist removeFromWishlist" aria-label="wishlist">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="1.8rem" height="1.6rem">
                                                <use xlink:href="#svg-close"></use>
                                            </svg>
                                        </a>
                                        <a href="{{ route('product', $product->slug) }}" class="products__image"><img src="{{ product_image($product->image) }}" alt="{{ $product->title }}"></a>
                                        <div class="products__text">
                                            <a href="{{ route('product', $product->slug) }}" class="products__title">{{ $product->title }}</a>
                                            <span class="products__category">{{ $product->category->title }}</span>
                                            @if($product->stars)
                                                <ul class="products__star">
                                                    @foreach(range(1,5) as $value)
                                                        <li class="@if($product->stars >= $value) active @endif">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="1.6rem" height="1.6rem">
                                                                <use xlink:href="#svg-star"></use>
                                                            </svg>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                            <div class="products__price">
                                                @if($product->sale_price)
                                                    <span
                                                        class="products__price__new">{{ get_price($product->sale_price) }}</span>
                                                    <span
                                                        class="products__price__old">{{ get_price($product->price) }}</span>
                                                @else
                                                    <span
                                                        class="products__price__new">{{ get_price($product->price) }}</span>
                                                @endif
                                            </div>

                                            @if(!in_array($product->id, cart_ids()))
                                                <a href="javascript:void(0);" class="btn w-100 addToCart"
                                                   title="@lang('site.product.add_to_cart')">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.2rem" height="2rem">
                                                        <use xlink:href="#svg-basket"></use>
                                                    </svg>
                                                    <span>@lang('site.product.add_to_cart')</span>
                                                </a>
                                            @else
                                                <a href="javascript:void(0);" class="btn w-100 removeFromCart"
                                                   title="@lang('site.product.remove_from_cart')">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.2rem" height="2rem">
                                                        <use xlink:href="#svg-basket"></use>
                                                    </svg>
                                                    <span>@lang('site.product.remove_from_cart')</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="empty">
                            <div class="empty__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="4.0rem" height="2.7rem">
                                    <use xlink:href="#svg-empty"></use>
                                </svg>
                            </div>
                            <h1>@lang('site.empty.dear_customer')</h1>
                            <p>@lang('site.empty.wishlist_products')</p>
                            <a href="{{ route('home') }}" class="btn btn-orange">@lang('site.empty.order_products')</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End Products -->

    @widget('seo-widget')
@endsection
