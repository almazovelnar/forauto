@extends('frontend.layouts.app')

@section('content')
    <!-- Main -->
    <section class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    @widget('main-filter')
                </div>
                <div class="col-md-9">
                    <div class="main__banner"
                         data-flickity='{ "cellAlign": "left", "contain": true,"prevNextButtons":false,"wrapAround":true }'>
                        @foreach($slides as $slide)
                            <div class="main__banner__item">
                                <a @if($slide->link) href="{{ $slide->link }}" @endif>
                                    <div class="main__banner__image"><img
                                            src="{{ asset('storage/slides/' . $slide->image) }}" alt="{{ $slide->title }}">
                                    </div>
                                    <div class="main__banner__text">
                                        @if($slide->info)
                                            <span>{!! $slide->info !!}</span>
                                        @endif
                                        <h2>{{ $slide->title }}</h2>
                                        <p>{{ $slide->description }}</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Main -->

    <!-- Services -->
    <section class="services m-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="services__item">
                        <span class="services__icon"><img src="/assets/front/images/s-icon1.svg" alt="@lang('site.service.title_1')"></span>
                        <div class="services__text">
                            <h3>@lang('site.service.title_1')</h3>
                            <p>@lang('site.service.text_1')</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services__item">
                        <span class="services__icon"><img src="/assets/front/images/s-icon2.svg" alt="@lang('site.service.title_2')"></span>
                        <div class="services__text">
                            <h3>@lang('site.service.title_2')</h3>
                            <p>@lang('site.service.text_2')</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="services__item">
                        <span class="services__icon"><img src="/assets/front/images/s-icon3.svg" alt="@lang('site.service.title_3')"></span>
                        <div class="services__text">
                            <h3>@lang('site.service.title_3')</h3>
                            <p>@lang('site.service.text_3')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services -->


    @if($topProducts->isNotEmpty())
        <!-- Products -->
        <section class="products m-top">
            <div class="container-fluid">
                <h2 class="section-title">@lang('site.layout.popular_products')</h2>
                <div class="row products__slider-mob">
                    @foreach($topProducts as $topProduct)
                        <div class="col-md-3">
                            @include('frontend.partials._product', ['product' => $topProduct])
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Products -->
    @endif


    @if($hotProducts->isNotEmpty())
        <!-- Products -->
        <section class="products m-top">
            <div class="container-fluid">
                <h2 class="section-title">@lang('site.layout.discount_products')</h2>
                <div class="row products__slider-mob">
                    @foreach($hotProducts as $hotProduct)
                        <div class="col-md-3">
                            @include('frontend.partials._product', ['product' => $hotProduct])
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Products -->
    @endif

    @if($seasonProducts->count() == 2)
        <!-- Banners -->
        <section class="banners m-top">
            <div class="container-fluid">
                <h2 class="section-title">@lang('site.layout.seasonal_products')</h2>
                <div class="row">
                    @foreach($seasonProducts as $key => $seasonProduct)
                        @if($key == 0)
                            <div class="col-md-7">
                                <a href="{{ route('product', $seasonProduct->slug) }}" class="banners__item">
                                    <div class="banners__image"><img src="{{ product_image($seasonProduct->season_banner) }}" alt="{{ $seasonProduct->title }}"></div>
                                    <div class="banners__text">
                                        <span class="banners__label">@lang('site.product.season_title_1')</span>
                                        <h2>@lang('site.product.season_info_1')</h2>
                                        <p>@lang('site.product.season_text_1')</p>
                                    </div>
                                </a>
                            </div>
                        @else
                            <div class="col-md-5">
                                <a href="{{ route('product', $seasonProduct->slug) }}" class="banners__item">
                                    <div class="banners__image"><img src="{{ product_image($seasonProduct->season_banner) }}" alt="{{ $seasonProduct->title }}"></div>
                                    <div class="banners__text">
                                        <span class="banners__label">@lang('site.product.season_title_2')</span>
                                        <h2>@lang('site.product.season_info_2')</h2>
                                        <p>@lang('site.product.season_text_2')</p>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Banners -->
    @endif

    @widget('blog-widget')
    @widget('brand-slider')
    @widget('seo-widget')
@endsection
