@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.cart_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Basket -->
    <section class="basket">
        <div class="container-fluid">
            <h1 class="page-title mb-5">@lang('site.layout.cart') <span class="text-gray">({{ $products->count() }})</span></h1>
            <div class="products__row">
                <div class="products__body ps-0 pt-md-5">
                    @if($products->isNotEmpty())
                        <div class="row pt-md-3 align-items-start">
                        <div class="col">
                            @foreach($products as $product)
                                @php $inCart = $cartProducts->where('id', $product->id)->first(); @endphp
                                <div class="basket__item" data-id="{{ $product->id }}">
                                    <a href="{{ route('product', $product->slug) }}" class="basket__image"><img src="{{ product_image($product->image) }}" alt="{{ $product->title }}"></a>
                                    <div class="basket__info">
                                        <div class="basket__text">
                                            <a href="{{ route('product', $product->slug) }}" class="products__title">{{ $product->title }}</a>
                                            <span class="products__category">{{ $product->category->title }}</span>
{{--                                            <span class="products__category">Təxmini çatdırılma  1-2 iş günü</span>--}}
                                        </div>
                                        <div class="basket__count">
                                            <button class="basket__count__btn basket__count__btn--minus" @if($inCart->qty == 1) disabled @endif>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.2rem">
                                                    <use xlink:href="#svg-minus"></use>
                                                </svg>
                                            </button>
                                            <input min="1" max="99" value="{{ $inCart->qty }}" type="number" class="basket__count__input productQty">
                                            <button class="basket__count__btn basket__count__btn--plus" @if($inCart->qty == 99) disabled @endif>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.2rem">
                                                    <use xlink:href="#svg-plus"></use>
                                                </svg>
                                            </button>
                                        </div>
                                        <span class="products__price__new">{{ get_price($product->sale_price ?: $product->price) }}</span>
                                        <button class="basket__remove removeFromCartIn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.2rem">
                                                <use xlink:href="#svg-delete"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-3 basket__side-bar">
                            <div class="basket__result">
                                <p>@lang('site.cart.cart_totals')</p>
                                <ul>
{{--                                    <li>--}}
{{--                                        <span>Məhsul məbləği</span>--}}
{{--                                        <span><strong>172 Azn</strong></span>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <span>Endirim</span>--}}
{{--                                        <span><strong>0 Azn</strong></span>--}}
{{--                                    </li>--}}
                                    <li>
                                        <span>@lang('site.cart.subtotal')</span>
                                        <span><strong>{{ get_price(cart()->priceTotal())}}</strong></span>
                                    </li>
                                </ul>
{{--                                <div class="form-group">--}}
{{--                                    <input type="text" class="form-control" placeholder="Promo kodu daxil et">--}}
{{--                                </div>--}}
                                <a href="{{ route('checkout') }}" class="btn btn-orange btn--sm w-100">@lang('site.cart.checkout')</a>
                            </div>
                        </div>
                    </div>
                    @else
                        <div class="empty">
                            <div class="empty__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="4.0rem" height="2.7rem">
                                    <use xlink:href="#svg-empty"></use>
                                </svg>
                            </div>
                            <h1>@lang('site.empty.dear_customer')</h1>
                            <p>@lang('site.empty.products')</p>
                            <a href="{{ route('home') }}" class="btn btn-orange">@lang('site.empty.order_products')</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End Basket -->

@endsection
