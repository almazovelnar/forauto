@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>{{ $post->title }}</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Terms -->
    <section class="content-page">
        <div class="container-fluid container-fluid--sm">
            <h1 class="page-title page-title--lg mb-5 text-center">{{ $post->title }}</h1>
            <div class="content">
                <img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}">
                {!! $post->description !!}
            </div>
        </div>
    </section>
    <!-- End Terms -->
@endsection
