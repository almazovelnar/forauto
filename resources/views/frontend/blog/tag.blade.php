@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li><a href="{{route('blog.index')}}">@lang('site.layout.blog_page')</a></li>
                            <li>#{{ $tag->name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <div class="blog_bg_area">
        <div class="container">
            <!--blog area start-->
            <div class="blog_page_section blog_reverse">
                <div class="row">
                    <div class="col-lg-3 col-md-12">
                        @widget('blog_sidebar')
                    </div>

                    <div class="col-lg-9 col-md-12">
                        <div class="blog_wrapper mb-30">
                            <div class="blog_header">
                                <h1>#{{ $tag->name }}</h1>
                            </div>
                            <div class="blog_wrapper_inner">

                                @if(!$tag->posts->isEmpty())
                                    @foreach($tag->posts as $post)
                                        <article class="single_blog">
                                            <figure>
                                                <div class="blog_thumb">
                                                    <a href="{{ route('blog.post', $post->slug) }}"><img src="{{ post_image($post->image) }}" alt="{{ $post->title }}"></a>
                                                </div>
                                                <figcaption class="blog_content">
                                                    <h4 class="post_title"><a href="{{ route('blog.post', $post->slug) }}">{{ $post->title }}</a></h4>
                                                    <div class="blog_meta">
                                                        <span class="meta_date">{{ $post->published_at }}</span>
                                                    </div>
                                                    <div class="blog_desc">
                                                        <p>{{ \Illuminate\Support\Str::limit($post->description, 150) }}</p>
                                                    </div>
                                                    <footer class="btn_more">
                                                        <a href="{{ route('blog.post', $post->slug) }}"> @lang('site.blog.read_more')</a>
                                                    </footer>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    @endforeach
                                @else
                                    <h4>@lang('site.blog.not_found_results')</h4>
                                @endif
                            </div>
                        </div>
                        <!--blog pagination area start-->
                        {{ $posts->onEachSide(2)->links('frontend.partials.blog_pagination') }}
                        <!--blog pagination area end-->
                    </div>
                </div>
            </div>
            <!--blog area end-->


        </div>
    </div>

    @widget('brand_slider')
@endsection
