@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.blog_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Blog -->
    <section class="blog">
        <div class="container-fluid">
            <h1 class="page-title page-title--lg mb-5">@lang('site.layout.blog_page')</h1>
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-4">
                        <a href="{{ route('blog.post', $post->slug) }}" class="blog__item">
                            <div class="blog__image"><img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}"></div>
                            <div class="blog__text">
                                <h2>{{ $post->title }}</h2>
                                <span>
                                    @lang('site.layout.read_more')
                                    <svg xmlns="http://www.w3.org/2000/svg" width="1.4rem" height="1.4rem">
                                        <use xlink:href="#svg-arrow-right"></use>
                                    </svg>
                                </span>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- End Blog -->
@endsection
