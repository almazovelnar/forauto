@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li>{{ $post->title }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--blog body area start-->
    <div class="blog_bg_area blog_details_bg">
        <div class="container">
            <div class="blog_page_section">
                <div class="row">
                    <div class="col-lg-9 col-md-12">
                        <!--blog grid area start-->
                        <div class="blog_wrapper blog_details">
                            <article class="single_blog">
                                <figure>
                                    <div class="post_header">
                                        <h3 class="post_title">{{ $post->title }}</h3>
                                        <div class="blog_meta">
                                            <span class="post_category"><a href="{{ route('blog.category', $post->category->slug) }}">{{ $post->category->title }}</a></span> /
                                            <span class="meta_date">{{ getParsedDate($post->published_at) }}</span>
                                        </div>
                                    </div>
                                    @if($post->image)
                                        <div class="blog_thumb">
                                            <img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}">
                                        </div>
                                    @endif
                                    <figcaption class="blog_content">
                                        <div class="post_content">
                                            {!! $post->description !!}
                                        </div>
                                    </figcaption>
                                </figure>
                            </article>

                            @if(!$relatedPosts->isEmpty())
                                <div class="related_posts">
                                    <h3>@lang('site.blog.related_posts')</h3>
                                    <div class="row">
                                        @foreach($relatedPosts as $relatedPost)
                                            <div class="col-lg-4 col-md-6">
                                                <article class="single_related">
                                                    <figure>
                                                        <div class="related_thumb">
                                                            <img src="{{ asset('storage/posts/' . $relatedPost->image) }}" alt="{{ $relatedPost->title }}">
                                                        </div>
                                                        <figcaption class="related_content">
                                                            <h4><a href="{{ route('blog.post', $relatedPost->slug) }}">{{ $relatedPost->title }}</a></h4>
                                                            <div class="blog_meta">
                                                                <span class="meta_date"> {{ $relatedPost->published_at }}	</span>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </article>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!--blog grid area start-->
                    </div>

                    <div class="col-lg-3 col-md-12">
                        @widget('blog_sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--blog section area end-->

    <!--brand area start-->
@endsection
