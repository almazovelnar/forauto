@extends('frontend.layouts.app')

@section('content')
    <!-- Products -->
    <section class="products">
        <div class="d-none filter-url" data-url="{{ route('search.filter') }}"></div>
        <div class="container-fluid">
            <h1 class="page-title">{{ $query }}</h1>
            <div class="products__filter-result">
                <p>@lang('site.product.search_results', ['count' => $products->count()])</p>
            </div>
            <div class="products__row">
                <div class="products__body ps-0">
                        @if($products)
                            <div class="products__body__head">
                                <div class="form-group mb-md-0">
                                    <select class="form-control filter-sort">
                                        @foreach(\App\Enum\Sort::getList() as $key => $sortName)
                                            <option value="{{ $key }}" @if(isset($filter['sort']) && $key == $filter['sort']) selected @endif>{{ $sortName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group form-group--checkbox">
                                    <input type="checkbox" class="d-none filter-discount" id="sale" @if(isset($filter['discount']) && $filter['discount']) checked @endif>
                                    <label for="sale" class="text-gray">@lang('site.filter.discount')</label>
                                </div>
                            </div>
                            <div class="row">

                                @foreach($products as $product)
                                    <div class="col-md-3">
                                        @include('frontend.partials._product', ['product' => $product])
                                    </div>
                                @endforeach

                                <div class="col-12">
                                    {{ $products->links('frontend.partials.product_pagination') }}
                                </div>
                            </div>
                        @else
                            <div class="empty">
                                <div class="empty__icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="4.0rem" height="2.7rem">
                                        <use xlink:href="#svg-empty"></use>
                                    </svg>
                                </div>
                                <h1>@lang('site.empty.dear_customer')</h1>
                                <p>@lang('site.empty.search_products')</p>
                                <a href="{{ route('home') }}" class="btn btn-orange">@lang('site.empty.order_products')</a>
                            </div>
                        @endif
                    </div>
            </div>
        </div>
    </section>
    <!-- End Products -->
@endsection
