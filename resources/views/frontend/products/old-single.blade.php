@extends('frontend.layouts.app')

@section('content')
    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li><a href="{{route('products')}}">@lang('site.layout.products_page')</a></li>
                            <li>{{ $product->title }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <div class="product_page_bg">
        <div class="container">
            <!--product details start-->
            <div class="product_details" data-id="{{ $product->id }}">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="product-details-tab">
                            <div id="img-1" class="zoomWrapper single-zoom">
                                <a href="#">
                                    <img id="zoom1" src="{{ asset('media/large/' . $product->image) }}" data-zoom-image="{{ asset('media/large/' . $product->image) }}" alt="big-1">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="product_d_right">
                            <form action="#">

                                <h3><a href="javascript:void(0);">{{ $product->title }}</a></h3>
                                <div class="price_box">
                                    @if($product->sale_price)
                                        <span class="old_price">{{ get_price($product->price) }} AZN</span>
                                        <span class="current_price">{{ get_price($product->sale_price) }} AZN</span>
                                    @else
                                        <span class="current_price">{{ get_price($product->price) }} AZN</span>
                                    @endif
                                </div>
                                <div class="product_desc">
                                    <p>{!! $product->description !!}</p>
                                </div>
                                <div class="product_variant quantity">
                                    <label>@lang('site.product.quantity')</label>
                                    <input min="1" max="100" value="1" type="number" class="quantityCartIn">
                                    <button type="button" class="button addToCartIn">@lang('site.product.add_to_cart')</button>
                                </div>
                                <div class=" product_d_action">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);" class="addToWishlistIn @if(in_array($product->id, wishlist_ids())) d-none @endif">+ @lang('site.product.add_to_wishlist')</a>
                                            <a href="javascript:void(0);" class="removeFromWishlistIn @if(!in_array($product->id, wishlist_ids())) d-none @endif">- @lang('site.product.remove_from_wishlist')</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="product_meta">
                                    <span>@lang('site.product.category_name') <a href="#">{{ $product->category->title }}</a></span>
                                </div>

                            </form>
{{--                            <div class="priduct_social">--}}
{{--                                <ul>--}}
{{--                                    <li><a class="facebook" href="#" title="facebook"><i class="fa fa-facebook"></i> Like</a></li>--}}
{{--                                    <li><a class="twitter" href="#" title="twitter"><i class="fa fa-twitter"></i> tweet</a></li>--}}
{{--                                    <li><a class="pinterest" href="#" title="pinterest"><i class="fa fa-pinterest"></i> save</a></li>--}}
{{--                                    <li><a class="google-plus" href="#" title="google +"><i class="fa fa-google-plus"></i> share</a></li>--}}
{{--                                    <li><a class="linkedin" href="#" title="linkedin"><i class="fa fa-linkedin"></i> linked</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}

                        </div>
                    </div>
                </div>
            </div>
            <!--product details end-->

            @if(!empty($product->specifications))
                <!--product info start-->
                <div class="product_d_info">
                    <div class="row">
                        <div class="col-12">
                            <div class="product_d_inner">
                                <div class="product_info_button">
                                    <ul class="nav" role="tablist" id="nav-tab">
                                        <li>
                                            <a class="active" data-bs-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">@lang('site.product.specifications')</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="sheet" role="tabpanel" >
                                        <div class="product_d_table">
                                            {!! $product->specifications !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--product info end-->
            @endif

            @if(!$relatedProducts->isEmpty())
                <!--product area start-->
                    <section class="product_area related_products">
                        <div class="row">
                            <div class="col-12">
                                <div class="section_title title_style2">
                                    <div class="title_content">
                                        <h2>@lang('site.product.related_products')</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="product_carousel product_details_column5 owl-carousel">
                                @foreach($relatedProducts as $relatedProduct)
                                    <div class="col-lg-3">
                                        <article class="single_product" data-id="{{ $relatedProduct->id }}">
                                            <figure>
                                                <div class="product_thumb">
                                                    <a class="primary_img" href="{{ route('product', $relatedProduct->slug) }}"><img src="{{ product_image($relatedProduct->image) }}" alt="{{ $relatedProduct->title }}"></a>
                                                </div>
                                                <div class="product_content">
                                                    <div class="product_content_inner">
                                                        <p class="manufacture_product"><a href="javascript:void(0);">{{ $relatedProduct->brand->title }}</a></p>
                                                        <h4 class="product_name"><a href="{{ route('product', $relatedProduct->slug) }}">{{ $relatedProduct->title }}</a></h4>
                                                        <div class="price_box">
                                                            @if($relatedProduct->sale_price)
                                                                <span class="old_price">{{ get_price($relatedProduct->price) }} AZN</span>
                                                                <span class="current_price">{{ get_price($relatedProduct->sale_price) }} AZN</span>
                                                            @else
                                                                <span class="current_price">{{ get_price($relatedProduct->price) }} AZN</span>
                                                            @endif
                                                        </div>

                                                    </div>
                                                    <div class="action_links">
                                                        <ul>
                                                            <li class="add_to_cart">
                                                                @if(!in_array($relatedProduct->id, cart_ids()))
                                                                    <a class="addToCart"  href="javascript:void(0);" title="@lang('site.product.add_to_cart')">@lang('site.product.add_to_cart')</a>
                                                                @else
                                                                    <a class="removeFromCart"  href="javascript:void(0);" title="@lang('site.product.remove_from_cart')">@lang('site.product.remove_from_cart')</a>
                                                                @endif
                                                            </li>
                                                            <li class="wishlist"><a class="addToWishlist @if(in_array($relatedProduct->id, wishlist_ids())) active @endif" href="javascript:void(0);"  title="@lang('site.product.add_to_wishlist')"><i class="icon-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </figure>
                                        </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                    <!--product area end-->
            @endif
        </div>
    </div>

    @widget('brand_slider')
@endsection
