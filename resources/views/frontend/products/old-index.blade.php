@extends('frontend.layouts.app')

@section('content')


    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li>@lang('site.layout.shop_page')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!--shop  area start-->
    <div class="shop_area shop_reverse">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    @widget('product_sidebar')
                </div>
                <div class="col-lg-9 col-md-12">

{{--                    <!--shop banner area start-->--}}
{{--                    <div class="shop_banner_area mb-30">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-12">--}}
{{--                                <div class="shop_banner_thumb">--}}
{{--                                    <img src="assets/img/bg/banner23.jpg" alt="">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--shop banner area end-->--}}

                    <!--shop toolbar start-->
                    <div class="shop_toolbar_wrapper">
                        <div class="niceselect_option">
                            <form class="select_option" action="#">
                                <select name="orderby" id="short" class="sortProduct">
                                    @foreach($sortData as $sortKey => $sortName)
                                        <option value="{{ $sortKey }}" @if(request()->input('filters.sort') == $sortKey) selected @endif>{{ $sortName }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                        <div class="page_amount">
                            <p>{{ trans('site.product.page_info', ['show' => $products->firstItem().' - '.$products->lastItem(), 'total' => $products->total()]) }}</p>
                        </div>
                    </div>
                    <!--shop toolbar end-->
                    <div class="row shop_wrapper">
                        @if(!$products->isEmpty())
                            @foreach($products as $product)
                                <div class="col-lg-3 col-md-4 col-6">
                                    <article class="single_product" data-id="{{ $product->id }}">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img" href="{{ route('product', $product->slug) }}"><img src="{{ product_image($product->image) }}" alt="{{ $product->title }}"></a>
                                            </div>
                                            <div class="product_content grid_content">
                                                <div class="product_content_inner">
                                                    <p class="manufacture_product"><a href="javascript:void(0);">{{ $product->brand->title }}</a></p>
                                                    <h4 class="product_name"><a href="{{ route('product', $product->slug) }}">{{ $product->title }}</a></h4>
                                                    <div class="price_box">
                                                        @if($product->sale_price)
                                                            <span class="old_price">{{ get_price($product->price) }} AZN</span>
                                                            <span class="current_price">{{ get_price($product->sale_price) }} AZN</span>
                                                        @else
                                                            <span class="current_price">{{ get_price($product->price) }} AZN</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="add_to_cart">
                                                            @if(!in_array($product->id, cart_ids()))
                                                                <a class="addToCart"  href="javascript:void(0);" title="@lang('site.product.add_to_cart')">@lang('site.product.add_to_cart')</a>
                                                            @else
                                                                <a class="removeFromCart"  href="javascript:void(0);" title="@lang('site.product.remove_from_cart')">@lang('site.product.remove_from_cart')</a>
                                                            @endif
                                                        </li>
                                                        <li class="wishlist"><a class="addToWishlist @if(in_array($product->id, wishlist_ids())) active @endif" href="javascript:void(0);"  title="@lang('site.product.add_to_wishlist')"><i class="icon-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </figure>
                                    </article>
                                </div>
                            @endforeach
                        @else
                            <h4>@lang('site.product.not_found_results')</h4>
                        @endif
                    </div>

                    {{ $products->onEachSide(2)->links('frontend.partials.product_pagination') }}
                </div>
            </div>
        </div>
    </div>
    <!--shop  area end-->

    @widget('brand_slider')
@endsection


@section('addJs')
    <script src="{{ asset('assets/front/js/filter.js') }}"></script>
@endsection
