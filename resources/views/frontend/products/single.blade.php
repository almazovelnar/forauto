@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li><a href="{{route('products', $product->category->slug)}}">{{ $product->category->title }}</a></li>
            <li>{{ $product->title }}</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Product -->
    <section class="product" data-id="{{ $product->id }}">
        <div class="container-fluid">
            <div class="product__main">
                <form action="{{ route('fast-buy', ['id' => $product->id]) }}" method="POST">
                    @csrf
                    <div class="d-md-flex justify-content-between gap-5">
                        <div class="product__images">
                            @if($product->photos->count() > 1)
                                <div class="product__slider-nav">
                                    @foreach($product->photos as $photo)
                                        <div class="product__slider-nav__item"><img
                                                src="{{ get_image($photo->filename, $photo->path, 'o') }}"
                                                alt="{{ $product->title }}"></div>
                                    @endforeach
                                </div>
                            @endif
                            <div class="product__slider">
                                <a href="javascript:void(0);"
                                   class="products__wishlist @if(in_array($product->id, wishlist_ids())) removeFromWishlistIn active @else addToWishlistIn @endif"
                                   aria-label="wishlist">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.6rem" height="3.0rem">
                                        <use xlink:href="#svg-heart"></use>
                                    </svg>
                                </a>

                                @if($product->photos->isEmpty())
                                    <div class="product__slider__item">
                                        <img src="{{ product_image($product->image) }}" alt="{{ $product->title }}">
                                    </div>
                                @else
                                    @foreach($product->photos as $photo)
                                        <div class="product__slider__item"><img
                                                src="{{ get_image($photo->filename, $photo->path, 'o') }}"
                                                alt="{{ $product->title }}"></div>

                                    @endforeach
                                @endif


                            </div>
                        </div>
                        <div class="product__text">
                            @csrf
                            <h1 class="products__title">{{ $product->title }}</h1>
                            <p class="products__category">{{ $product->category->title }}</p>
                            @if($product->stars)
                                <ul class="products__star">
                                    @foreach(range(1,5) as $value)
                                        <li class="@if($product->stars >= $value) active @endif">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="1.6rem" height="1.6rem">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="products__price">
                                @if($product->sale_price)
                                    <span class="products__price__new">{{ get_price($product->price) }}</span>
                                    <span class="products__price__old">{{ get_price($product->sale_price) }}</span>
                                @else
                                    <span class="products__price__new">{{ get_price($product->price) }}</span>
                                @endif
                            </div>
                            <div class="product__desc">
                                <p>{!! $product->description !!}</p>
                            </div>
                            {{--                                <div class="product__desc">--}}
                            {{--                                    <p>@lang('site.product.code', ['code' => $product->code])</p>--}}
                            {{--                                    <p>@lang('site.product.count', ['count' => $product->count])</p>--}}
                            {{--                                </div>--}}
                            <div class="basket__count">
                                <button class="basket__count__btn basket__count__btn--minus not_update">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.2rem">
                                        <use xlink:href="#svg-minus"></use>
                                    </svg>
                                </button>
                                <input type="number" class="basket__count__input quantityCartIn" value="1" max="{{ $product->count }}"
                                       name="count">
                                <button class="basket__count__btn basket__count__btn--plus not_update">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.2rem">
                                        <use xlink:href="#svg-plus"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="product__btns">
                                <a href="javascript:void(0);" class="btn addToCartIn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.2rem" height="2rem">
                                        <use xlink:href="#svg-basket"></use>
                                    </svg>
                                    @lang('site.product.add_to_cart')
                                </a>
                                <a href="javascript:void(0);" class="btn btn-green-transparent fast-buy">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="1.7rem" height="2rem">
                                        <use xlink:href="#svg-finger"></use>
                                    </svg>
                                    @lang('site.product.fast_buy')
                                </a>
                            </div>
                            <div class="product__fast-buy">
                                <div class="form-group">
                                    <input type="text" class="form-control phone-input" name="phone">
                                </div>
                                <button class="btn btn--sm">@lang('site.product.confirm_fast_buy')</button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="product__info">
                    {{--                    <div class="product__info__item">--}}
                    {{--                        <h2 class="product__info__title">Bir kartla hissə-hissə ödə!</h2>--}}
                    {{--                        <ul class="product__type">--}}

                    {{--                            <li>--}}
                    {{--                                <input name="month" id="month1" type="radio" class="d-none"--}}
                    {{--                                       checked>--}}
                    {{--                                <label for="month1">2 ay</label>--}}
                    {{--                            </li>--}}

                    {{--                            <li>--}}
                    {{--                                <input name="month" id="month2" type="radio" class="d-none"--}}
                    {{--                                >--}}
                    {{--                                <label for="month2">3 ay</label>--}}
                    {{--                            </li>--}}

                    {{--                            <li>--}}
                    {{--                                <input name="month" id="month3" type="radio" class="d-none"--}}
                    {{--                                >--}}
                    {{--                                <label for="month3">6 ay</label>--}}
                    {{--                            </li>--}}

                    {{--                            <li>--}}
                    {{--                                <input name="month" id="month4" type="radio" class="d-none"--}}
                    {{--                                >--}}
                    {{--                                <label for="month4">12 ay</label>--}}
                    {{--                            </li>--}}

                    {{--                        </ul>--}}
                    {{--                        <div class="product__payment-info">--}}
                    {{--                            <table>--}}
                    {{--                                <thead>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th>İlkin ödəniş</th>--}}
                    {{--                                    <th>Aylıq ödəniş</th>--}}
                    {{--                                    <th>Yekun məbləğ</th>--}}
                    {{--                                </tr>--}}
                    {{--                                </thead>--}}
                    {{--                                <tbody>--}}
                    {{--                                <tr>--}}
                    {{--                                    <td>0</td>--}}
                    {{--                                    <td>5</td>--}}
                    {{--                                    <td>86</td>--}}
                    {{--                                </tr>--}}
                    {{--                                </tbody>--}}
                    {{--                            </table>--}}
                    {{--                        </div>--}}
                    {{--                        <a href="#" class="btn btn--md btn-orange w-100">--}}
                    {{--                            <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="1.9rem">--}}
                    {{--                                <use xlink:href="#svg-cards"></use>--}}
                    {{--                            </svg>--}}
                    {{--                            Hissə-hissə al--}}
                    {{--                        </a>--}}
                    {{--                    </div>--}}

                    <div class="product__info__item">
                        <h2 class="product__info__title">@lang('site.product.delivery')</h2>
                        <ul class="product__shipping">
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                    <use xlink:href="#svg-car"></use>
                                </svg>
                                <p>@lang('site.product.delivery_by_courier')<span>@lang('site.product.free')</span></p>
                            </li>
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                    <use xlink:href="#svg-pack"></use>
                                </svg>
                                <p>@lang('site.product.delivery_by_myself')<span>@lang('site.product.free')</span></p>
                            </li>
                        </ul>
                    </div>

                    <div class="product__info__item">
                        <h2 class="product__info__title">@lang('site.product.payment')</h2>
                        <ul class="product__shipping">
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                    <use xlink:href="#svg-money"></use>
                                </svg>
                                <p>@lang('site.product.payment_cash')<span
                                        class="text-gray">@lang('site.product.payment_cash_info')</span></p>
                            </li>
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                    <use xlink:href="#svg-card"></use>
                                </svg>
                                <p>@lang('site.product.payment_card')<span
                                        class="text-gray">@lang('site.product.payment_card_info')</span></p>
                            </li>
                            <li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                    <use xlink:href="#svg-cards"></use>
                                </svg>
                                <p>@lang('site.product.payment_installment_card')<span
                                        class="text-gray">@lang('site.product.payment_installment_card_info')</span></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="product__body">
                <ul class="product__tab tab">
                    <li><a href="javascript:void(0);" class="active tab__item"
                           data-tab="tab1">@lang('site.product.features')</a></li>
                    @if(!empty($product->specifications))
                        <li><a href="javascript:void(0);" class="tab__item"
                               data-tab="tab2">@lang('site.product.description')</a></li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div class="tab-content__item active" data-tab="tab1">
                        <ul class="product__stats">
                            @foreach($product->productProperties as $productProperty)
                                <li>{{ $productProperty->property->title }}<span><a
                                            href="{{ route('products', ['slug' => $product->category->slug, "filter[" . $productProperty->property->slug . "][]" => $productProperty->value]) }}"
                                            class="text-orange">{{ $productProperty->value }}</a></span></li>
                            @endforeach
                        </ul>
                    </div>
                    @if(!empty($product->specifications))
                        <div class="tab-content__item" data-tab="tab2">
                            <div class="product__content">
                                {!! $product->specifications !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End Product -->

    @if($relatedProducts->isNotEmpty())
        <!-- Products -->
        <section class="products m-top pt-4">
            <div class="container-fluid">
                <h2 class="section-title border-0">@lang('site.product.related_products')</h2>
                <div class="row products__slider-mob">
                    @foreach($relatedProducts as $relatedProduct)
                        <div class="col-md-3">
                            @include('frontend.partials._product', ['product' => $relatedProduct])
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Products -->
    @endif
@endsection
