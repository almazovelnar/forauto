@extends('frontend.layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>{{ $category->title }}</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Products -->
    <section class="products">
        <div class="d-none filter-url" data-url="{{ route('products.filter') }}"></div>
        <div class="container-fluid">
            <h1 class="page-title">{{ $category->title }}</h1>
{{--            <div class="products__filter-result">--}}
{{--                @widget('ProductFilter', ['type' => \App\Widgets\ProductFilter::TYPE_LIST])--}}
{{--            </div>--}}
            <div class="row g-0 products__row">
                <input type="checkbox" id="filter_toggle" class="d-none">
                <div class="col-md-3 filter-bar">
                    @widget('ProductFilter', ['type' => \App\Widgets\ProductFilter::TYPE_SIDEBAR])
                </div>
                <div class="col position-relative">
                    <div class="loading"><img src="/assets/front/images/loading.svg" alt=""></div>
                    <div class="products__body">
                        <div class="products__body__head">
                            <label for="filter_toggle" class="products__filter-btn d-md-none">
                                <svg xmlns="http://www.w3.org/2000/svg" width="1.8rem" height="1.6rem">
                                    <use xlink:href="#svg-filter"></use>
                                </svg>
                            </label>
                            <div class="form-group mb-md-0">
                                <select class="form-control filter-sort">
                                    @foreach(\App\Enum\Sort::getList() as $key => $sortName)
                                        <option value="{{ $key }}" @if(isset($filter['sort']) && $key == $filter['sort']) selected @endif>{{ $sortName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group--checkbox">
                                <input type="checkbox" class="d-none filter-discount" id="sale" @if(isset($filter['discount']) && $filter['discount']) checked @endif>
                                <label for="sale" class="text-gray">@lang('site.filter.discount')</label>
                            </div>
                        </div>
                        <div class="row products-container">
                            @foreach($products as $product)
                                <div class="col-md-4">
                                    @include('frontend.partials._product', ['product' => $product])
                                </div>
                            @endforeach

                            <div class="col-12">
                                {{ $products->links('frontend.partials.product_pagination') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Products -->

    @widget('seo-widget')
@endsection
