@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li>@lang('site.login.login_page')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- customer login start -->
    <div class="login_page_bg">
        <div class="container">
            <div class="customer_login">
                <div class="row">
                    <!--login area start-->
                    <div class="offset-md-3 col-lg-6 col-md-6">
                        <div class="account_form login">
                            <h2>@lang('site.login.login_title')</h2>
                            <form action="{{ route('login.submit') }}" method="POST">
                                @csrf
                                <p>
                                    <label>@lang('site.login.email')</label>
                                    <input type="email" name="email">
                                </p>
                                <p>
                                    <label>@lang('site.login.password')</label>
                                    <input type="password" name="password">
                                </p>
                                <div class="login_submit">
{{--                                    <a href="#">Lost your password?</a>--}}
                                    <button type="submit">@lang('site.login.login')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--login area start-->
                </div>
            </div>
        </div>
    </div>

    <!-- customer login end -->
    @widget('brand_slider')
@endsection
