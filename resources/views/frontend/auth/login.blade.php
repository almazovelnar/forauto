@extends('frontend.layouts.auth-app')

@section('content')
    <section class="login">
        <div class="row g-0">
            <div class="col-md-6">
                <div class="login__image"><img src="/assets/front/images/login.jpg" alt=""></div>
            </div>
            <div class="col-md-6">
                <div class="login__body">
                    <a href="{{ route('home') }}" class="login__back" aria-label="logo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1.6rem" height="1.6rem">
                            <use xlink:href="#svg-prev"></use>
                        </svg>
                        @lang('site.register.return_back')
                    </a>
                    <form action="{{ route('login.submit') }}" class="login__form" method="POST">
                        @csrf
                        <div class="login__head">
                            <h1>@lang('site.login.welcome')</h1>
                            <p>@lang('site.login.welcome_text')</p>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.login.phone')</label>
                            <input type="text" name="phone" class="form-control phone-input" value="{{ old('phone') }}"
                                   placeholder="@lang('site.login.phone_placeholder')">

                            @error('phone')
                            <span class="error-text">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group form-group--password mb-4">
                            <label>@lang('site.login.password')</label>
                            <div class="position-relative">
                                <input type="password" name="password" class="form-control"
                                       placeholder="@lang('site.login.password_placeholder')">
                                <span class="show-pass">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                        <use xlink:href="#svg-eye"></use>
                                    </svg>
                                </span>
                            </div>

                            @error('password')
                            <span class="error-text">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group form-group--checkbox mb-5">
                            <input type="checkbox" name="remember" id="remember" class="d-none">
                            <label for="remember">@lang('site.login.remember_me')</label>
                        </div>
                        <button class="btn btn--md w-100" type="submit">@lang('site.login.login_button')</button>
                    </form>
                    <div class="login__footer">
                        <p>@lang('site.login.dont_have_account') <a
                                href="{{ route('register') }}">@lang('site.login.sign_up')</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
