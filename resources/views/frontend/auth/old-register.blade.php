@extends('frontend.layouts.app')

@section('content')

    <!--breadcrumbs area start-->
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
                            <li>@lang('site.login.login_page')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs area end-->

    <!-- customer login start -->
    <div class="login_page_bg">
        <div class="container">
            <div class="customer_login">
                <div class="row">
                    <!--register area start-->
                    <div class="offset-md-3 col-lg-6 col-md-6">
                        <div class="account_form register">
                            <h2>@lang('site.login.register_title')</h2>
                            <form action="{{ route('register.submit') }}" method="POST">
                                @csrf
                                <p>
                                    <label>@lang('site.login.email') <span>*</span></label>
                                    <input type="text" name="email">
                                    @error('email')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p>
                                    <label>@lang('site.login.name') <span>*</span></label>
                                    <input type="text" name="name">
                                    @error('name')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p>
                                    <label>@lang('site.login.surname') <span>*</span></label>
                                    <input type="text" name="surname">
                                    @error('surname')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p>
                                    <label>@lang('site.login.phone')</label>
                                    <input type="text" name="phone">
                                    @error('phone')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p>
                                    <label>@lang('site.login.password') <span>*</span></label>
                                    <input type="password" name="password">
                                    @error('password')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p>
                                    <label>@lang('site.login.repeat_password') <span>*</span></label>
                                    <input type="password" name="passwordConfirm">
                                    @error('passwordConfirm')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <div class="login_submit">
                                    <button type="submit">@lang('site.login.register')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--register area end-->
                </div>
            </div>
        </div>
    </div>

    <!-- customer login end -->
    @widget('brand_slider')
@endsection
