@extends('frontend.layouts.auth-app')

@section('content')
    <section class="login">
        <div class="row g-0">
            <div class="col-md-6">
                <div class="login__image"><img src="/assets/front/images/login.jpg" alt=""></div>
            </div>
            <div class="col-md-6">
                <div class="login__body">
                    <a href="{{ route('home') }}" class="login__back" aria-label="logo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1.6rem" height="1.6rem">
                            <use xlink:href="#svg-prev"></use>
                        </svg>
                        @lang('site.register.return_back')
                    </a>
                    <form action="{{ route('register.submit') }}" class="login__form" method="POST">
                        @csrf
                        <div class="login__head">
                            <h1>@lang('site.register.welcome')</h1>
                            <p>@lang('site.register.welcome_text')</p>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('site.register.name')</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="@lang('site.register.name_placeholder')" value="{{ old('name') }}">
                                    @error('name')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('site.register.surname')</label>
                                    <input type="text" name="surname" class="form-control"
                                           placeholder="@lang('site.register.surname_placeholder')" value="{{ old('surname') }}">
                                    @error('surname')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>@lang('site.register.phone')</label>
                                    <input type="text" name="phone" class="form-control phone-input"
                                           placeholder="@lang('site.register.phone_placeholder')" value="{{ old('phone') }}">
                                    @error('phone')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group form-group--password">
                                    <label>@lang('site.register.password')</label>
                                    <div class="position-relative">
                                        <input type="password" name="password" class="form-control"
                                               placeholder="@lang('site.login.password_placeholder')">
                                        <span class="show-pass">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                                <use xlink:href="#svg-eye"></use>
                                            </svg>
                                        </span>
                                    </div>

                                    @error('password')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group form-group--password">
                                    <label>@lang('site.register.passwordConfirm')</label>
                                    <div class="position-relative">
                                        <input type="password" name="passwordConfirm" class="form-control"
                                               placeholder="@lang('site.login.password_placeholder')">
                                        <span class="show-pass">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="2.0rem" height="2.0rem">
                                                <use xlink:href="#svg-eye"></use>
                                            </svg>
                                        </span>
                                    </div>

                                    @error('passwordConfirm')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        {{--                        <div class="form-group form-group--checkbox mb-5">--}}
                        {{--                            <input type="checkbox" id="remember" class="d-none">--}}
                        {{--                            <label for="remember"><span>Bütün <a href="#">qaydalarla</a> razıyam.</span></label>--}}
                        {{--                        </div>--}}
                        <button class="btn btn--md w-100" type="submit">@lang('site.register.register_button')</button>

                        {{--                        <div class="login__social">--}}
                        {{--                            <p><span>və ya</span></p>--}}
                        {{--                            <ul>--}}
                        {{--                                <li>--}}
                        {{--                                    <a href="#">--}}
                        {{--                                        <img src="images/google.png" alt="">--}}
                        {{--                                        Google ilə davam et--}}
                        {{--                                    </a>--}}
                        {{--                                </li>--}}
                        {{--                                <li>--}}
                        {{--                                    <a href="#">--}}
                        {{--                                        <img src="images/fb.png" alt="">--}}
                        {{--                                        Facebook ilə davam et--}}
                        {{--                                    </a>--}}
                        {{--                                </li>--}}
                        {{--                            </ul>--}}
                        {{--                        </div>--}}
                    </form>
                    <div class="login__footer">
                        <p>@lang('site.register.have_account') <a
                                href="{{ route('login') }}">@lang('site.register.login')</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
