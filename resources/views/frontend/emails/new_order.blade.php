Müştəri məlumatları:

Adı: {{ $order->name }}<br>
Soyadı: {{ $order->surname }} <br>
Əlaqə nömrəsi: {{ $order->surname }} <br>
Email: {{ $order->surname }} <br>
Ünvan: {{ $order->address }} <br><br>

Sifariş məlumatları: <br>

@foreach($items as $item)
    Məhsul adı: {{ $item->title }} <br>
    Məhsul kodu: {{ $item->code }} <br>
    Qiyməti: {{ get_price($item->price) }} <br>
    Sayı: {{ $item->count }} <br>
    -------------------------------------- <br>
@endforeach

<br>
----------------------------- <br>
----------------------------- <br>
----------------------------- <br>
Sifariş kodu: {{ $order->order_id }} <br>
Sifariş tarixi: {{ getParsedDate($order->created_at) }} <br>
Ümumi məhsul sayı: {{ $order->total_count }} <br>
Ümumi məbləğ: {{ get_price($order->total_price) }} AZN <br>

