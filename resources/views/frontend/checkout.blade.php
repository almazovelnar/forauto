@extends('frontend.layouts.app')

@section('content')

    <!-- Breadcrumbs -->
    <div class="container-fluid">
        <ul class="breadcrumbs">
            <li><a href="{{route('home')}}">@lang('site.layout.home_page')</a></li>
            <li>@lang('site.layout.checkout_page')</li>
        </ul>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Checkout -->
    <section class="checkout">
        <div class="container-fluid">
            <h1 class="page-title mb-5">@lang('site.layout.checkout_page')</h1>
            <div class="products__row">
                <div class="products__body ps-0 pt-5">
                    <form action="{{ route('order.send') }}" method="POST" id="order_form" class="row pt-3 align-items-start">
                        @csrf
                        <div class="col basket__body">
                            <div class="checkout__item">
                                <h2 class="checkout__title">1.@lang('site.checkout.personal_information')</h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.checkout.name')</label>
                                            <input type="text" class="form-control" placeholder="@lang('site.checkout.name_placeholder')" name="name" value="{{ old('name', $customer->name ?? null) }}">

                                            @error('name')
                                            <span class="error-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.checkout.surname')</label>
                                            <input type="text" class="form-control" placeholder="@lang('site.checkout.surname_placeholder')" name="surname" value="{{ old('surname', $customer->surname ?? null) }}">

                                            @error('surname')
                                            <span class="error-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.checkout.email')</label>
                                            <input type="email" class="form-control" placeholder="@lang('site.checkout.email_placeholder')"  name="email" value="{{ old('email', $customer->email ?? null) }}">

                                            @error('email')
                                            <span class="error-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>@lang('site.checkout.phone')</label>
                                            <input type="text" class="form-control phone-input" placeholder="@lang('site.checkout.phone_placeholder')" name="phone" value="{{ old('phone', $customer->phone ?? null) }}">

                                            @error('phone')
                                            <span class="error-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__item">
                                <h2 class="checkout__title">2.@lang('site.checkout.delivery_information')</h2>

                                <div class="checkout__type">
                                    <input type="radio" class="d-none" name="delivery" id="courier" value="courier" checked>
                                    <label for="courier" class="courier tab__item" data-tab="courier">
                                        @lang('site.checkout.delivery_courier')
                                    </label>

                                    <input type="radio" class="d-none" name="delivery" id="myself" value="myself">
                                    <label for="myself" class="myself tab__item" data-tab="myself">
                                        @lang('site.checkout.delivery_myself')
                                    </label>
                                </div>

                                <div class="tab-content">
                                    <div class="tab-content__item active" data-tab="courier">
                                        <div class="checkout__courier">
                                    <div class="checkout__note">
                                        <p>@lang('site.checkout.delivery_courier_info_1')</p>
                                        <p>@lang('site.checkout.delivery_courier_info_2')</p>
                                    </div>
                                    <div class="row">
{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>@lang('site.checkout.city')</label>--}}
{{--                                                <select class="form-control" name="city">--}}
{{--                                                    <option value="baku">Bakı</option>--}}
{{--                                                </select>--}}

{{--                                                @error('city')--}}
{{--                                                <span class="error-text">{{ $message }}</span>--}}
{{--                                                @enderror--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>@lang('site.checkout.region')</label>--}}
{{--                                                <select class="form-control" name="region">--}}
{{--                                                    <option value="">Nizami rayonu</option>--}}
{{--                                                </select>--}}

{{--                                                @error('region')--}}
{{--                                                <span class="error-text">{{ $message }}</span>--}}
{{--                                                @enderror--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>@lang('site.checkout.address')</label>
                                                <input type="text" class="form-control" name="address" placeholder="@lang('site.checkout.address_placeholder')" value="{{ old('address') }}">

                                                @error('address')
                                                <span class="error-text">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>@lang('site.checkout.note')</label>
                                                <textarea rows="4" class="form-control" name="note" placeholder="@lang('site.checkout.note_placeholder')">{{ old('note') }}</textarea>

                                                @error('note')
                                                <span class="error-text">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                    <div class="tab-content__item" data-tab="myself">
                                        <div class="checkout__myself">
                                            <div class="checkout__note">
                                                <p>@lang('site.checkout.delivery_myself_info_1')</p>
                                                <p>@lang('site.checkout.delivery_myself_info_2')</p>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group mb-md-0">
                                                        <label>@lang('site.checkout.store_address')</label>
                                                        <input type="text" class="form-control" value="{{ site_info('address') }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-md-0">
                                                        <label>@lang('site.checkout.store_contact_number')</label>
                                                        <input type="text" class="form-control" value="{{ site_info('phone')[0] }}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(site_info('location'))
                                                <a href="javascript:void(0);" class="checkout__map">@lang('site.checkout.look_at_map')</a>
                                                <div class="checkout__map-block">
                                                    <iframe width="100%" height="400"
                                                            src="https://maps.google.com/maps?q={{ site_info('location') }}&hl=en&z=14&output=embed"></iframe>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="checkout__item">
                                <h2 class="checkout__title">3.@lang('site.checkout.payment_method')</h2>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-group--radio mb-md-0">
                                            <input type="radio" class="d-none" id="payment1" name="payment" value="cash" checked>
                                            <label for="payment1" class="h-100">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                                    <use xlink:href="#svg-money"></use>
                                                </svg>
                                                <span>@lang('site.checkout.payment_cash')</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-group--radio mb-md-0">
                                            <input type="radio" class="d-none" id="payment3" name="payment" value="POS">
                                            <label for="payment3" class="h-100">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">
                                                    <use xlink:href="#svg-money"></use>
                                                </svg>
                                                <span>@lang('site.checkout.payment_pos')</span>
                                            </label>
                                        </div>
                                    </div>
{{--                                    <div class="col-md-4">--}}
{{--                                        <div class="form-group form-group--radio mb-md-0">--}}
{{--                                            <input type="radio" class="d-none" id="payment2" name="payment">--}}
{{--                                            <label for="payment2" class="h-100">--}}
{{--                                                <svg xmlns="http://www.w3.org/2000/svg" width="2.4rem" height="2.4rem">--}}
{{--                                                    <use xlink:href="#svg-card"></use>--}}
{{--                                                </svg>--}}
{{--                                                <span>Kartla onlayn ödəniş</span>--}}
{{--                                            </label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    @error('payment')
                                    <span class="error-text">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 basket__side-bar">
                            <div class="basket__result">
                                <p>@lang('site.checkout.your_order')</p>
                                <ul>
{{--                                    <li>--}}
{{--                                        <span>Məhsul məbləği</span>--}}
{{--                                        <span><strong>172 Azn</strong></span>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <span>Endirim</span>--}}
{{--                                        <span><strong>0 Azn</strong></span>--}}
{{--                                    </li>--}}
                                    <li>
                                        <span>@lang('site.checkout.order_total')</span>
                                        <span><strong>{{ get_price(cart()->priceTotal()) }}</strong></span>
                                    </li>
                                </ul>
                                <button type="submit" class="btn btn-orange btn--sm w-100">@lang('site.checkout.confirm')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Checkout -->
@endsection
