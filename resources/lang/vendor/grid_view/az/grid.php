<?php
return [
    'page-info' => 'Ümumi :total, göstərilən :start-:end',
    'select' => 'Seç',
    'actions' => 'Действия',
    'delete' => 'Sil',
    'deletion' => 'Silinmə',
    'search' => 'Axtar',
    'reset' => 'Sıfırla',
    'send' => 'Göndər',
];
