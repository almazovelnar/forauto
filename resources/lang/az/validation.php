<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dil Yoxlama Xətləri
    |--------------------------------------------------------------------------
    |
    | Aşağıdakı dil sətirləri istifadə etdiyi standart xəta mesajlarını ehtiva edir
    | validator sinfi. Bu qaydaların bəzilərinin bir neçə versiyası var
    | ölçü qaydaları kimi. Bu mesajların hər birini burada tənzimləməkdən çekinmeyin.
    |
    */

    'chosen_tag_empty'     => 'Heç bir xəbərə aid olmayan teqi seçmək olmaz',
    'date_busy_for_renew'  => 'Bu tarix digər xəbərdə yenilənmə üçün istifadə olunub',
    'unique_date'          => 'Bu tarix üçün xəbər yerləşdirib',
    'accepted'             => ':attribute qəbul olunmalıdır.',
    'accepted_if'          => 'dəyər :value olan zaman atribut qəbul olunmalıdır',
    'active_url'           => ':attribute etibarlı URL deyil.',
    'after'                => ':attribute :date -dən sonrakı tarix olmalıdır',
    'after_or_equal'       => ':attribute :date tarixindən sonrakı və ya ona bərabər olan tarix olmalıdır.',
    'alpha'                => ':attribute yalnız hərflərdən ibarət olmalıdır.',
    'alpha_dash'           => ':attribute yalnız hərflər, rəqəmlər, xətt və alt xətt olmalıdır.',
    'alpha_num'            => ':attribute yalnız hərf və rəqəmlərdən ibarət olmalıdır.',
    'array'                => ':attribute massiv olmalıdır.',
    'before'               => ':attribute :date -dən əvvəlki tarix olmalıdır.',
    'before_or_equal'      => ':attribute :date -dən əvvəlki və ya ona bərabər olan tarix olmalıdır.',
    'arasında'             => [
        'numeric' => ':attribute :min və :max arasında olmalıdır.',
        'file'    => ':attribute :min və :max kilobayt arasında olmalıdır.',
        'string'  => ':attribute :min və :max simvolları arasında olmalıdır.',
        'array'   => ':attribute :min və :max elementləri arasında olmalıdır.',
    ],
    'boolean'              => ':attribute sahəsi doğru və ya yalan olmalıdır.',
    'confirmed'            => ':attribute təsdiqi uyğun gəlmir.',
    'current_password'     => 'Şifrə yalnışdır',
    'date'                 => ':attribute etibarlı tarix deyil.',
    'date_equals'          => ':attribute :date ilə bərabər tarix olmalıdır.',
    'date_format'          => ':attribute :format formatına uyğun gəlmir.',
    'different'            => ':attribute və :other fərqli olmalıdır.',
    'digits'               => ':attribute :rəqəmli rəqəmlər olmalıdır.',
    'digits_between'       => ':attribute :min və :max rəqəmləri arasında olmalıdır.',
    'dimensions'           => ':attribute yanlış şəkil ölçüləri var.',
    'distinct'             => ':attribute sahəsinin dublikat dəyəri var.',
    'email'                => ':attribute etibarlı e-poçt ünvanı olmalıdır.',
    'ends_with'            => ':attribute aşağıdakılardan biri ilə bitməlidir: :values.',
    'exists'               => ':attribute etibarsızdır.',
    'file'                 => ':attribute fayl olmalıdır.',
    'filled'               => ':attribute sahəsinin dəyəri olmalıdır.',
    'gt'                   => [
        'numeric' => ':attribute :value -dən böyük olmalıdır.',
        'file'    => ':attribute :value kilobaytdan böyük olmalıdır.',
        'string'  => ':attribute :value simvollarından böyük olmalıdır.',
        'array'   => ':attribute :value elementlərindən çox olmalıdır.',
    ],
    'gte'                  => [
        'numeric' => ':attribute :value -dən böyük və ya bərabər olmalıdır.',
        'file'    => ':attribute :dəyər kilobaytdan böyük və ya bərabər olmalıdır.',
        'string'  => ':attribute :value simvollarından böyük və ya bərabər olmalıdır.',
        'array'   => ':attribute -da :value elementləri olmalıdır və ya daha çox.',
    ],
    'image'                => ':attribute şəkil olmalıdır.',
    'in'                   => 'Seçilmiş :attribute etibarsızdır.',
    'in_array'             => ':attribute sahəsi :other -də mövcud deyil.',
    'integer'              => ':attribute tam ədəd olmalıdır.',
    'ip'                   => ':attribute etibarlı IP ünvanı olmalıdır.',
    'ipv4'                 => ':attribute etibarlı IPv4 ünvanı olmalıdır.',
    'ipv6'                 => ':attribute etibarlı IPv6 ünvanı olmalıdır.',
    'json'                 => ':attribute etibarlı JSON sətri olmalıdır.',
    'lt'                   => [
        'numeric' => ':attribute :value -dan az olmalıdır.',
        'file'    => ':attribute ölçüsü :value kilobaytdan az olmalıdır.',
        'string'  => ':attribute :value simvollarından kiçik olmalıdır.',
        'array'   => ':attribute-də :value elementlərindən az olmalıdır.',
    ],
    'lte'                  => [
        'numeric' => ':attribute :value -dan kiçik və ya bərabər olmalıdır.',
        'file'    => ':attribute :value kilobaytdan kiçik və ya bərabər olmalıdır.',
        'string'  => ':attribute :value simvollarından kiçik və ya bərabər olmalıdır.',
        'array'   => ':attribute :value elementindən çox olmamalıdır.',
    ],
    'max'                  => [
        'numeric' => ':attribute :max-dan böyük olmamalıdır.',
        'file'    => ':attribute :max kilobaytdan çox olmamalıdır.',
        'string'  => ':attribute :max simvoldan çox olmamalıdır.',
        'array'   => ':attribute :max-dan çox element olmamalıdır.',
    ],
    'mimes'                => ':attribute  :values tipli fayl olmalıdır.',
    'mimetypes'            => ' :attribute  :values tipli fayl olmalıdır.',
    'min'                  => [
        'numeric' => ':attribute ən azı :min olmalıdır.',
        'file'    => ':attribute ölçüsü :min kilobytes -dən aşağı olmamalıdır.',
        'string'  => ':attribute tərkibində ən azı :min simvol olmalıdır.',
        'array'   => ':attribute tərkibində ən azı :min element olmalıdır.',
    ],
    'multiple_of'          => ':attribute  :value -dan qısa olmalıdır.',
    'not_in'               => 'Seçilmiş :attribute yalnışdır.',
    'not_regex'            => ':attribute formatı yalnışdır.',
    'numeric'              => ':attribute nömrə olmalıdır.',
    'password'             => 'Şifrə yalnışdır.',
    'present'              => ':attribute xanası mövcud olmalıdır',
    'regex'                => ':attribute formatı yalnışdır.',
    'required'             => 'Xananı doldurun',
    'required_if'          => ':attribute xanası mütləqdir, əgər :other bərabərdir :value.',
    'required_unless'      => ':attribute xanası mütləqdir, əgər :other :values xanasında deyilsə.',
    'required_with'        => ':attribute xanası mütləqdir əgər :values mövcuddursa.',
    'required_with_all'    => ':attribute xanası mütləqdir əgər :values mövcuddursa.',
    'required_without'     => ':attribute xanası mütləqdir, əgər :values mövcud deyilsə.',
    'required_without_all' => ':values -in heç bir dəyəri olmadıqda, :attribute xanası tələb olunur.',
    'prohibited'           => ':attribute xanası qadağandır.',
    'prohibited_if'        => ':other bərabərdir :value, o zaman :attribute xanası qadağandır.',
    'prohibited_unless'    => ':attribute xanası qadağandır, o zaman ki :other :values -in tərkibində deyil.',
    'same'                 => ':attribute və :other uyğun olmalıdırlar.',
    'size'                 => [
        'numeric' => ':attribute :size olmalıdır.',
        'file'    => ':attribute size kilobytes olmalıdır.',
        'string'  => ':attribute :size simvollarından ibarət olmalıdır.',
        'array'   => ':attribute tərkibində :size elementləri olmalıdır.',
    ],
    'starts_with'          => ':attribute qeyd olunan :values -lardan birindən başlamalıdır.',
    'string'               => ':attribute sətir olmalıdır.',
    'timezone'             => ':attribute həqiqi saat qovşağı olmalıdır.',
    'unique'               => ':attribute artıq məşğuldur.',
    'uploaded'             => ':attribute yükləmək mümkün olmadı',
    'url'                  => ':attribute etibarlı URL olmalıdır.',
    'uuid'                 => ':attribute etibarlı UUID olmalıdır.',


    /*
    |--------------------------------------------------------------------------
    | Fərdi Doğrulama Dil Xətləri
    |--------------------------------------------------------------------------
    |
    | Burada atributları yoxlamaq üçün istifadəçilərin mesajlarını qeyd edə bilərsiniz,"atribute.rule" razılığını istifadə edərək  sətirlərin adlandıra bilərsiniz. Bu, tez bir zamanda atribut qaydası üçün xüsusi fərdi dil sətrini təyin etmək  üçün imkan verir
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Fərdi Doğrulama Atributları
    |--------------------------------------------------------------------------
    |
    | Aşağıdakı dil sətirləri atribut doldurucusunu əvəz etmək üçün istifadə olunur.
    | Məsəl üçün "email"-dən daha rahat oxuna bilər "E-Mail Address". Bu, sadəcə olaraq mesajımızı daha ifadəli etməyə kömək edir.
    |
    */

    'attributes' => [],

];
