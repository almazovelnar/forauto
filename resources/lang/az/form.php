<?php

return [
    'success' => [
        'save'        => 'Dəyişikliklər uğurla yadda saxlanıldı',
        'tag_merge'   => ':count teqlər birləşdirildi',
        'delete'      => 'Məlumat uğurla silindi',
        'restore'     => 'Məlumat uğurla bərpa edildi (:title)',
        'publish'     => 'Məlumat uğurla dərc olundu (:title)',
        'reject'      => 'Məlumat uğurla ləğv edildi (:title)',
        'push_notice' => 'Bildiriş uğurla göndərildi (:title)',
    ],
    'error'   => [
        'delete_user'  => 'İstifadəçini silmək mümkün olmadı',
        'save'         => 'Dəyişiklikləri saxlamaq mümkün olmadı',
        'tag_merge'    => 'Teqləri birləşdirmək mümkün olmadı',
        'delete'       => 'Məlumatı silmək mümkün olmadı',
        'restore'      => 'Məlumatı bərpa etmək mümkün olmadı',
        'publish'      => 'Məlumatı dərc etmək mümkün olmadı',
        'reject'       => 'Məlumatı ləğv etmək mümkün olmadı',
        'push_notice'  => 'Bildiriş göndərmək mümkün olmadı',
        'user_in_post' => 'Bu yazı başqa istifadəçi tərəfindən məşğuldur',
    ]
];
