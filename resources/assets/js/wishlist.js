let wishlist_params = {
    add_url    : document.currentScript.getAttribute('data-wishlist-add-url'),
    remove_url : document.currentScript.getAttribute('data-wishlist-remove-url'),
};

function updateWishlistInfo(count = 0) {
    if (count) {
        $('.header_wishlist .wishlist_count').removeClass('d-none').text(count)
    } else {
        $('.header_wishlist .wishlist_count').addClass('d-none');
    }
}

$(document).on('click', '.addToWishlist:not(.active)', function (e) {
    e.preventDefault();
    let that = $(this);
    let id = that.closest('.products__item').data('id');
    let thisProductBtn = $(".products__item[data-id='"+id+"']").find('.addToWishlist');

    $.ajax({
        url: wishlist_params.add_url,
        data: {id: id},
        success: function (res) {
            thisProductBtn.addClass('active');
            if (res.success) {
                updateWishlistInfo(res.count);
            }
        }
    })
})

$(document).on('click', '.addToWishlistIn', function (e) {
    e.preventDefault();
    let that = $(this)

    $.ajax({
        url: wishlist_params.add_url,
        data: {id: $(this).closest('.product').data('id')},
        success: function (res) {
            if (res.success) {
                updateWishlistInfo(res.count);
                that.addClass('active removeFromWishlistIn').removeClass('addToWishlistIn');
            }
        }
    })
})

$(document).on('click', '.removeFromWishlistIn', function (e) {
    e.preventDefault();
    let that = $(this)

    $.ajax({
        url: wishlist_params.remove_url,
        data: {id: $(this).closest('.product').data('id')},
        success: function (res) {
            if (res.success) {
                updateWishlistInfo(res.count);
                that.addClass('addToWishlistIn').removeClass('removeFromWishlistIn active');
            }
        }
    })
})

$(document).on('click', '.addToWishlist.active', function (e) {
    e.preventDefault();
    let that = $(this)
    let id = that.closest('.products__item').data('id');
    let thisProductBtn = $(".products__item[data-id='"+id+"']").find('.addToWishlist');

    $.ajax({
        url: wishlist_params.remove_url,
        data: {id: id},
        success: function (res) {
            thisProductBtn.removeClass('active')
            if (res.success) {
                updateWishlistInfo(res.count);
            }
        }
    })
})

$(document).on('click', '.removeFromWishlist', function (e) {
    e.preventDefault();

    $.ajax({
        url: wishlist_params.remove_url,
        data: {id: $(this).closest('.products__item').data('id')},
        success: function (res) {
            if (res.success === true) {
                location.reload();
                Swal.fire(
                    '',
                    translations.itemRemovedFromWishlist,
                    'success'
                )
            }
        }
    })
})
