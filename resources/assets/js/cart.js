let cart_params = {
    add_url    : document.currentScript.getAttribute('data-cart-add-url'),
    update_url    : document.currentScript.getAttribute('data-cart-update-url'),
    remove_url : document.currentScript.getAttribute('data-cart-remove-url'),
};

function updateCartInfo(count = 0, price = 0) {
    if (count) {
        $('.header_cart .cart_count').removeClass('d-none').text(count)
        $('.header_cart .cart_price').removeClass('d-none').text(price + ' AZN')
    } else {
        $('.header_cart .cart_count').addClass('d-none');
        $('.header_cart .cart_price').addClass('d-none');
    }
}

$(document).on('click', '.addToCart', function (e) {
    e.preventDefault();
    let that = $(this)
    let id = $(this).closest('.products__item, .wishlist_product').data('id');
    let thisProductBtn = $(".products__item[data-id='"+id+"']").find('.addToCart');

    $.ajax({
        url: cart_params.add_url,
        data: {id: id},
        success: function (res) {
            if (res.success) {
                updateCartInfo(res.count, res.price);
                thisProductBtn.removeClass('addToCart').addClass('removeFromCart btn-orange')
                thisProductBtn.find('span').text(res.text);
            }
        }
    })
})

$(document).on('click', '.addToCartIn', function (e) {
    e.preventDefault();
    let that = $(this)
    let qty = $('.quantityCartIn').val();

    $.ajax({
        url: cart_params.add_url,
        data: {id: $(this).closest('.product').data('id'), qty: qty},
        success: function (res) {
            if (res.success) {
                updateCartInfo(res.count, res.price);
                showNotification(res.notification);
            }
        }
    })
})

$(document).on('click', '.removeFromCart', function (e) {
    e.preventDefault();
    let that = $(this)
    let id = $(this).closest('.products__item, .wishlist_product').data('id');
    let thisProductBtn = $(".products__item[data-id='"+id+"']").find('.removeFromCart');

    $.ajax({
        url: cart_params.remove_url,
        data: {id: id},
        success: function (res) {
            if (res.success) {
                updateCartInfo(res.count, res.price);
                thisProductBtn.removeClass('removeFromCart btn-orange').addClass('addToCart')
                thisProductBtn.find('span').text(res.text);
            }
        }
    })
})

$(document).on('click', '.removeFromCartIn', function (e) {
    e.preventDefault();
    let that = $(this)

    $.ajax({
        url: cart_params.remove_url,
        data: {id: $(this).closest('.basket__item').data('id')},
        success: function (res) {
            if (res.success) {
                location.reload()
            }
        }
    })
})

$(document).on('click', '.updateCart', function (e) {
    e.preventDefault();
    let products = {};

    $('.productBlock').each(function (i) {
        let that = $(this)

        products[that.data('id')] = that.find('.productQty').val();
    })

    $.ajax({
        url: cart_params.update_url,
        data: {products: products},
        success: function (res) {
            if (res.success) {
                location.reload()
            }
        }
    })
})


function changeCountInCart(e) {
    e.preventDefault();
    let products = {};

    $('.basket__item').each(function (i) {
        let that = $(this);
        products[that.data('id')] = $('.productQty').val();
    })

    $.ajax({
        url: cart_params.update_url,
        data: {products: products},
        success: function (res) {
            if (res.success) {
                location.reload()
            }
        }
    })
}

$(document).on('click', '.basket__count__btn:not(.not_update)', function (e) {
    changeCountInCart(e)
})

$(document).on('change', '.basket__count__input', function (e) {
    changeCountInCart(e)
})
