$('.filter-sort').on('change', function (e) {
    e.preventDefault();
    let that = $(this);
    let url = $('.filter-url').data('url');

    $.ajax({
        url: url,
        data: {sort: that.val()},
        beforeSend: function () {
            $('.loading').addClass('active')
        },
        success: function (res) {
            if (res.success) {
                location.reload();
            }
        }
    })
})
$('.filter-discount').on('change', function (e) {
    e.preventDefault();
    let that = $(this);
    let url = $('.filter-url').data('url');

    $.ajax({
        url: url,
        data: {discount: that.is(':checked')},
        beforeSend: function () {
            $('.loading').addClass('active')
        },
        success: function (res) {
            if (res.success) {
                location.reload();
            }
        }
    })
})

$('.filter input').on('change', function (e) {
    e.preventDefault();
    var filter = {};
    var url = window.location.origin + window.location.pathname;
    var urlParams = new URLSearchParams();

    $('.filter input[type="checkbox"]').each(function () {
        var that = $(this);
        if (that.is(':checked')) {
            urlParams.append(that.attr('name'), that.val())
        }
    })
    $('.filter input[type="text"]').each(function () {
        var that = $(this);
        if (that.val()) {
            urlParams.append(that.attr('name'), that.val())
        }
    })

    let newUrl = url + '?' + urlParams.toString();
    window.history.pushState(null, null, newUrl);


    $.ajax({
        url: newUrl,
        type: 'GET',
        beforeSend: function () {
            $('.loading').addClass('active')
        },
        success: function (res) {
            if (res.success) {
                $('.products-container').html(res.html);
                $('.loading').removeClass('active')
            }
        }
    })
})
