$(document).ready(function () {
    let currentUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

    $(".nav-treeview .nav-link, .nav-link").each(function () {
        if (currentUrl.indexOf($(this).attr('href')) !== -1) {
            $(this).addClass('active');
            $(this).parent().parent().parent().addClass('menu-is-opening menu-open');
        }
    });

    $('.select2').select2()

    $('#langSelector').change(function () {
        let currentLangValue = $(this).val();
        let w = window.location;
        w.href = w.pathname + '?filters%5Blanguage%5D=' + currentLangValue;
    });

    $('.inputCreator').click(function() {
        let makeType = $(this).data('make-type');
        let makeName = $(this).data('make-name');
        let newElement = $('<input />', {
            type: makeType,
            name: makeName,
            class: 'form-control new-input'
        }).appendTo($('.' + makeType + '-inputs'));
    });

    $(".v-block").on("mouseover", function() {
        $(this).find('video').get(0).play();

    }).on('mouseleave', function() {
        $(this).find('video').get(0).load();
    });

    $('#is_season').on('change', function (e) {
        let that = $(this);

        if (that.is(':checked')) {
            $('#season_banner').closest('.form-group').removeClass('d-none');
        } else {
            $('#season_banner').closest('.form-group').addClass('d-none');
        }
    })
})
