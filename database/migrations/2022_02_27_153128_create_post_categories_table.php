<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class CreatePostCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger(NestedSet::LFT)->default(0);
            $table->unsignedInteger(NestedSet::RGT)->default(0);
            $table->unsignedBigInteger(NestedSet::PARENT_ID)->nullable();

            $table->json('title');
            $table->json('description');
            $table->json('meta');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->boolean('status')->default(\App\Enum\Status::ACTIVE);
            $table->timestamps();

            $table->index(NestedSet::getDefaultColumns());
            $table->foreign('parent_id', 'post_category_parent_key')->references('id')->on('post_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_categories');
    }
}
