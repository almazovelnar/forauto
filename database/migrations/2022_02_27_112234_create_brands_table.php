<?php

use Kalnoy\Nestedset\NestedSet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger(NestedSet::LFT)->default(0);
            $table->unsignedInteger(NestedSet::RGT)->default(0);
            $table->unsignedBigInteger(NestedSet::PARENT_ID)->nullable();

            $table->json('title');
            $table->json('description');
            $table->json('link');
            $table->json('slug');
            $table->json('meta');
            $table->string('image')->nullable();
            $table->boolean('status')->default(\App\Enum\Status::ACTIVE);
            $table->boolean('chosen')->default(false);
            $table->timestamps();

            $table->index(NestedSet::getDefaultColumns());
            $table->foreign('parent_id', 'brand_parent_key')->references('id')->on('brands');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
