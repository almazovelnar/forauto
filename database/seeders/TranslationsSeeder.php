<?php

namespace Database\Seeders;

use Arr;
use App\Models\Translate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Translate::query()->truncate();
        foreach ($this->getTranslations() as $group => $translates) {
            foreach ($translates as $prefix => $translations) {
                $withPrefix = Arr::isAssoc($translations);
                if (!$withPrefix) {
                    $translations = [$translations];
                }

                foreach ($translations as $key => $text) {
                    $translate = DB::table('language_lines')
                        ->where('group', $group)
                        ->where('key', $withPrefix ? "$prefix.$key" : $prefix);


                    $text = json_encode([
                        'az' => $text[0],
                        'ru' => $text[1],
                    ]);

                    if (!$translate->first()) {
                        $translate->insert([
                            'group' => $group,
                            'key'   => $withPrefix ? "$prefix.$key" : $prefix,
                            'text'  => $text
                        ]);
                    }

                }
            }
        }

        // clear cache
        if ($first = Translate::first())
            $first->save();
    }

    private function getTranslations(): array
    {
        return [
            'site' => [
                'layout'    => [
                    'profile'              => ['Şəxsi kabinet', 'Profil'],
                    'register'             => ['Qeydiyyat', 'Регистрация'],
                    'login'                => ['Giriş / Qeydiyyat', 'Вход / Регистрация'],
                    'cart'                 => ['Səbət', 'Корзина'],
                    'wishlist'             => ['Seçilmişlər', 'Выбранные'],
                    'search'               => ['Axtar', 'Поиск'],
                    'all_categories'       => ['Bütün kateqoriyalar', 'Все категории'],
                    'popular_products'     => ['Populyar məhsullar', 'Популярные'],
                    'discount_products'    => ['Endirimdə olan məhsullar', 'Со скидкой'],
                    'seasonal_products'    => ['Sezon məhsulları', 'Сезонные'],
                    'brands'               => ['Brendlərimiz', 'Наши бренды'],
                    'search_product'       => ['Axtarış..', 'Введите для поиска..'],
                    'home_page'            => ['Ana səhifə', 'Главная'],
                    'home'                 => ['Ana səhifə', 'Главная'],
                    'products'             => ['Məhsullar', 'Продукты'],
                    'blog'                 => ['Bloq', 'Блог'],
                    'contact-us'           => ['Əlaqə', 'Контакты'],
                    'contact'              => ['Əlaqə', 'Контакты'],
                    'faq'                  => ['Sual-cavab', 'FAQ'],
                    'follow_us'            => ['Bizi izləyin', 'Следите за нами'],
                    'follow_us_text'       => [
                        'Bizi izləmək üçün aşağıda ki, sosial şəbəkələrdən istifadə edə bilərsiz.',
                        'Bizi izləmək üçün aşağıda ki, sosial şəbəkələrdən istifadə edə bilərsiz.'
                    ],
                    'info'                 => ['İnformasiya', 'Информация'],
                    'contact_info'         => ['Əlaqə məlumatları', 'Контактная информация'],
                    'blog_page'            => ['Bloq', 'Блог'],
                    'products_page'        => ['Məhsullar', 'Продукты'],
                    'contact_page'         => ['Əlaqə', 'Контакты'],
                    'contact_us'           => ['Bizimlə əlaqə', 'Bizimlə əlaqə'],
                    'wishlist_page'        => ['Seçilmişlər', 'Seçilmişlər'],
                    'cart_page'            => ['Səbət', 'Səbət'],
                    'details_page'         => ['Hesab məlumatları', 'Hesab məlumatları'],
                    'change_password_page' => ['Şifrənin yenilənməsi', 'Şifrənin yenilənməsi'],
                    'orders_page'          => ['Sifariş tarixçəsi', 'Sifariş tarixçəsi'],
                    'checkout_page'        => ['Sifariş məlumatları', 'Sifariş məlumatları'],
                    'order_page'           => ['Sifariş №:id', 'Sifariş №:id'],
                    'faq_page'             => ['FAQ', 'FAQ'],
                    'about_company'        => ['Şirkət', 'Şirkət'],
                    'services'             => ['Müştəri xidmətləri', 'Müştəri xidmətləri'],
                    'all_rights_reserved'  => ['Bütün hüquqlar qorunur', 'Все права защищены'],
                    'footer_text'          => ['Keyfiyyətli ehtiyat hissələri ilə, təhlükəsiz yollara', 'Keyfiyyətli ehtiyat hissələri ilə, təhlükəsiz yollara'],
                    'profile_orders_page'  => ['Sifariş tarixçəsi', 'Sifariş tarixçəsi'],
                    'read_more'            => ['Daha ətraflı', 'Daha ətraflı'],
                ],
                'home'      => [
                    'our_products'    => ['Məhsullarımız', 'Наши продукты'],
                    'chosen_products' => ['Seçilmiş məhsullar', 'Выбранные редакцией'],
                    'bestsellers'     => ['Ən çox satılanlar', 'Бестселлеры'],
                    'hot_deals'       => ['Endirimdə', 'Со скидкой'],
                    'new_arrivals'    => ['Yeniliklər', 'Новинки'],
                ],
                'profile'   => [
                    'name'                         => ['Ad', 'Ad'],
                    'name_placeholder'             => ['Ad', 'Ad'],
                    'surname'                      => ['Soyad', 'Soyad'],
                    'surname_placeholder'          => ['Soyad', 'Soyad'],
                    'phone'                        => ['Əlaqə nömrəsi', 'Əlaqə nömrəsi'],
                    'phone_placeholder'            => ['Əlaqə nömrəsi', 'Əlaqə nömrəsi'],
                    'email'                        => ['E-mail', 'E-mail'],
                    'email_placeholder'            => ['E-mail', 'E-mail'],
                    'orders_page'                  => ['Sifariş tarixçəsi', 'Sifariş tarixçəsi'],
                    'details_page'                 => ['Hesab məlumatları', 'Hesab məlumatları'],
                    'change_password_page'         => ['Şifrəni dəyiş', 'Şifrəni dəyiş'],
                    'logout'                       => ['Çıxış', 'Çıxış'],
                    'save_button'                  => ['Yadda saxla', 'Yadda saxla'],
                    'personal_information'         => ['Şəxsi məlumatlar', 'Şəxsi məlumatlar'],
                    'password'                     => ['Yeni şifrə', 'Şəxsi məlumatlar'],
                    'password_placeholder'         => ['Şifrəni daxil edin', 'Şifrəni daxil edin'],
                    'password_confirm'             => ['Şifrəni tədsiqləyin', 'Şifrəni tədsiqləyin'],
                    'password_confirm_placeholder' => ['Yeni şifrəni təkrar daxil edin', 'Yeni şifrəni təkrar daxil edin'],
                    'order_id'                     => ['Sifariş №', 'Sifariş №'],
                    'date'                         => ['Tarix', 'Tarix'],
                    'status'                       => ['Status', 'Status'],
                    'total_price'                  => ['Ümumi məbləğ', 'Ümumi məbləğ'],
                    'total_count'                  => ['Məhsul sayı', 'Məhsul sayı'],
                    'view'                         => ['Ətraflı', 'Ətraflı'],
                    'product_code'                 => ['Kod', 'Kod'],
                    'product_name'                 => ['Ad', 'Ad'],
                    'price'                        => ['Qiymət', 'Qiymət'],
                    'count'                        => ['Say', 'Say'],
                    'confirm_logout'               => ['Şəxsi kabinetinizi tərk etmək istədiyinizə əminsiniz?', 'Şəxsi kabinetinizi tərk etmək istədiyinizə əminsiniz?'],
                ],
                'service'   => [
                    'title_1' => ['Ödənişsiz çatdırılma', 'Ödənişsiz çatdırılma'],
                    'text_1'  => ['Bütün məhsulların pulsuz çatdırılması', 'Bütün məhsulların pulsuz çatdırılması'],
                    'title_2' => ['7/24 sifariş qəbulu', '7/24 sifariş qəbulu'],
                    'text_2'  => ['İstənilən zaman sifariş qəbulu', 'İstənilən zaman sifariş qəbulu'],
                    'title_3' => ['Onlayn ödəniş', 'Onlayn ödəniş'],
                    'text_3'  => ['İstənilən bank kartlarından onlayn ödəniş', 'İstənilən bank kartlarından onlayn ödəniş'],
                ],
                'lang'      => [
                    'az' => ['Azərbaycan', 'Азербайджанский'],
                    'ru' => ['Rus', 'Русский'],
                ],
                'empty'     => [
                    'dear_customer'     => ['Hörmətli müştəri,', 'Hörmətli müştəri,'],
                    'products'          => ['Sizin hər hansı bir sifarişiniz mövcud deyil!', 'Sizin hər hansı bir sifarişiniz mövcud deyil!'],
                    'wishlist_products' => ['Sizin seçilmiş məhsulunuz mövcud deyil!', 'Sizin seçilmiş məhsulunuz mövcud deyil!'],
                    'search_products'   => ['Axtarışa uyğun nəticə tapılmadı', 'Axtarışa uyğun nəticə tapılmadı'],
                    'order_products'    => ['Məhsul sifariş et', 'Məhsul sifariş et'],
                ],
                'order'     => [
                    'dear_customer'   => ['Hörmətli müştəri,', 'Hörmətli müştəri,'],
                    'success_message' => ['Sifarişiniz uğurla tamamlandı', 'Sifarişiniz uğurla tamamlandı'],
                    'error_message'   => ['Sifariş zamanı xəta baş verdi', 'Sifariş zamanı xəta baş verdi'],
                ],
                'blog'      => [
                    'title'         => ['Bloq', 'Блог'],
                    'read_more'     => ['Ətraflı', 'Подробнее'],
                    'tags'          => ['Teqlər', 'Теги'],
                    'categories'    => ['Kateqoriyalar', 'Категории'],
                    'popular_posts' => ['Top 3', 'Популярные'],
                ],
                'product'   => [
                    'manufacturer'                  => ['İstehsalçı', 'İstehsalçı'],
                    'price_range'                   => ['Qiymət aralığı', 'Qiymət aralığı'],
                    'price_from'                    => ['- dən', '- dən'],
                    'price_to'                      => ['- dək', '- dək'],
                    'filter_price'                  => ['Qiymət', 'Цена'],
                    'filter'                        => ['Axtarış', 'Поиск'],
                    'filter_manufacturers'          => ['Brendlər', 'Бренды'],
                    'shop'                          => ['Məhsullar', 'Продукты'],
                    'description'                   => ['Təsvir', 'Təsvir'],
                    'features'                      => ['Xüsusiyyətlər', 'Xüsusiyyətlər'],
                    'specifications'                => ['Spesifikasiyalar', 'Характеристики'],
                    'category_name'                 => ['Kateqoriya:', 'Категория:'],
                    'quantity'                      => ['Sayı', 'Кол-во'],
                    'page_info'                     => ['Ümumi: :total, göstərilənlər :show', 'Показано :show, из :total'],
                    'add_to_cart'                   => ['Səbətə əlavə et', 'Добавить в корзину'],
                    'remove_from_cart'              => ['Səbətdən çıxart', 'Удалить из корзины'],
                    'add_to_wishlist'               => ['Seçilmişlərə əlavə et', 'Добавить в избранные'],
                    'remove_from_wishlist'          => ['Seçilmişlərdən çıxart', 'Удалить из избранных'],
                    'related_products'              => ['Alternativ məhsullar', 'Похожие товары'],
                    'is_new'                        => ['Stokda yeni', 'Stokda yeni'],
                    'discount'                      => [':percent% qənaət', 'Скидка на :percent%'],
                    'code'                          => ['Məhsulun kodu - :code', 'Məhsulun kodu - :code'],
                    'count'                         => ['Bazada :count ədəd var', 'Bazada :count ədəd var'],
                    'fast_buy'                      => ['Bir klikə al', 'Bir klikə al'],
                    'confirm_fast_buy'              => ['Təsdiq et', 'Təsdiq et'],
                    'fast_buy_order_success'        => ['Sorğu uğurla göndərildi. Sizinlə tezliklə əlaqə saxlanılacaq.', 'Sorğu uğurla göndərildi. Sizinlə tezliklə əlaqə saxlanılacaq.'],
                    'fast_buy_order_error'          => ['Sorğu göndərmək mümkün olmadı. Bizimlə əlaqə saxlayaraq çətinlik barədə məlumat verə bilərsiz.', 'Sorğu göndərmək mümkün olmadı. Bizimlə əlaqə saxlayaraq çətinlik barədə məlumat verə bilərsiz.'],
                    'delivery'                      => ['Çatdırılma', 'Çatdırılma'],
                    'delivery_by_courier'           => ['Kuryer ilə', 'Kuryer ilə'],
                    'delivery_by_myself'            => ['Özünüz mağazadan götürdükdə', 'Özünüz mağazadan götürdükdə'],
                    'free'                          => ['Pulsuz', 'Pulsuz'],
                    'payment'                       => ['Ödəmə', 'Ödəmə'],
                    'payment_cash'                  => ['Nağd', 'Nağd'],
                    'payment_cash_info'             => ['Təhvil alarkən ödəmək', 'Təhvil alarkən ödəmək'],
                    'payment_card'                  => ['Bank kartları ilə', 'Bank kartları ilə'],
                    'payment_card_info'             => ['Təhvil alındıqda və ya onlayn', 'Təhvil alındıqda və ya onlayn'],
                    'payment_installment_card'      => ['Birbank kartları ilə taksit', 'Birbank kartları ilə taksit'],
                    'payment_installment_card_info' => ['24 aylıq 0%', '24 aylıq 0%'],
                    'search_results'                => ['Axtarış nəticəsində :count məhsul tapıldı.', 'Axtarış nəticəsində :count məhsul tapıldı.'],
                    'product_added_to_cart'         => [':count məhsul səbətə əlavə olundu', ':count məhsul səbətə əlavə olundu'],
                    'product_removed_from_cart'     => ['Məhsul səbətdən çıxarıldı', 'Məhsul səbətdən çıxarıldı'],
                ],
                'filter'    => [
                    'date_desc'  => ['Yeniliklər', 'Новинки'],
                    'popular'    => ['Populyar', 'Популярные'],
                    'price'      => ['Ucuzdan bahaya', 'Цена: Сначала дешёвые'],
                    'price_desc' => ['Bahadan ucuza', 'Цена: Сначала дорогие'],
                    'name'       => ['Ad: A-Z', 'Название: А-Я'],
                    'name_desc'  => ['Ad: Z-A', 'Название: Я-А'],
                    'discount'   => ['Endirimdə olan', 'Endirimdə olan'],
                    'oil'        => ['Mühərrik yağları', 'Mühərrik yağları'],
                    'wheel'      => ['Təkər', 'Təkər'],
                    'search'     => ['Axtar', 'Axtar'],
                    'reset'      => ['Sıfırla', 'Sıfırla'],
                    'brand'      => ['Brend', 'Brend'],
                    'viscosity'  => ['Qatılıq', 'Qatılıq'],
                    'capacity'   => ['Həcm', 'Həcm'],
                    'choose'     => ['Seçin', 'Seçin'],
                    'clear'      => ['Təmizlə', 'Təmizlə'],
                ],
                'cart'      => [
                    'mini_cart'      => ['Səbət', 'Корзина'],
                    'mini_view_cart' => ['Səbətə daxil ol', 'Войти в корзину'],
                    'mini_checkout'  => ['Sifariş ver', 'Оформить заказ'],
                    'mini_total'     => ['Ümumi qiymət', 'Общая сумма'],
                    'cart_totals'    => ['Ümumi məbləğ', 'Ümumi məbləğ'],
                    'subtotal'       => ['Ödəniləcək məbləğ', 'Ödəniləcək məbləğ'],
                    'checkout'       => ['Sifarişi tamamla', 'Sifarişi tamamla'],
                ],
                'delivery'  => [
                    'myself'  => ['Özüm götürəcəm', 'Özüm götürəcəm'],
                    'courier' => ['Kuryer ilə çatdırılma', 'Kuryer ilə çatdırılma'],
                ],
                'payment'   => [
                    'cash' => ['Nəğd ödəniş', 'Nəğd ödəniş'],
                    'pos'  => ['Çatdırılmada POS ilə', 'Çatdırılmada POS ilə'],
                    'card' => ['Kart ilə ödəniş', 'Kart ilə ödəniş'],
                ],
                'checkout'  => [
                    'personal_information'    => ['Şəxsi məlumatlar', 'Şəxsi məlumatlar'],
                    'your_order'              => ['Ümumi məbləğ', 'Ümumi məbləğ'],
                    'order_total'             => ['Ödəniləcək məbləğ', 'Ödəniləcək məbləğ'],
                    'confirm'                 => ['Sifarişi tamamla', 'Sifarişi tamamla'],
                    'name'                    => ['Ad', 'Ad'],
                    'name_placeholder'        => ['Ad', 'Ad'],
                    'surname'                 => ['Soyad', 'Soyad'],
                    'surname_placeholder'     => ['Soyad', 'Soyad'],
                    'email'                   => ['E-mail', 'E-mail'],
                    'email_placeholder'       => ['E-mail', 'E-mail'],
                    'phone'                   => ['Əlaqə nömrəsi', 'Əlaqə nömrəsi'],
                    'phone_placeholder'       => ['Əlaqə nömrəsi', 'Əlaqə nömrəsi'],
                    'delivery_information'    => ['Çatdırılma məlumatları', 'Çatdırılma məlumatları'],
                    'payment_method'          => ['Ödəniş üsulu', 'Ödəniş üsulu'],
                    'delivery_courier'        => ['Kuryer ilə çatdırılma', 'Kuryer ilə çatdırılma'],
                    'delivery_myself'         => ['Özüm götürəcəm', 'Özüm götürəcəm'],
                    'city'                    => ['Şəhər', 'Şəhər'],
                    'region'                  => ['Rayon', 'Rayon'],
                    'street'                  => ['Küçə', 'Şəhər'],
                    'address'                 => ['Ünvan', 'Ünvan'],
                    'address_placeholder'     => ['Ünvan', 'Ünvan'],
                    'street_placeholder'      => ['Küçə', 'Şəhər'],
                    'note'                    => ['Əlavə qeyd', 'Əlavə qeyd'],
                    'note_placeholder'        => ['Əlavə qeydinizi yazın', 'Əlavə qeydinizi yazın'],
                    'payment_cash'            => ['Nəğd ödəniş', 'Nəğd ödəniş'],
                    'payment_pos'             => ['Çatdırılmada POS-ilə', 'Çatdırılmada POS-ilə'],
                    'store_address'           => ['Mağazanın ünvanı', 'Mağazanın ünvanı'],
                    'store_contact_number'    => ['Mağazanın əlaqə nömrəsi', 'Mağazanın əlaqə nömrəsi'],
                    'look_at_map'             => ['Ünvana xəritədə bax', 'Ünvana xəritədə bax'],
                    'delivery_courier_info_1' => [
                        '*Sifariş edilən məhsul kuryer vasitəsiylə həftənin bütün günləri istənilən əraziyə çatdırılır. Çatdırılma sifariş verilən gündən ən çox 2 gün, ən az 2 saat ərzində həyata keçirilir.',
                        '*Sifariş edilən məhsul kuryer vasitəsiylə həftənin bütün günləri istənilən əraziyə çatdırılır. Çatdırılma sifariş verilən gündən ən çox 2 gün, ən az 2 saat ərzində həyata keçirilir.'
                    ],
                    'delivery_courier_info_2' => ['*Bakı şəhərinin istənilən nöqtəsinə, eləcədə Sumqayıt şəhərinə çatdırılma pulsuzdur.', '*Bakı şəhərinin istənilən nöqtəsinə, eləcədə Sumqayıt şəhərinə çatdırılma pulsuzdur.'],
                    'delivery_myself_info_1'  => ['*Sifariş müştəri tərəfindən bir başa götürüldüyü halda mağazanın iş saatlarına diqqət etməsini önəririk.', '*Sifariş müştəri tərəfindən bir başa götürüldüyü halda mağazanın iş saatlarına diqqət etməsini önəririk.'],
                    'delivery_myself_info_2'  => ['*Mağazanın iş saatları həftənin bütün günləri saat 09:00 – 19:00 arasıdır.', '*Mağazanın iş saatları həftənin bütün günləri saat 09:00 – 19:00 arasıdır.'],
                ],
                'read_more' => ['Ətraflı', 'Подробнее'],
                'contact'   => [
                    'contact_us'                => ['Bizimlə əlaqə saxlayın', 'Свяжитесь с нами'],
                    'write_us'                  => ['Bizə yazın', 'Напишите нам'],
                    'address'                   => ['Ünvan:', 'Адрес:'],
                    'send'                      => ['Göndər', 'Отправить'],
                    'your_name'                 => ['Adınız', 'Ваше имя'],
                    'name'                      => ['Ad', 'Имя'],
                    'name_placeholder'          => ['Ad', 'Имя'],
                    'surname'                   => ['Soyad', 'Soyad'],
                    'surname_placeholder'       => ['Soyad', 'Soyad'],
                    'your_email'                => ['Sizin e-mail', 'Ваш эл.адрес'],
                    'email'                     => ['E-mail', 'Эл.адрес'],
                    'email_placeholder'         => ['E-mail', 'Эл.адрес'],
                    'your_message'              => ['Mesajınız', 'Ваше сообщение'],
                    'message'                   => ['Mesaj', 'Сообщение'],
                    'message_placeholder'       => ['Mesaj', 'Сообщение'],
                    'phone'                     => ['Əlaqə nömrəsi', 'Əlaqə nömrəsi'],
                    'phone_placeholder'         => ['Əlaqə nömrəsi', 'Əlaqə nömrəsi'],
                    'submit'                    => ['Göndər', 'Göndər'],
                    'message_sent_successfully' => ['Sorğunuz uğurla göndərildi', 'Sorğunuz uğurla göndərildi'],
                    'title_1'                   => ['Bizə yazın', 'Bizə yazın'],
                    'title_2'                   => ['Mağazamıza gəlin', 'Mağazamıza gəlin'],
                    'title_3'                   => ['Bizimlə əlaqə saxlayın', 'Bizimlə əlaqə saxlayın'],
                    'text_1'                    => ['Hər hansısa problem yaşandığı zaman və ya təklifiniz olduğu halda bizə yazın.', 'Hər hansısa problem yaşandığı zaman və ya təklifiniz olduğu halda bizə yazın.'],
                    'text_2'                    => ['Axtardığınız məhsulu mağazamızdan əldə edə bilərsiniz.', 'Axtardığınız məhsulu mağazamızdan əldə edə bilərsiniz.'],
                    'text_3'                    => ['Hər hansısa sualınız varsa 7/24 aktiv şəkildə cavablandırırıq.', 'Hər hansısa sualınız varsa 7/24 aktiv şəkildə cavablandırırıq.'],
                ],
                'login'     => [
                    'welcome'              => ['Xoşgəlmisiniz!', 'Xoşgəlmisiniz!'],
                    'welcome_text'         => ['Hesabınıza daxil olun', 'Hesabınıza daxil olun'],
                    'phone'                => ['Mobil nömrə', 'Mobil nömrə'],
                    'phone_placeholder'    => ['Mobil nömrəni daxil edin', 'Mobil nömrəni daxil edin'],
                    'password'             => ['Şifrə', 'Şifrə'],
                    'password_placeholder' => ['Şifrəni daxil edin', 'Şifrəni daxil edin'],
                    'remember_me'          => ['Məni xatırla', 'Məni xatırla'],
                    'login_button'         => ['Daxil ol', 'Daxil ol'],
                    'sign_up'              => ['Qeydiyyatdan keçin', 'Qeydiyyatdan keçin'],
                    'dont_have_account'    => ['Hesabınız yoxdur?', 'Hesabınız yoxdur?'],
                    'return_back'          => ['Əsas səhifə', 'Əsas səhifə'],
                    'failed'               => ['Qeyd olunan məlumatlara uyğun istifadəçi tapılmadı', 'Qeyd olunan məlumatlara uyğun istifadəçi tapılmadı'],
                ],
                'register'  => [
                    'welcome'              => ['Xoşgəlmisiniz!', 'Xoşgəlmisiniz!'],
                    'welcome_text'         => ['Daxil olmaq üçün yeni hesab yaradın', 'Daxil olmaq üçün yeni hesab yaradın'],
                    'phone'                => ['Mobil nömrə', 'Mobil nömrə'],
                    'name'                 => ['Ad', 'Ad'],
                    'email'                => ['E-mail', 'E-mail'],
                    'name_placeholder'     => ['Ad', 'Ad'],
                    'surname'              => ['Soyad', 'Soyad'],
                    'surname_placeholder'  => ['Soyad', 'Soyad'],
                    'phone_placeholder'    => ['Mobil nömrəni daxil edin', 'Mobil nömrəni daxil edin'],
                    'password'             => ['Şifrə', 'Şifrə'],
                    'passwordConfirm'      => ['Şifrəni təkrar daxil edin', 'Şifrəni təkrar daxil edin'],
                    'password_placeholder' => ['Şifrəni daxil edin', 'Şifrəni daxil edin'],
                    'remember_me'          => ['Məni xatırla', 'Məni xatırla'],
                    'login_button'         => ['Daxil ol', 'Daxil ol'],
                    'sign_up'              => ['Qeydiyyatdan keçin', 'Qeydiyyatdan keçin'],
                    'dont_have_account'    => ['Hesabınız yoxdur?', 'Hesabınız yoxdur?'],
                    'register_button'      => ['Qeydiyyatdan keç', 'Qeydiyyatdan keç'],
                    'login'                => ['Daxil ol', 'Daxil ol'],
                    'have_account'         => ['Hesabınız var?', 'Hesabınız var?'],
                    'return_back'          => ['Əsas səhifə', 'Əsas səhifə'],
                ],
            ]
        ];
    }
}
