<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRoleAdmin();

        if (!User::first()) {
            $model = new User();
            $model->email       = 'root@admin.com';
            $model->name        = 'Admin';
            $model->status      = true;
            $model->password    = Hash::make('secret');

            $model->assignRole('admin');

            $model->save();

            $this->command->info('User created! Email: root@admin.com; Password: secret');
        }
    }

    private function createRoleAdmin(): void
    {
        Role::findOrCreate('admin');
    }
}
