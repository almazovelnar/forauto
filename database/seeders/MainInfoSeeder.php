<?php

namespace Database\Seeders;

use App\Models\MainInfo;
use Illuminate\Database\Seeder;

class MainInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!MainInfo::first()) {

            $model = new MainInfo();

            foreach (\LaravelLocalization::getSupportedLanguagesKeys() as $lang) {
                $model->setTranslation('title', $lang, 'Title');
                $model->setTranslation('description', $lang, 'Description');
                $model->setTranslation('keywords', $lang, 'Keywords');
                $model->setTranslation('address', $lang, 'Address');
            }

            $model->name  = 'Name';
            $model->email = ['test@email.com'];
            $model->phone = ['+994000000000'];

            $model->save();
        }
    }
}
