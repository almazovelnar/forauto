<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = $this->getConfigList();

        foreach ($list as $key => $item) {
            if (Config::query()->where('param', $key)->first())
                continue;

            $model = new Config();
            $model->param = $key;
            $model->value = '#';
            $model->default = '#';
            $model->label = $item;
            $model->type = 'string';

            $model->save();
        }
    }

    public function getConfigList()
    {
        return [
            'FACEBOOK'       => 'Facebook profile link',
            'INSTAGRAM'      => 'Instagram profile link',
            'YOUTUBE'        => 'Youtube profile link',
            'TWITTER'        => 'Twitter profile link',
            'KASPI_LINK'     => 'Kaspi banner link',
            'GENCLIK_LINK'   => 'Gənclik banner link',
            'EDEBIYYAT_LINK' => 'Ədəbiyyat banner link',
        ];
    }
}
