const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .styles([
        'resources/css/bootstrap.min.css',
        'resources/plugins/slider/fslider.css',
        'resources/plugins/owl-carousel/owl.carousel.css',
        'resources/plugins/owl-carousel/owl.theme.default.css',
        'resources/css/style.css'
    ],
        'public/assets/frontend/css/app.css'
    )
    .scripts([
        'resources/js/jquery.min.js',
        'resources/plugins/owl-carousel/owl.carousel.min.js',
        'resources/js/bootstrap.min.js',
        'resources/plugins/slider/fslider.min.js',
        'resources/js/script.js',
        'resources/js/post/infinityScroll.js'
    ],
        'public/assets/frontend/js/app.js'
    )
    .sourceMaps()
    .copy('resources/img', 'public/assets/frontend/img')
    .copy('resources/fonts', 'public/assets/frontend/fonts')
    .version();
