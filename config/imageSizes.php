<?php

return [
    'template' => [
        'list'  => [
            'w' => 125,
            'h' => 187.5
        ],
        'large' => [
            'w' => 345,
            'h' => 374
        ],
    ]
];
