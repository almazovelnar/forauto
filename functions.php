<?php

use Carbon\Carbon;

if (!function_exists('grid')) {
    function grid($config)
    {
        return (new \App\Helpers\Grid($config))->render();
    }
}

if (!function_exists('site_info')) {
    function site_info(string $key, $default = null)
    {
        return \App\Helpers\Information::instance()->get($key, $default);
    }
}

if (!function_exists('site_config')) {
    function site_config(string $key)
    {
        return \App\Helpers\Config::instance()->get($key);
    }
}

if (!function_exists('product_image')) {
    function product_image($filename, $template = 'list'): string
    {
        if (empty($filename)) {
            return asset('images/thunk.png');
        }
        return asset('media/' . $template . '/' . $filename);
    }
}

if (!function_exists('post_image')) {
    function post_image($filename): string
    {
        if (empty($filename)) {
            return asset('images/thunk.png');
        }
        return asset('storage/posts/' . $filename);
    }
}

if (!function_exists('getParsedDate')) {
    function getParsedDate($date, string $format = 'd F Y, H:i'): string
    {
        if (is_a($date, Carbon::class)) {
            return $date->translatedFormat($format);
        }
        return Carbon::parse($date)->translatedFormat($format);
    }
}
if (!function_exists('get_price')) {
    function get_price($price = null, $withCurrency = true): ?string
    {
        if (!$price) return null;
        $price = number_format((str_replace(',', '', $price)) / 100, 2);

        if ($withCurrency) {
            return $price . ' AZN';
        }

        return $price;
    }
}
if (!function_exists('get_discount')) {
    function get_discount($price, $salePrice = null): ?string
    {
        if (!$salePrice) return null;
        return ceil(100 - ($salePrice/$price * 100));
    }
}

if (!function_exists('get_image')) {
    function get_image($filename, $folder = '', ?string $size = null): string
    {
        if (empty($filename)) {
            return asset('images/thunk.svg');
        }
//dd($filename,$folder);
        if ($size != 'o') {
            $sizes =config('imageSizes.template');
            $storage  = Storage::disk('public');
            $path     = $storage->path('thumbs/' . $size . '/');
            $original = storage_path('app/public/photos/' . ($folder ? $folder . '/' : '')  . $filename);
            if (!is_file($original)) {
                return asset('images/thunk.svg');
            }
            if (!is_file($path . $filename)) {
                if (!is_dir($path)) {
                    $storage->createDir('thumbs/' . $size);
//                    mkdir($path);
                }
                \Intervention\Image\Facades\Image::make($original)
                    ->resize($sizes[$size]['w'], $sizes[$size]['h'])
                    ->save($path . $filename);
            }
        }

        return asset('storage/' . ($size == 'o' ? "photos/" : "thumbs/$size/") . $filename);
    }

}

if (! function_exists('str_text')) {
    function str_text($value, $limit = 60, $end = '...'): string
    {
        return \Illuminate\Support\Str::words(strip_tags(htmlspecialchars_decode(html_entity_decode($value))), $limit, $end);
    }
}

if (! function_exists('wishlist')) {
    function wishlist()
    {
        return \Gloudemans\Shoppingcart\Facades\Cart::instance('wishlist')->content();
    }
}

if (! function_exists('getCart')) {
    function getCart()
    {
        return \Gloudemans\Shoppingcart\Facades\Cart::instance('cart')->content();
    }
}

if (! function_exists('cart')) {
    function cart()
    {
        return \Gloudemans\Shoppingcart\Facades\Cart::instance('cart');
    }
}

if (! function_exists('wishlist_ids')) {
    function wishlist_ids()
    {
        return \Gloudemans\Shoppingcart\Facades\Cart::instance('wishlist')->content()->pluck('id')->toArray();
    }
}

if (! function_exists('cart_ids')) {
    function cart_ids()
    {
        return \Gloudemans\Shoppingcart\Facades\Cart::instance('cart')->content()->pluck('id')->toArray();
    }
}
