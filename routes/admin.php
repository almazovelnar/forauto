<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\PostController;
use App\Http\Controllers\Backend\SiteController;
use App\Http\Controllers\Backend\BrandController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\SlideController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\CustomerController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\Auth\LoginController;
use App\Http\Controllers\Backend\PostCategoryController;

Route::middleware(['auth', 'preventBackHistory', 'role:admin'])->group(function () {
    Route::get('/', [SiteController::class, 'index'])->name('home');

    Route::get('property/get-list', [\App\Http\Controllers\Backend\PropertyController::class, 'getList'])->name('property.get-list');
    Route::resource('property', \App\Http\Controllers\Backend\PropertyController::class)->except(['show']);

    Route::resource('categories', CategoryController::class)->except(['show']);
    Route::get('categories/move/{id}/{type}', [CategoryController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('categories.move');


    Route::resource('brands', BrandController::class)->except(['show']);
    Route::get('brands/move/{id}/{type}', [BrandController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('brands.move');


    Route::resource('products', ProductController::class);
    Route::get('products-import', [ProductController::class, 'import'])->name('products.import');
    Route::post('products-importing', [ProductController::class, 'importProducts'])->name('products.import-products');

    Route::resource('slides', SlideController::class)->except('show');
    Route::get('slides/move/{id}/{type}', [SlideController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('slides.move');

    /* Page routes */
    Route::resource('pages', \App\Http\Controllers\Backend\PageController::class)->except(['show']);
    Route::post('pages/delete-image/{id}', [\App\Http\Controllers\Backend\PageController::class, 'deleteImage'])
        ->name('pages.delete-image');
    Route::get('pages/move/{id}/{type}', [\App\Http\Controllers\Backend\PageController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('pages.move');

    /* User routes */
    Route::post('users/delete-thumb/{id}', [\App\Http\Controllers\Backend\UserController::class, 'deleteThumb'])
        ->name('users.delete-image');
    Route::resource('users', \App\Http\Controllers\Backend\UserController::class);

    Route::resource('faq', \App\Http\Controllers\Backend\FaqController::class)->except(['show']);
    Route::resource('config', \App\Http\Controllers\Backend\ConfigController::class)->except(['show']);
    Route::resource('translates', \App\Http\Controllers\Backend\TranslateController::class)->except(['show']);

    /* Main Info routes */
    Route::post('main-info/delete-logo/{id}', [\App\Http\Controllers\Backend\MainInfoController::class, 'deleteLogo'])
        ->name('main-info.delete-logo');
    Route::post('main-info/delete-favicon/{id}', [\App\Http\Controllers\Backend\MainInfoController::class, 'deleteFavicon'])
        ->name('main-info.delete-favicon');
    Route::get('main-info', [\App\Http\Controllers\Backend\MainInfoController::class, 'index'])->name('main-info');
    Route::put('main-info', [\App\Http\Controllers\Backend\MainInfoController::class, 'update'])->name('main-info.update');

    /* Image Upload route */
    Route::post('ckeditor/image_upload', [\App\Http\Controllers\CkeditorController::class, 'upload'])->name('ckeupload');

    Route::resource('tags', \App\Http\Controllers\Backend\TagController::class);
    Route::prefix('blog')->name('blog.')->group(function () {
        Route::resource('categories', PostCategoryController::class);
        Route::get('categories/move/{id}/{type}', [PostCategoryController::class, 'move'])
            ->where('type', 'up|down')
            ->where('id', '[0-9]+')
            ->name('categories.move');
        Route::resource('posts', \App\Http\Controllers\Backend\PostController::class);
    });
    Route::get('posts/tag-list', [PostController::class, 'tagList'])->name('posts.tag-list');


    Route::resource('orders', \App\Http\Controllers\Backend\OrderController::class);
    Route::get('orders/reversal/{transaction_id}', [OrderController::class, 'reversal'])->name('orders.reversal');

    Route::resource('customers', CustomerController::class)->except(['show']);

    Route::prefix('photos')->name('photos.')->group(function () {
        Route::get('index', [\App\Http\Controllers\Backend\PhotoController::class, 'index'])->name('index');
        Route::get('folder', [\App\Http\Controllers\Backend\PhotoController::class, 'folderIndex'])->name('folder');
        Route::get('trash', [\App\Http\Controllers\Backend\PhotoController::class, 'trashIndex'])->name('trash');
        Route::get('restore', [\App\Http\Controllers\Backend\PhotoController::class, 'restore'])->name('restore');
        Route::post('create', [\App\Http\Controllers\Backend\PhotoController::class, 'create'])->name('create');
        Route::get('update', [\App\Http\Controllers\Backend\PhotoController::class, 'update'])->name('update');
        Route::post('update', [\App\Http\Controllers\Backend\PhotoController::class, 'update'])->name('update.submit');
        Route::post('size', [\App\Http\Controllers\Backend\PhotoController::class, 'size'])->name('size.submit');
        Route::get('create-folder', [\App\Http\Controllers\Backend\PhotoController::class, 'folderCreate'])->name('create-folder');
        Route::get('delete-folder', [\App\Http\Controllers\Backend\PhotoController::class, 'folderDelete'])->name('delete-folder');
        Route::get('update-folder', [\App\Http\Controllers\Backend\PhotoController::class, 'folderUpdate'])->name('update-folder');
        Route::post('update-folder', [\App\Http\Controllers\Backend\PhotoController::class, 'folderUpdate'])->name('update-folder.submit');
        Route::post('create-folder', [\App\Http\Controllers\Backend\PhotoController::class, 'folderCreate'])->name('create-folder.submit');
        Route::post('delete', [\App\Http\Controllers\Backend\PhotoController::class, 'delete'])->name('delete');
        Route::post('soft-delete', [\App\Http\Controllers\Backend\PhotoController::class, 'softDelete'])->name('softDelete');
        Route::post('crop', [\App\Http\Controllers\Backend\PhotoController::class, 'crop'])->name('crop');
        Route::get('sort', [\App\Http\Controllers\Backend\PhotoController::class, 'sort'])->name('sort');
        Route::post('render-gallery', [\App\Http\Controllers\Backend\PhotoController::class, 'renderGallery'])->name('render-gallery');
        Route::post('get-size', [\App\Http\Controllers\Backend\PhotoController::class, 'getSize'])->name('get-size');
    });

});

Route::middleware(['preventBackHistory'])->group(function () {
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login.login');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});
