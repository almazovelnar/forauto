<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\SiteController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\OrderController;
use App\Http\Controllers\Frontend\StaticController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\ProfileController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


Route::prefix(LaravelLocalization::setLocale())->middleware(['localize'])->group(function () {
    Route::get('/', [SiteController::class, 'index'])->name('home');

    Route::post('/product/fast-buy/{id}', [\App\Http\Controllers\Frontend\ProductController::class, 'fastBuy'])->name('fast-buy');

    Route::get('/products/filter', [\App\Http\Controllers\Frontend\ProductController::class, 'filter'])
        ->name('products.filter');
    Route::get('/products/{slug}', [\App\Http\Controllers\Frontend\ProductController::class, 'index'])
        ->where('slug', '[\w\-]+')
        ->name('products');

    Route::get('/search', [\App\Http\Controllers\Frontend\SearchController::class, 'index'])
        ->middleware(\App\Http\Middleware\SearchQuery::class)->name('search');
    Route::get('/search/filter', [\App\Http\Controllers\Frontend\SearchController::class, 'filter'])->name('search.filter');
    Route::get('/product/{slug}', [\App\Http\Controllers\Frontend\ProductController::class, 'index'])->name('products.category')
        ->where('slug', '[\w\-]+');
    Route::get('/product/{slug}', [\App\Http\Controllers\Frontend\ProductController::class, 'view'])->name('product')
        ->where('slug', '[\w\-]+');


    Route::get('/blog', [\App\Http\Controllers\Frontend\BlogController::class, 'index'])->name('blog.index');
    Route::name('blog.')->prefix('blog')->group(function () {
        Route::get('/{slug}', [\App\Http\Controllers\Frontend\BlogController::class, 'index'])->name('category')
            ->where('slug', '[\w\-]+');
        Route::get('/post/{slug}', [\App\Http\Controllers\Frontend\BlogController::class, 'view'])->name('post')
            ->where('slug', '[\w\-]+');
        Route::get('/tag/{slug}', [\App\Http\Controllers\Frontend\BlogController::class, 'tag'])->name('tag')
            ->where('slug', '[\w\-]+');
        Route::get('/search', [\App\Http\Controllers\Frontend\BlogController::class, 'search'])->name('search');
    });


    Route::get('/page/{slug}', [StaticController::class, 'view'])
        ->name('page')
        ->where('slug', '[\w\-]+');
    Route::get('/contact', [ContactController::class, 'index'])->name('contact');
    Route::get('/faq', [StaticController::class, 'faq'])->name('faq');
    Route::post('/contact/send', [ContactController::class, 'send'])->name('contact.send_message');

    Route::get('/favorites', [\App\Http\Controllers\Frontend\FavoriteController::class, 'index'])->name('favorites');
    Route::get('/favorite/add', [\App\Http\Controllers\Frontend\FavoriteController::class, 'add'])->name('favorite.add');
    Route::get('/favorite/remove', [\App\Http\Controllers\Frontend\FavoriteController::class, 'remove'])->name('favorite.remove');
    Route::get('/favorite/filter', [\App\Http\Controllers\Frontend\FavoriteController::class, 'filter'])->name('favorite.filter');



    Route::get('/cart', [CartController::class, 'index'])->name('cart');
    Route::get('/cart/add', [\App\Http\Controllers\Frontend\CartController::class, 'add'])->name('cart.add');
    Route::get('/cart/update', [\App\Http\Controllers\Frontend\CartController::class, 'update'])->name('cart.update');
    Route::get('/cart/remove', [\App\Http\Controllers\Frontend\CartController::class, 'remove'])->name('cart.remove');

    Route::get('/checkout', [\App\Http\Controllers\Frontend\CheckoutController::class, 'index'])->name('checkout');

    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::post('/send', [OrderController::class, 'send'])->name('send');
        Route::get('/thank-you', [OrderController::class, 'thankYou'])->name('thankYou');
        Route::get('/payment-result', [OrderController::class, 'paymentResult'])->name('payment.result');
        Route::get('/fail', [OrderController::class, 'fail'])->name('fail');
    });

    Route::group(['middleware' => ['auth:siteCustomer']], function () {
        Route::get('/history', [ProfileController::class, 'history'])->name('history');
        Route::get('/history/{id}', [ProfileController::class, 'historyView'])->name('history.view');
        Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
        Route::get('/profile/change-password', [ProfileController::class, 'changePasswordForm'])->name('profile.change-password');
        Route::put('/profile/change-password', [ProfileController::class, 'changePassword'])->name('profile.change-password.submit');
        Route::put('/profile/update', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::get('/profile/delete', [ProfileController::class, 'delete'])->name('profile.delete');
    });

    Route::middleware(['preventBackHistory'])->group(function () {
        Route::get('login', [\App\Http\Controllers\Frontend\Auth\LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [\App\Http\Controllers\Frontend\Auth\LoginController::class, 'login'])->name('login.submit');
        Route::get('register', [\App\Http\Controllers\Frontend\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
        Route::post('register', [\App\Http\Controllers\Frontend\Auth\RegisterController::class, 'register'])->name('register.submit');
        Route::get('logout', [\App\Http\Controllers\Frontend\Auth\LoginController::class, 'logout'])->name('logout');

        Route::get('reset-password', [\App\Http\Controllers\Frontend\Auth\ResetPasswordController::class, 'showResetPasswordForm'])->name('reset-password');
        Route::post('reset-password', [\App\Http\Controllers\Frontend\Auth\ResetPasswordController::class, 'showResetPasswordForm'])->name('reset-password.submit');
        Route::get('send-reset-code', [\App\Http\Controllers\Frontend\Auth\ResetPasswordController::class, 'showCodeInputForm'])->name('show-code-form');
        Route::post('send-reset-code', [\App\Http\Controllers\Frontend\Auth\ResetPasswordController::class, 'showCodeInputForm'])->name('show-code-form.submit');
        Route::get('change-password', [\App\Http\Controllers\Frontend\Auth\ResetPasswordController::class, 'changePassword'])->name('change-password');
        Route::post('change-password', [\App\Http\Controllers\Frontend\Auth\ResetPasswordController::class, 'changePassword'])->name('change-password.submit');
    });
});
